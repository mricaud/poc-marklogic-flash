xquery version "1.0-ml";

declare namespace xf = "http://www.lefebvre-sarrut.eu/ns/xmlfirst";
declare namespace sr = "http://www.w3.org/2005/sparql-results#";
declare namespace cts = "http://marklogic.com/cts";

(:import module namespace search = "http://marklogic.com/appservices/search" at "/MarkLogic/appservices/search/search.xqy";:)
import module namespace search = "http://marklogic.com/appservices/search" at "/impl/search.xqy";
import module namespace sem = "http://marklogic.com/semantics" at "/MarkLogic/semantics.xqy";

declare variable $opt := xdmp:get-request-field("opt", "search-options.xml");
declare variable $options := xdmp:document-get(xdmp:modules-root()||"conf/"|| $opt)/*;

declare variable $debug := xdmp:get-request-field("debug", "");

declare variable $content-type := if ($debug = '') then("text/html; charset=utf-8") else("text/xml");

declare variable $q := xdmp:get-request-field("q", "");

declare variable $results := search:search($q, $options, xs:unsignedLong(xdmp:get-request-field("start","1")));

(:declare variable $results := for $x in search:search($q, $options, xs:unsignedLong(xdmp:get-request-field("start","1")))
                             return cts:highlight($x, $q, <span class="highlight">{$cts:text}</span>);
                             (\: FIXME : ici le cts:highlight agit sur le resultat de la recherche, donc il highlight même des éléments qui ne sont pas dans le xml,
                             ex : <search:qtext><span class="highlight" xmlns="">loi</span></search:qtext>:\)
                             résolu en passant par un snippet customizé:) 

declare variable $facet-size as xs:integer := 5;

declare function local:show-result($snipet as element(), $uri as xs:string)
{
    let $href := 'display-document.xqy?uri=' || xdmp:url-encode($uri)
    return
    
    if ($snipet/xf:editorialEntity) then
    (
    let $ee := $snipet/xf:editorialEntity
    return (
    <tr xmlns="http://www.w3.org/1999/xhtml">
      <td class="systemTitle">
        <a href="{$href}" target="_blank">
          {$ee/xf:metadata/xf:meta[@code = 'systemTitle']/xf:value/*/node()}
        </a>
      </td>
      <td class="refdoc">
        {$ee/xf:metadata/xf:meta[@code = 'EFL_META_blocRefDoc']/xf:value/*/node()}
      </td>
      <td class="rattach">
        {$ee/xf:metadata/xf:meta[@code = 'EFL_META_blocRattach']/xf:value/*/node()}
      </td>
      <td class="status">
        {string($ee/@status)}
      </td>
      <td class="auteur">
        {for $a in tokenize(substring-before(fn:substring($ee/@authorUser, 2), ']'), ',') return $a}
      </td>
      <td class="creationDate">
        {substring-before($ee/@creationDateTime, 'T')}
      </td>
      <td class="lastModificationDate">
        {substring-before($ee/@lastModificationDateTime, 'T')}
      </td>
      <td class="mainProduct">
        {string($ee/@mainProduct)}
      </td>
      <td class="numDansPublication">
        ?
      </td>
      <td class="accrochage-revue">
        {
          let $id as xs:string := $ee/@xf:id
          
          let $sparqlQuery as xs:string := 
            <myQuery>
                PREFIX rdf: &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                PREFIX rdfs: &lt;http://www.w3.org/2000/01/rdf-schema#>
                PREFIX xf: &lt;http://www.lefebvre-sarrut.eu/ns/xmlfirst#>
                
                SELECT ?label
                WHERE {{
                  ?currentEE rdf:type xf:EditorialEntity .
                  ?currentEE xf:id "{$id}" .
                  ?currentEE xf:hasParent/rdfs:label ?label
                  # xf:{$id} xf:hasParent/rdfs:label ?label
                }}
            </myQuery>/text()
          
          let $triples := sem:sparql($sparqlQuery)
          let $triples-xml := sem:query-results-serialize($triples, "xml")
          for $label in $triples-xml//sr:binding[@name = 'label']/sr:literal/text()
          return <p>{$label}</p>
        }
      </td>
      <td class="accrochage-fil-inneo">
        ?
      </td>
      <td class="publishingDateTime">
        {for $p in $ee/xf:metadata/xf:meta[@code = 'systemProducts']/xf:value/xf:productRef[@publishingDateTime]
        return(
            substring-before($p/@publishingDateTime, 'T'),
            ' (', 
            $p/parent::xf:value/following-sibling::*[1]/self::xf:verbalization/*/node(),
            ')',
            <br/>
            )
        }
      </td>
      <td class="publie">
        {
          let $p := $ee/xf:metadata/xf:meta[@code = 'systemProducts']/xf:value/xf:productRef[@publishingDateTime]
          return if (count($p) = 0) then ('Non') else('Oui')
        }
      </td>
    </tr>
    )
   )
   else(
    <tr xmlns="http://www.w3.org/1999/xhtml">
      <td colspan="13"><a href="{$href}" target="_blank">{$uri}</a> n'est pas une entité éditoriale</td>
    </tr>
   )
};

declare function local:search-results()
{
  let $show-results as element()* := 
    <table xmlns="http://www.w3.org/1999/xhtml">
      <thead>
        <tr>
          <th>Libellé</th>
          <th>RefDoc</th>
          <th>Rattach</th>
          <th>Statut</th>
          <th>Auteur(s)</th>
          <th>Date de création</th>
          <th>Date de MAJ</th>
          <th>Produit</th>
          <th>N° dans publication</th>
          <th>Accrochage Revues</th>
          <th>Accrochage Fil Inneo</th>
          <th>Date de publication</th>
          <th>Publié</th>
        </tr>
       </thead>
       <tbody>
        {
          for $snippet in $results/search:result/search:snippet
          return local:show-result($snippet, $snippet/parent::search:result/@uri)
        }
        </tbody>
      </table>
    
  return 
    if(string($results/@total) != '0')
    then ($show-results)
    else ()
};

declare function local:facets()
{
    for $facet in $results/search:facet
    let $facet-count := fn:count($facet/search:facet-value)
    let $facet-name := fn:data($facet/@name)
    return
        if($facet-count > 0)
        then <div class="facet">
                <strong>{$facet-name}</strong>
                {
                    let $facet-items :=
                        for $val in $facet/search:facet-value
                        let $print := if($val/text()) then $val/text() else "Non valorisé"
                        let $qtext := ($results/search:qtext)
                        (:let $sort := local:get-sort($qtext):)
                        let $this :=
                            if (fn:matches($val/@name/string(),"\W"))
                            then fn:concat('"',$val/@name/string(),'"')
                            else if ($val/@name eq "") then '""'
                            else $val/@name/string()
                        let $this := fn:concat($facet/@name,':',$this)
                        (:let $selected := true():)
                        (:let $selected := fn:matches($qtext,replace($this, '-', '\\-'),"i"):)
                        (:FIXME : value "[]" have been found in document set :)
                        (:let $selected := fn:matches($qtext,replace($this, '(\\-|\\[|\\])', '\\$1'),"i"):)
                        (:FIXME : contains is maybe not enought cause the string could maybe just starts with $this, need to check for delimiters:)
                        let $selected := fn:contains($qtext,$this)
                        let $icon := 
                            if($selected)
                            then <img src="images/checkmark.gif"/>
                            else <img src="images/checkblank.gif"/>
                        let $link := 
                            if($selected)
                            then search:remove-constraint($qtext,$this,$options)
                            else if(string-length($qtext) gt 0)
                            then fn:concat("(",$qtext,")"," AND ",$this)
                            else $this
                        (:let $link := if($sort and fn:not(local:get-sort($link))) then fn:concat($link," ",$sort) else $link:)
                        let $link := fn:encode-for-uri($link)
                        return
                            <div class="facet-value">
                              {$icon}<a href="index.xqy?q={$link}&amp;opt={$opt}">{$print}</a> [{fn:data($val/@count)}]
                            </div>
                        return (
                                <div>{$facet-items[1 to $facet-size]}</div>,
                                if($facet-count gt $facet-size)
                                then (
                          <div class="facet-hidden" id="{$facet-name}">{$facet-items[position() gt $facet-size]}</div>,
                          <div class="facet-toggle" id="{$facet-name}_more"><img src="images/checkblank.gif"/><a href="javascript:toggle('{$facet-name}');" class="white">Voir plus...</a></div>,
                          <div class="facet-toggle-hidden" id="{$facet-name}_less"><img src="images/checkblank.gif"/><a href="javascript:toggle('{$facet-name}');" class="white">Réduire...</a></div>
                        )
                        else ()
                       )
             }
            </div>
        else (<div>&#160;</div>)
};


declare function local:pagination($resultspag)
{
    let $start := xs:unsignedLong($resultspag/@start)
    let $length := xs:unsignedLong($resultspag/@page-length)
    let $total := xs:unsignedLong($resultspag/@total)
    let $last := xs:unsignedLong($start + $length -1)
    let $end := if ($total > $last) then $last else $total
    (:let $qtext := $resultspag/search:qtext[1]/text():)
    let $qtext := string-join($resultspag/search:qtext[1]//text())
    let $next := if ($total > $last) then $last + 1 else ()
    let $previous := if (($start > 1) and ($start - $length > 0)) then fn:max((($start - $length),1)) else ()
    let $next-href := 
         if ($next) 
         then fn:concat("/index.xqy?q=",if ($qtext) then fn:encode-for-uri($qtext) else (),"&amp;start=",$next, "&amp;opt=",$opt)
         else ()
    let $previous-href := 
         if ($previous)
         then fn:concat("/index.xqy?q=",if ($qtext) then fn:encode-for-uri($qtext) else (),"&amp;start=",$previous, "&amp;opt=",$opt)
         else ()
    let $total-pages := fn:ceiling($total div $length)
    let $currpage := fn:ceiling($start div $length)
    let $pagemin := 
        fn:min(for $i in (1 to 4)
        where ($currpage - $i) > 0
        return $currpage - $i)
    let $rangestart := fn:max(($pagemin, 1))
    let $rangeend := fn:min(($total-pages,$rangestart + 4))
    
    return (
        <div id="pagination" xmlns="http://www.w3.org/1999/xhtml">
          <b>{$start}</b> à <b>{$end}</b> sur {$total}
          <!--local:sort-options(),-->
          {if($rangestart eq $rangeend)
            then ()
            else
            <span id="pagenavigation"> 
               { if ($previous) then <a href="{$previous-href}"><button type="button">&lt;&lt;</button></a> else () }
               {
                 for $i in ($rangestart to $rangeend)
                 let $page-start := (($length * $i) + 1) - $length
                 let $page-href := concat("/index.xqy?q=",if ($qtext) then encode-for-uri($qtext) else (),"&amp;start=",$page-start, "&amp;opt=",$opt)
                 return 
                    if ($i eq $currpage) 
                    then <b>&#160;<u>{$i}</u>&#160;</b>
                    else <span>&#160;<a href="{$page-href}">{$i}</a>&#160;</span>
                }
               { if ($next) then <a href="{$next-href}"><button type="button">>></button></a> else ()}
            </span>
            }
         </div>
    )
};

xdmp:set-response-content-type($content-type),

let $output :=
('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">',
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Flash Search ML POC</title>
    <link href="css/flashml.css" rel="stylesheet" type="text/css" />
    <script src="js/flashml.js" type="text/javascript"/>
  </head>
  <body>
    <div id="top">
      <form name="form1" method="get" action="index.xqy">
        <p>Recherche : <input type="text" style="width:50%;" name="q" id="q" action="index.xqy" value="{$q}"/>&#160;
        Facette : 
        <select name="opt" onchange="this.form.submit()">
          {
            for $type in ('', 'rangeIndex', 'sparql', 'ctsTriples')
            return 
              let $value := "search-options-" || $type || ".xml"
              return 
              <option value="{$value}">
                {if ($opt = $value) then (attribute selected {'selected'}) else()}
                {$type}
              </option>
          }
        </select>
        &#160;
        <button type="button" onclick="document.getElementById('q').value = ''; document.location.href='index.xqy?opt={$opt}'">reset</button>
        &#160;
        <input type="submit"/>
        </p>
        
      </form>
    </div>
    <div id="left">
    {local:facets()}
    </div>
    <div id="main">
        {
        if(string($results/@total) != '0') then(
          ( 
            <div id="statistics">
              Query: {seconds-from-duration(xs:duration($results/search:metrics/search:query-resolution-time))} s.&#160;&#160;
              Facet: {seconds-from-duration(xs:duration($results/search:metrics/search:facet-resolution-time))} s.&#160;&#160;
              Snippet: {seconds-from-duration(xs:duration($results/search:metrics/search:snippet-resolution-time))} s.&#160;&#160;
              Total: {seconds-from-duration(xs:duration($results/search:metrics/search:total-time))} s.&#160;&#160;
            </div>,
            local:pagination($results)),
            local:search-results()
          ) 
        else (<p style="text-align:center;font-weight:bold">Pas de résultats</p>)
        }
    </div>
  </body>
</html>
)

let $output.debug :=
<debug>{
  $results
}</debug>

return
if($debug = '') then ($output) else ($output.debug)