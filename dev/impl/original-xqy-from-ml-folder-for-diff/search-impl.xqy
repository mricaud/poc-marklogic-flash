xquery version "1.0-ml";

(: Copyright 2002-2020 MarkLogic Corporation.  All Rights Reserved. :)

module namespace impl = "http://marklogic.com/appservices/search-impl";

declare default function namespace "http://www.w3.org/2005/xpath-functions";

import module namespace ast = "http://marklogic.com/appservices/search-ast" at "ast.xqy";
import module namespace tdop     = "http://marklogic.com/parser/tdop" at "../utils/tdop.xqy";
import module namespace snip     = "http://marklogic.com/appservices/search-snippet" at "snippet.xqy";
import module namespace val      = "http://marklogic.com/appservices/search-values" at "values.xqy";
import module namespace hof      = "http://marklogic.com/higher-order" at "../utils/higher-order.xqy";
import module namespace functx   = "http://www.functx.com" at "/MarkLogic/functx/functx-1.0-nodoc-2007-01.xqy";
import module namespace json="http://marklogic.com/xdmp/json" at "/MarkLogic/json/json.xqy";
import module namespace schematron = "http://marklogic.com/xdmp/schematron"
      at "/MarkLogic/schematron/schematron.xqy";

import schema namespace opt = "http://marklogic.com/appservices/search" at "search.xsd";

import module namespace prns = "http://marklogic.com/appservices-util/preserve-ns"
    at "/MarkLogic/appservices/utils/preserve-ns.xqy";

import module namespace extrc = "http://marklogic.com/appservices/extract"
    at "/MarkLogic/appservices/search/extract.xqy";

import module namespace debug = "http://marklogic.com/debug" at "../utils/debug.xqy";

(: Search API implementation. Not for general use. See search.xqy for the public API listing :)

declare namespace cts    = "http://marklogic.com/cts";
declare namespace search = "http://marklogic.com/appservices/search";
declare namespace searchdev  = "http://marklogic.com/appservices/search/searchdev";
declare namespace error = "http://marklogic.com/xdmp/error";
declare namespace db = "http://marklogic.com/xdmp/database";
declare namespace qry = "http://marklogic.com/cts/query";
declare namespace svrl = "http://purl.oclc.org/dsdl/svrl";

declare option xdmp:mapping "false";

declare variable $impl:AS_DOCUMENTS as xs:int := 1;
declare variable $impl:AS_OBJECTS   as xs:int := 2;
declare variable $impl:AS_ELEMENTS  as xs:int := 3;

(:
Top Down Operator Precedence:
http://effbot.org/zone/simple-top-down-parsing.htm

Crockford: http://javascript.crockford.com/tdop/tdop.html

  A nud method is used by values (such as variables and literals) and by
  prefix operators. A led method is used by infix operators and suffix operators.
  A token may have both a nud method and a led  method. For example, - might be both
  a prefix operator (negation) and an infix operator (subtraction), so it would have
  both nud and led methods.

Here:
  starter == nud
  joiner == led
 
:)
(: here is the default config for google-style searches
though it would be possible to plug in a different table
(with accompanying definitions of needed starters/joiners)
to do verity-style search, or anything else :)


declare variable $impl:XML_SCHEMA_NS_URI
        := "http://www.w3.org/2001/XMLSchema";
declare variable $impl:UNICODE-CP-COLLATION
        := "http://www.w3.org/2005/xpath-functions/collation/codepoint";

declare variable $COLLATION-TEST-STRING1 := "&#xF701;&#xF702;&#xF703;&#xF704;";
declare variable $COLLATION-TEST-STRING2 := "&#xF705;&#xF706;&#xF707;&#xF708;";

declare variable $impl:default-per-match-tokens  :=  30;
declare variable $impl:default-max-matches       :=   4;
declare variable $impl:default-max-snippet-chars := 200;

declare variable $default-options := 
<search:options xmlns:search="http://marklogic.com/appservices/search">
    <search:concurrency-level>8</search:concurrency-level>
    <search:debug>0</search:debug>
    <search:page-length>10</search:page-length>
    <search:search-option>score-logtfidf</search:search-option>
    <search:quality-weight>1.0</search:quality-weight>
    <search:return-aggregates>true</search:return-aggregates>
    <search:return-constraints>false</search:return-constraints>
    <search:return-facets>true</search:return-facets>
    <search:return-frequencies>true</search:return-frequencies>
    <search:return-qtext>true</search:return-qtext>
    <search:return-query>false</search:return-query>
    <search:return-results>true</search:return-results>
    <search:return-metrics>true</search:return-metrics>
    <search:return-plan>false</search:return-plan>
    <search:return-similar>false</search:return-similar>
    <search:return-values>true</search:return-values>
    <search:transform-results apply="snippet">
        <search:per-match-tokens>{$impl:default-per-match-tokens}</search:per-match-tokens>
        <search:max-matches>{$impl:default-max-matches}</search:max-matches>
        <search:max-snippet-chars>{$impl:default-max-snippet-chars}</search:max-snippet-chars>
        <search:preferred-matches/>
    </search:transform-results>
    <search:searchable-expression>fn:collection()</search:searchable-expression>
    <search:sort-order direction="descending">
        <search:score/>
    </search:sort-order>
    <search:term apply="term">
        <search:empty apply="all-results"/>
    </search:term>
    <search:grammar>
        <search:quotation>"</search:quotation>
        <search:implicit><cts:and-query strength="20"/></search:implicit>
        <search:starter strength="30" apply="grouping" delimiter=")">(</search:starter>
        <search:starter strength="40" apply="prefix" element="cts:not-query">-</search:starter>
        <search:joiner  strength="10" apply="infix" element="cts:or-query" tokenize="word">OR</search:joiner>
        <search:joiner  strength="20" apply="infix" element="cts:and-query" tokenize="word">AND</search:joiner>
        <search:joiner  strength="30" apply="infix" element="cts:near-query" tokenize="word">NEAR</search:joiner>
        <search:joiner  strength="30" apply="near2" consume="2" element="cts:near-query">NEAR/</search:joiner>
        <search:joiner  strength="32" apply="boost" element="cts:boost-query" tokenize="word">BOOST</search:joiner>
        <search:joiner  strength="35" apply="not-in" element="cts:not-in-query" tokenize="word">NOT_IN</search:joiner>
        <search:joiner  strength="50" apply="constraint">:</search:joiner>
        <search:joiner  strength="50" apply="constraint" compare="LT" tokenize="word">LT</search:joiner>
        <search:joiner  strength="50" apply="constraint" compare="LE" tokenize="word">LE</search:joiner>
        <search:joiner  strength="50" apply="constraint" compare="GT" tokenize="word">GT</search:joiner>
        <search:joiner  strength="50" apply="constraint" compare="GE" tokenize="word">GE</search:joiner>
        <search:joiner  strength="50" apply="constraint" compare="NE" tokenize="word">NE</search:joiner>
    </search:grammar>
</search:options>;

declare variable $search-options-group :=
<option-group name="search-option">
    <exact-value group="1">filtered</exact-value>
    <exact-value group="1">unfiltered</exact-value>
    <exact-value group="2">checked</exact-value>
    <exact-value group="2">unchecked</exact-value>
    <exact-value group="3">score-logtfidf</exact-value>
    <exact-value group="3">score-logtf</exact-value>
    <exact-value group="3">score-simple</exact-value>
    <exact-value group="3">score-random</exact-value>
    <exact-value group="3">score-zero</exact-value>
    <exact-value group="4">format-json</exact-value>
    <exact-value group="4">format-xml</exact-value>
    <exact-value group="4">format-text</exact-value>
    <exact-value group="4">format-binary</exact-value>
    <exact-value group="5">relevance-trace</exact-value>
</option-group>;

(: == Tokenizer ================================= :)

(:
   input: a raw query string
   output: a sequence of <searchdev:tok>
:)
declare function impl:tokenize($input as xs:string, $options as element(opt:options), $incomp as xs:boolean) as element(searchdev:tok)* {
    (
    let $input := replace($input, "(^\s+)|(\s+$)", "")
    let $strlen := string-length($input)
    let $quot := $options/opt:grammar/opt:quotation/string()
    let $quotlen := string-length($quot)
    let $pos := 1

    let $startermap := map:map()
    let $starters := for $rule in $options/opt:grammar/opt:starter
                     where contains($input, $rule) (: at quick first pass, throw out any starters that simply don't appear anywhere :)
                     order by string-length($rule) descending
                     return (map:put($startermap, $rule, ($rule, (empty($rule/@tokenize) or $rule/@tokenize ne 'word'))), string($rule))
    let $joinermap := map:map()
    let $joiners := for $rule in $options/opt:grammar/opt:joiner
                    where contains($input, $rule) (: at quick first pass, throw out any joiners that simply don't appear anywhere :)
                    order by string-length($rule) descending
                    return (map:put($joinermap, $rule, ($rule, (empty($rule/@tokenize) or $rule/@tokenize ne 'word'))), string($rule))
    let $delims :=  for $delim in $options/opt:grammar/opt:starter/@delimiter
                    where contains($input, $delim) (: at quick first pass, throw out any delimiters that simply don't appear anywhere :)
                    order by string-length($delim) descending
                    return string($delim)
    let $qsplit := if (empty($quot) or $quot eq "")
                   then $input
                   else tokenize($input, functx:escape-for-regex($quot))  (: even-numbered items in this sequence are quoted :)
    let $splitlen := count($qsplit)
    for $segment at $segnum in $qsplit
    return if ($segnum mod 2 eq 0)
           then ( (: this is a quoted part of the qtext :)
                <searchdev:tok type="starter" from="{$pos}"><opt:quotation>{$quot}</opt:quotation></searchdev:tok>,
                if (string-length($segment) gt 0 or $segnum ne $splitlen)
                then <searchdev:tok type="term" from="{$pos + $quotlen}">{$segment}</searchdev:tok>
                else (), (:unterminated quote at the end :)
                xdmp:set($pos, $pos + string-length($segment) + $quotlen), (: one for start quote... :)
                if ($segnum eq $splitlen) (: only infer a closing delim if we're not at the end :)
                then ()
                else ( <searchdev:tok type="delim" from="{$pos}"><opt:quotation>{$quot}</opt:quotation></searchdev:tok>,
                       xdmp:set($pos, $pos + $quotlen)  (: ... one for end quote :)
                     )
                )
           else ( (: this is a non-quoted part of the qtext :)
                if (matches($segment, "^\s")) then xdmp:set($pos, $pos + 1) else (),
                let $normsegment := normalize-space($segment)
                let $chunks := tokenize( $normsegment, "\s") (: "chunk" = a bunch of close tokens :)
                for $c in $chunks
                return 
                       (impl:findtoks($c, string-length($c), $starters, $startermap, $joiners, $joinermap, $delims, $pos, $strlen, $incomp),
                       xdmp:set($pos, $pos + string-length($c) + 1)),
                if ($segment eq '' or matches($segment, "\s$")) then () else xdmp:set($pos, $pos - 1)
                (: we always add a space after a chunk, but at the end of a segment, it might not have been there :)
                )
    ),
        <searchdev:tok type="end"/>
};

(:~
  @param input the chunk (or sub-chunk in a recursive call) to tokenize
  @param strlen the length of $input
  @param ss all starters as a sequence
  @param smap all starters as a map string ==> (<starter>, $is_cleaver as xs:boolean)
  @param js   all joiners as a sequence
  @param jmap all joiners as a map string ==> (<joiner>, $is_cleaver as xs:boolean)
  @param ds   all delims as a sequence
  @param from the starting position of this chunk relative to the overall qtext (1-based)
  @param qtextlen the overall length of the input qtext
  @param incomp true if the qtext is incomplete (and it's therefore OK for a joiner to be at the end)
:)

declare function impl:findtoks($input as xs:string,
                 $strlen as xs:integer,
                 $ss as xs:string*, $smap as map:map,
                 $js as xs:string*, $jmap as map:map,
                 $ds as xs:string*,
                 $from as xs:integer,
                 $qtextlen as xs:integer,
                 $incomp as xs:boolean) as element(searchdev:tok)* {
    if ($input eq "")
    then ()
    else
        (: first look for joiners. Note that these are sorted longest-first :)
        (: the first or last token in the overall qtext may not be a joiner :)
        let $foundj := (
            for $join in $js
            where (
                     (
                         ($input eq $join) or 
                         (
                             (
                                 map:get($jmap, $join)[2] (: is_cleaver? :)
                             ) and 
                             contains($input, $join)
                          )
                      ) and
                      not($from eq 1 and
                          starts-with($input, $join)
                         ) and
                      ($incomp or
                      not ($strlen + $from - 1 eq $qtextlen and
                           ends-with($input, $join)
                          )
                      )
                  )
            return $join
        )
        return
            if (count($foundj) gt 0)
            then
                let $before := substring-before($input, $foundj[1])
                let $blen := string-length($before)
                let $after := substring-after($input, $foundj[1])
                let $alen := string-length($after)
                let $afrom := $from + $strlen - $alen
                return (
                    impl:findtoks($before, $blen, $ss, $smap, $js, $jmap, $ds, $from, $qtextlen, $incomp),
                    <searchdev:tok type="joiner" from="{$blen + $from}">{map:get($jmap, $foundj[1])[1]}</searchdev:tok>,
                    impl:findtoks($after,  $alen, $ss, $smap, $js, $jmap, $ds, $afrom, $qtextlen, $incomp)
                )
            else (: next look for starters. These are sorted longest-first also :)
                 (: the final token may not be a starter :)
                let $founds := (
                    for $start in $ss
                    where (
                              (
                                  ($input eq $start) or
                                  (
                                      (
                                          map:get($smap, $start)[2]
                                      ) and
                                      starts-with($input, $start)
                                  )
                              ) and
                              ($incomp or
                              not ($strlen + $from - 1 eq $qtextlen and
                                   ends-with($input, $start)
                                  )
                              )
                          )
                                return $start)
                return
                    if (count($founds) gt 0)
                    then
                        let $before := substring-before($input, $founds[1])
                        let $blen := string-length($before)
                        let $after := substring-after($input, $founds[1])
                        let $alen := string-length($after)
                        let $afrom := $from + $strlen - $alen
                        return (
                            impl:findtoks($before, $blen, $ss, $smap, $js, $jmap, $ds, $from, $qtextlen, $incomp),
                            <searchdev:tok type="starter" from="{$blen + $from}">{map:get($smap, $founds[1])[1]}</searchdev:tok>,
                            impl:findtoks($after, $alen, $ss, $smap, $js, $jmap, $ds, $afrom, $qtextlen, $incomp)
                        )
                    else (: also find delims :)
                        let $foundd := (for  $delim in $ds where contains($input, $delim)
                                        return $delim)
                        return
                            if (count($foundd) gt 0)
                            then
                                let $before := substring-before($input, $foundd[1])
                                let $blen := string-length($before)
                                let $after := substring-after($input, $foundd[1])
                                let $alen := string-length($after)
                                let $afrom := $from + $strlen - $alen
                                return (
                                    impl:findtoks($before, $blen, $ss, $smap, $js, $jmap, $ds, $from, $qtextlen, $incomp),
                                    <searchdev:tok type="delim" from="{$blen + $from}">{$foundd[1]}</searchdev:tok>,
                                    impl:findtoks($after, $alen, $ss, $smap, $js, $jmap, $ds, $afrom, $qtextlen, $incomp)
                                )
                            else <searchdev:tok type="term" from="{$from}">{$input}</searchdev:tok>
};


(: == Untokenize ================================ :)

declare function impl:untokenize(
    $toks as element(searchdev:tok)*)
as xs:string
{
    impl:untokenize($toks,true())
};

declare function impl:untokenize(
    $toks as element(searchdev:tok)*,
    $trim as xs:boolean)
as xs:string
{
    let $string := 
        string-join(
            for $tok at $i in $toks
            return
                if ($tok[@type = "joiner"]/search:joiner[@tokenize eq "word"])
                then concat(" ",data($tok)," ")
                else if ($tok[@type eq "end"]) then ()
                else if (
                  $tok[@type eq "term"] and $toks[$i + 1][@type = "term"]
                  or $tok[@type eq "term"] and $toks[$i + 1]/search:starter[@apply = ("prefix","grouping")]
                  or $tok[@type eq "term"] and $toks[$i + 1]/search:joiner[@apply eq "near2"] 
                  or $tok[@type eq "term"] and $toks[$i - 1]/search:joiner[@apply eq "near2"]
                  or $tok[@type = "delim"] and $toks[$i + 1][@type = ("term","starter","joiner")]
                )
                then concat(data($tok)," ")
                else data($tok)
        ,"")
    return 
        if ($trim) then normalize-space($string)
        else $string 
};

(: == Parser ==================================== :)
(: here $ps represents parser-state :)

(:~ Get the whole options node :)
declare function impl:opts($ps as map:map) as element(opt:options) {
    tdop:additional-state($ps)
};

(:~ Get the symbol (grammar options fragment) of the current token :)
declare function impl:symbol-lookup($ps as map:map) as element()? {
    tdop:gt($ps)/* (: Can return empty for term, end tokens :)
};

(:~ Log a parse-time message :)
declare function impl:msg($ps as map:map, $n as node()) as empty-sequence() {
    map:put($ps, "msgs", (map:get($ps, "msgs"), $n))
};

(:~ Store an operator (query-wide, query-time option) :)
declare function impl:oper($ps as map:map, $n as node()) as empty-sequence() {
    map:put($ps, "operators", (map:get($ps, "operators"), $n))
};

(: Never check in code that calls this function, for debugging only :)
declare function impl:dump-state($ps as map:map) as empty-sequence() {
   let $is-debugging := $debug:DEBUG
   return (
       if ($is-debugging) then () else debug:set-flag(true()),
       for $key in map:keys($ps)
       return debug:log(concat($key, " => ", xdmp:describe(map:get($ps, $key)))),
       if ($is-debugging) then () else debug:set-flag(false())
       )
};

(: translate from <search:grammar> to what tdop:initialize-map() expects :)
declare function impl:configure-grammar-map-for-tdop() {
    let $config := map:map()
    let $starters := map:map()
    let $joiners := map:map()    
    let $_ := 
        (map:put($starters, "term", impl:starter-dispatch#1),
         map:put($starters,"delim", impl:starter-dispatch#1),
         map:put($starters,"starter",impl:starter-dispatch#1),
         map:put($starters,"end",impl:starter-dispatch#1))
    let $_ := map:put($joiners,"joiner",impl:joiner-dispatch#2)
    let $_ := 
        (map:put($config,"starters",$starters),
         map:put($config,"joiners",$joiners))        
    return $config
};

(:~
   @param $toks <searchdev:tok>*
   @param $opts options
   @param $pos the starting position in the token stream (0 default)
   @return a parse tree of cts elements
:)
declare function impl:parse($toks as element(searchdev:tok)*, $opts as element(opt:options), $pos as xs:integer?) as schema-element(cts:query)? {
    let $ps := tdop:initialize-map($toks, impl:configure-grammar-map-for-tdop(), $pos, $opts)
    return
        if ($toks[1]/@type eq "end")
        then impl:empty-dispatch($opts, ())
        else impl:do-parse($ps)
};

(: as a convenience, the 3-arity version of this function does an options merge.
   If you've already merged, then call do-tokenize-parse-nomerge :)
declare function impl:do-tokenize-parse($qtexts as xs:string+, $opts as element(opt:options), $incomplete as xs:boolean?) as schema-element(cts:query)? {
    let $merged-options := impl:merge-options($default-options, $opts)
    return impl:do-tokenize-parse-nomerge($qtexts, $merged-options, $incomplete)
};

declare function impl:do-tokenize-parse-nomerge($qtexts as xs:string+, $merged-options as element(opt:options), $incomplete as xs:boolean?) as schema-element(cts:query)? {
    let $incomplete := if (empty($incomplete)) then false() else $incomplete
    return
        if (count($qtexts) gt 1)
        then <cts:and-query qtextmulti="1">
                 { for $qtext in $qtexts return impl:parse(impl:tokenize($qtext, $merged-options, $incomplete), $merged-options, 0) }
             </cts:and-query>
        else impl:parse(impl:tokenize($qtexts, $merged-options, $incomplete), $merged-options, 0)
};

declare function impl:do-parse($ps as map:map) as schema-element(cts:query)? {
    let $_ := tdop:advance($ps)
    let $finaltree := impl:expressions($ps, 0, "")
    let $annotations := map:get($ps, "operators")
    let $messages := map:get($ps, "msgs")
    return if (empty($finaltree)) then impl:empty-dispatch(impl:opts($ps), ($annotations, $messages))
    (: TODO: add option for <return-query-warnings> and return a 2nd node here :)
    else
        element { name($finaltree) } {
            namespace xs {$impl:XML_SCHEMA_NS_URI}, 
            $finaltree/@*,
            $finaltree/*,
            $annotations,
            $messages
        }
};

(:~ Call expression() repeately until reaching an $until token, (empty str meaning end token)
    and join together the subexpressions
:)
declare function impl:expressions($ps as map:map, $rbp as xs:integer, $until as xs:string) as schema-element(cts:query)? { 
    let $subexpr := tdop:expression($ps, $rbp)
    return impl:inner-expressions($ps, $subexpr, $rbp, $until)
 };
 
declare function impl:inner-expressions($ps as map:map, $left as element()?, $rbp as xs:integer, $until as xs:string) as schema-element(cts:query)? {
    if (tdop:at-end($ps) (: always stop at the end:) or
        ($until ne "" and tdop:gt($ps) eq $until) (: we hit a stopping token :) )
    then
        if ($left instance of element(unknown-starter))
        then <cts:word-query qtextref="cts:text"><cts:text>{$left//text()}</cts:text></cts:word-query>
        else $left
    else
       let $subexpr := impl:expressions($ps, $rbp, $until)
       let $implicit := 
           if (empty($subexpr))
           then <cts:and-query strength="20" />
           else (impl:opts($ps)/opt:grammar/opt:implicit/*, <cts:and-query/>)[1]
       return element { $implicit/name() } { $implicit/@*, attribute qtextjoin {""}, attribute qtextgroup { impl:opts($ps)/opt:grammar/opt:starter[@apply eq "grouping"]/(string(), @delimiter/string()) },
           for $opt in $implicit/../@options/tokenize(normalize-space(.), "\s") return <cts:option>{$opt}</cts:option>,
           if ($left instance of element(unknown-starter))
           then <cts:word-query qtextref="cts:text"><cts:text>{$left//text()}</cts:text></cts:word-query>
           else $left,
           $subexpr
       }
};

(: A "simple" expression is an optimization that applies only when you know you need the single next token as a string, for example a bucket name :)
(: it ignores all binding powers, etc. so use with care :)
(: counterintuitively it can consume more than one token, for example to extract a quoted value :)
declare function impl:simple-expression($ps as map:map) as xs:string {
    let $current := tdop:gt($ps)
    let $_ := tdop:advance($ps)
    return
        if (exists($current/opt:quotation))
        then (tdop:gt($ps), tdop:advance($ps), tdop:advance($ps)) (: skip over quotes :)
        else $current
};


(: handle an empty token stream :)
declare function impl:empty-dispatch($opts as element(opt:options), $annots as element(cts:annotation)*) as schema-element(cts:query) {
    let $apply := $opts/opt:term/opt:empty/@apply
    return
        if ($apply eq "all-results") then impl:all-results($annots)
        else if ($apply eq "no-results") then impl:no-results($annots)
             else impl:all-results($annots) (: the default default :)
};

(: need to pass in state, even though it's empty, becaue it might be an annotation with useful metadata :)
declare function impl:all-results($annots as element(cts:annotation)*) as schema-element(cts:query) {
    <cts:and-query qtextempty="1" xmlns:cts="http://marklogic.com/cts">{$annots}</cts:and-query>
};

declare function impl:no-results($annots as element(cts:annotation)*) as schema-element(cts:query) {
    <cts:word-query qtextempty="1" xmlns:cts="http://marklogic.com/cts">{$annots}</cts:word-query>
};

(: take an already-parsed word-query, and get the text back :)
(: why not unparse()? Because that does too much in cases like [format:"plain text"] :)
(: why not just the string-join? Because that doesn't work with shotgun ORs :)
declare function impl:textonly($elem as element()?) as xs:string {
    if ($elem/@qtextconst)
    then $elem/@qtextconst (: intermediate stopgap: we look at only @qtextconst :)
    else string-join($elem//cts:text, " ")
};

declare function impl:joiner-dispatch($ps as map:map, $left as schema-element(cts:query)?) as schema-element(cts:query)? {
    let $symbol := impl:symbol-lookup($ps)
    let $opts := impl:opts($ps)
    let $fname := $symbol[self::opt:joiner]/@apply
    return
        if (impl:extension($symbol))
        then 
             let $fo := xdmp:function(QName($symbol/@ns, $fname), $symbol/@at)
             let $_ := map:put($ps, "strength", xs:integer($symbol/@strength))
             (: for the moment, only parser extensions are using the "strength" key, assume it will be stale after this joiner :)
             return xdmp:apply($fo, $ps, $left, $opts)
        else 
            if ($fname eq "ignore" or empty($fname)) then impl:joiner-ignore($ps, $left)
            else if ($fname eq "infix") then impl:joiner-infix($ps, $left)
            else if ($fname eq "near2") then impl:joiner-near2($ps, $left)
            else if ($fname eq "boost") then impl:joiner-boost($ps, $left)
            else if ($fname eq "not-in") then impl:joiner-not-in($ps, $left)
            else if ($fname eq "constraint") then impl:joiner-constraint($ps, $left)
            else error((),"SEARCH-APPLYUNDEFINED", string($fname))
};

declare function impl:joiner-ignore($ps as map:map, $left as schema-element(cts:query)?) as schema-element(cts:query)? {
    tdop:advance($ps),
    $left
};

declare function impl:joiner-infix($ps as map:map, $left as schema-element(cts:query)?) as schema-element(cts:query)? {
    let $symbol := impl:symbol-lookup($ps)
    
    (: optimization: tree flattening.
       Normally, a token stream at this point looks like [t1 OR t2] where t1 $left, OR is the joiner, and t2 an expression yet to be parsed
       
       but if it is say [t1 OR t2 OR t3 OR t4], we can accumulate (t2, t3) under the same OR query, then parse as usual on t4
    :)
    
    let $_ := tdop:advance($ps)
    return
        if (string($symbol) = ("AND", "OR"))
        then
            let $optimus-1 := impl:opt-pattern-1($ps, $symbol/@element, ())

            let $right := tdop:expression($ps, $symbol/@strength)
            return
                if ($right instance of element(unknown-starter))
                then $left
                else
                    element { xs:QName($symbol/@element) } {
                        attribute qtextjoin {$symbol/string()},
                        attribute strength {$symbol/@strength},
                        attribute qtextgroup { impl:opts($ps)/opt:grammar/opt:starter[@apply eq "grouping"]/(string(), @delimiter/string()) },
                        for $opt in $symbol/@options/tokenize(normalize-space(.), "\s") return <cts:option>{$opt}</cts:option>,
                        $left, $optimus-1, $right
                        }
        else
            let $right := tdop:expression($ps, $symbol/@strength)
            return
                if ($right instance of element(unknown-starter))
                then $left
                else
                    element { xs:QName($symbol/@element) } {
                        attribute qtextjoin {$symbol/string()},
                        attribute strength {$symbol/@strength},
                        attribute qtextgroup { impl:opts($ps)/opt:grammar/opt:starter[@apply eq "grouping"]/(string(), @delimiter/string()) },
                        for $opt in $symbol/@options/tokenize(normalize-space(.), "\s") return <cts:option>{$opt}</cts:option>,
                        $left, $right
                        }
};

(: Token stream might look like this:
      <tok type="term" from="1">t1</tok>
      <tok type="joiner" from="4"><joiner strength="10" apply="infix" element="cts:or-query" xmlns="...">OR</joiner></tok>
      <tok type="term" from="7">t2</tok>
      <tok type="joiner" from="10"><joiner strength="10" apply="infix" element="cts:or-query" xmlns="...">OR</joiner></tok>
      <tok type="term" from="13">t3</tok>
      <tok type="joiner" from="16"><joiner strength="10" apply="infix" element="cts:or-query" xmlns="...">OR</joiner></tok>
      <tok type="term" from="19">t4</tok>
      <tok type="end"/>

    If we're currently at position 2, interesting patterns we look for are:
        (term, joiner [with same @element]) (: opt-pattern-1 :)
        (term, joiner [thats a constraint], term, joiner [with same @element]) 
    :)

declare function impl:opt-pattern-1(
    $ps as map:map,
    $name as xs:QName,
    $accum as schema-element(cts:query)*)
{
 impl:opt-pattern-1($ps, $name, $accum, fn:false())
};
 
declare function impl:opt-pattern-1(
    $ps as map:map,
    $name as xs:QName,
    $accum as schema-element(cts:query)*,
    $ext as xs:boolean)
{
    let $ext-advance := 
        if ($ext) 
        then tdop:advance($ps)
        else ()
    let $tok1 := tdop:gt($ps)
    let $tok2 := tdop:gt($ps, 1)
    let $tok3 := tdop:gt($ps, 2)
    let $tok4 := tdop:gt($ps, 3)
    let $tok5 := tdop:gt($ps, 4)
    let $tok6 := tdop:gt($ps, 5)
    let $opts := impl:opts($ps)
    let $telem := $opts/opt:term[1]
    let $tname := $telem/@apply
  
    return
        if ($tok1/@type eq "term" and $tok2/search:joiner/@element eq $name)
        then
            if (impl:extension($telem))
            then
                (
                impl:opt-pattern-1(
                    $ps,
                    $name,
                    ($accum,
                    let $fn := xdmp:function(QName($telem/@ns, $tname), $telem/@at)
                    return xdmp:apply($fn, $ps, $opts)
                    ),
                fn:true())
                )
          
            else
                 (tdop:advance($ps),
                 tdop:advance($ps),
                 impl:opt-pattern-1(
                 $ps,
                 $name,
                 ($accum,
                 impl:do-term($tok1, impl:opts($ps)/opt:term[1],$ps)
                 ),
                 fn:false())
                 )
        else             
            if (($tok1/@type eq "term"  and $tok2/search:joiner/@apply = "constraint" 
                and $tok3/@type eq "term" and $tok4/search:joiner/@element eq $name)
                or 
                ($tok1/@type eq "term" and $tok2/search:joiner/@apply = "constraint"
                and $tok3[@type eq "starter"]/search:quotation and $tok4/@type eq "term"
                and $tok5[@type eq "delim"]/search:quotation 
                and $tok6/search:joiner/@element eq $name))
            then
                (
                 tdop:advance($ps),
                 impl:opt-pattern-1(
                 $ps,
                 $name,
                 ($accum,
                 impl:joiner-constraint($ps,
                     <cts:word-query qtextref="cts:text">
                     <cts:text>{string($tok1)}</cts:text>
                     </cts:word-query>)
                 ),
                 fn:true())
                 )
        else $accum 
 
};

declare function impl:joiner-near2($ps as map:map, $left as schema-element(cts:query)?) as schema-element(cts:query)? {
    let $symbol := impl:symbol-lookup($ps)
    let $_ := tdop:advance($ps)
    let $expr1 := impl:simple-expression($ps)
    let $expr2 := tdop:expression($ps, $symbol/@strength)
    let $dist := if ($expr1 castable as xs:double)
                 then $expr1
                 else (10, impl:msg($ps, <cts:annotation warning="SEARCH-IGNOREDQTEXT:[{$expr1}] is not a valid distance, using 10"/>))
    return
        if (empty($expr2))
        then ($left, impl:msg($ps, <cts:annotation warning="SEARCH-IGNOREDQTEXT:[{string($symbol)}{$expr1}{string($expr2)}]: expected two arguments"/>))
        else
            element { xs:QName($symbol/@element) } {
                attribute distance { $dist },
                attribute qtextjoin {concat($symbol/string(), $dist)},
                attribute strength {$symbol/@strength},
                for $opt in $symbol/@options/tokenize(normalize-space(.), "\s") return <cts:option>{$opt}</cts:option>,
                $left, $expr2
            }
};

declare function impl:joiner-boost($ps as map:map, $left as schema-element(cts:query)?) as schema-element(cts:query)? {
    let $symbol := impl:symbol-lookup($ps)
    let $_ := tdop:advance($ps)
    let $expr1 := tdop:expression($ps, $symbol/@strength)
    return
        if (empty($left))
        then ($left, impl:msg($ps, <cts:annotation warning="SEARCH-IGNOREDQTEXT:[{string($symbol)} {$expr1}]: expected two arguments"/>))
        else
            element { xs:QName($symbol/@element) } {
                attribute qtextjoin {concat($symbol/string())},
                attribute strength {$symbol/@strength},
                attribute qtextgroup { impl:opts($ps)/opt:grammar/opt:starter[@apply eq "grouping"]/(string(), @delimiter/string()) },
                for $opt in $symbol/@options/tokenize(normalize-space(.), "\s") return <cts:option>{$opt}</cts:option>,
                element cts:matching-query {
                    attribute qtextref { "schema-element(cts:query)" },
                    $left },
                element cts:boosting-query {
                    attribute qtextref { "schema-element(cts:query)" },
                    $expr1 }
            }
};

declare function impl:joiner-not-in($ps as map:map, $left as schema-element(cts:query)?) as schema-element(cts:query)? {
    let $symbol := impl:symbol-lookup($ps)
    let $_ := tdop:advance($ps)
    let $expr1 := tdop:expression($ps, $symbol/@strength)
    return
        if (empty($left))
        then ($left, impl:msg($ps, <cts:annotation warning="SEARCH-IGNOREDQTEXT:[{string($symbol)} {$expr1}]: expected two arguments"/>))
        else
            element { xs:QName($symbol/@element) } {
                attribute qtextjoin {concat($symbol/string())},
                attribute strength {$symbol/@strength},
                attribute qtextgroup { impl:opts($ps)/opt:grammar/opt:starter[@apply eq "grouping"]/(string(), @delimiter/string()) },
                for $opt in $symbol/@options/tokenize(normalize-space(.), "\s") return <cts:option>{$opt}</cts:option>,
                element cts:positive {
                    attribute qtextref { "schema-element(cts:query)" },
                    $left },
                element cts:negative {
                    attribute qtextref { "schema-element(cts:query)" },
                    $expr1 }
            }
};

declare function impl:joiner-constraint($ps as map:map, $left as schema-element(cts:query)?) as schema-element(cts:query)? {
    let $left-str :=
        let $left-orig := $left/cts:annotation/@ml-internal-orig
        return
            (: undo custom parsing of a constraint name :)
            if (exists($left-orig))
            then string($left-orig)
            else impl:textonly($left)
    let $opts := impl:opts($ps)
    let $matched-constraint := $opts/(opt:constraint|opt:operator)[@name eq string($left-str)][1]
    let $is-bucketed := $matched-constraint/opt:range[opt:bucket|opt:computed-bucket]
    let $symbol := impl:symbol-lookup($ps)
    let $compare := ($symbol/@compare/string(), "EQ")[1]
    let $_ := tdop:advance($ps)
    let $right := 
        (: if the next token is a bucketname, no sense in doing a full tdop:expression on it :)
        if ($is-bucketed) then () 
        else
            let $right-expr := tdop:expression($ps, $symbol/@strength - 1)
            let $right-orig :=
                if (count($right-expr) ne 1) then ()
                else $right-expr/cts:annotation/@ml-internal-orig
            return
                if (empty($right-orig))
                then $right-expr
                else (: undo custom parsing of a constrained term :)
                    <cts:word-query qtextref="cts:text">
                        <cts:text>{string($right-orig)}</cts:text>
                    </cts:word-query>
    let $bucket-name :=
        if ($is-bucketed)
        then impl:simple-expression($ps)
        else ""
    let $qtext-rhs :=
        if (empty($right))
        then $bucket-name
        else impl:textonly($right)
    let $qtext :=
        if ($symbol/@tokenize eq "word")
        then concat($left-str, " ", string($symbol), " ")
        else concat($left-str, string($symbol))
    return
        (: style note: intentionally flattenting indentation here :)
        if ($is-bucketed and $compare eq "EQ")
        then impl:joiner-constraint-range-bucketed($ps, $bucket-name, $matched-constraint, $qtext, $qtext-rhs)
        else if ($is-bucketed) (: no buckets with comparisons :)
        then impl:msg($ps, <cts:annotation warning="SEARCH-IGNOREDQTEXT:[{concat($qtext,$qtext-rhs)}]"/>)
        else if ($matched-constraint/opt:range)
        then impl:joiner-constraint-range-operator($ps, $right, $matched-constraint, $qtext, $compare)
        else if ($compare ne "EQ")
        then impl:msg($ps, <cts:annotation warning="SEARCH-IGNOREDQTEXT:[{concat($qtext, $qtext-rhs)}]"/>)
        else if ($matched-constraint/opt:collection and $compare eq "EQ")
        then impl:joiner-constraint-collection($ps, $right, $matched-constraint, $qtext)
        else if ($matched-constraint/opt:value and $compare eq "EQ")
        then impl:joiner-constraint-value($ps, $right, $matched-constraint, $qtext)
        else if ($matched-constraint/opt:word and $compare eq "EQ")
        then impl:joiner-constraint-word($ps, $right, $matched-constraint, $qtext)
        else if ($matched-constraint/opt:element-query and $compare eq "EQ")
        then impl:joiner-constraint-element($ps, $right, $matched-constraint, $qtext)
        else if ($matched-constraint/opt:container and $compare eq "EQ")
        then impl:joiner-constraint-container($ps, $right, $matched-constraint, $qtext)

        else if ($matched-constraint/opt:geo-elem and $compare eq "EQ")
        then impl:joiner-constraint-geo-elem($ps, $right, $matched-constraint, $qtext)
        else if ($matched-constraint/opt:geo-elem-pair and $compare eq "EQ")
        then impl:joiner-constraint-geo-elem-pair($ps, $right, $matched-constraint, $qtext)
        else if ($matched-constraint/opt:geo-attr-pair and $compare eq "EQ")
        then impl:joiner-constraint-geo-attr-pair($ps, $right, $matched-constraint, $qtext)
        else if ($matched-constraint/opt:geo-json-property and $compare eq "EQ")
        then impl:joiner-constraint-geo-json-property($ps, $right, $matched-constraint, $qtext)
        else if ($matched-constraint/opt:geo-json-property-pair and $compare eq "EQ")
        then impl:joiner-constraint-geo-json-property-pair($ps, $right, $matched-constraint, $qtext)
        else if ($matched-constraint/opt:geo-path and $compare eq "EQ")
        then impl:joiner-constraint-geo-path($ps, $right, $matched-constraint, $qtext)

        else if ($matched-constraint/opt:properties and $compare eq "EQ")
        then impl:joiner-constraint-properties($ps, $right, $matched-constraint, $qtext)
        else if ($matched-constraint/opt:custom)
        then impl:joiner-constraint-custom($ps, $right, $matched-constraint, $qtext)
        else if ($matched-constraint[local-name(.) eq "operator"])
        then impl:joiner-operator($ps, $right, $matched-constraint, $qtext)
        else (: unrecognized constraint :) if ($right/@qtextpre)

        (: if quoted, keep just the quoted part, rest as annotations, 
        e.g. [foo:"abc def"] becomes a word query for "abc def" :)
        then (
            impl:msg($ps,<cts:annotation warning="SEARCH-IGNOREDQTEXT:dropping unrecognized constraint prefix [{$qtext}]"/>),
            <cts:word-query qtextpre="{$left-str}{string($symbol)}{$right/@qtextpre}" qtextref="cts:text">{$right/@qtextpost}{impl:make-text-with-options($qtext-rhs, ())}</cts:word-query>)

        (: treat as literal text, e.g. [http://example.com]  :)
        else impl:do-term(concat($left-str, string($symbol), $qtext-rhs), $ps) 
};

declare function impl:joiner-operator($ps as map:map, $right as element()?, $matched-constraint as element(), $qtext as xs:string) as schema-element(cts:query)? {
    let $operator-ref := $matched-constraint/@name
    let $state-ref := impl:textonly($right)
    let $placeholder := 
        if ($matched-constraint/opt:state[@name eq $state-ref]) 
        then impl:oper($ps, <cts:annotation operator-ref="{$operator-ref}" state-ref="{$state-ref}" qtextconst="{concat($qtext,$state-ref)}"/>)
        else impl:msg($ps, <cts:annotation warning="SEARCH-IGNOREDQTEXT:[{concat($qtext,$state-ref)}]"/>)
    return $placeholder
};

declare function impl:joiner-constraint-custom(
    $ps         as map:map,
    $right      as element(),
    $constraint as element(),
    $qtext      as xs:string
) as schema-element(cts:query)? {
    impl:custom-parse(impl:opts($ps), $constraint/opt:custom, $right, $qtext)
};


(: down one level to support naked terms :)
declare function impl:custom-parse(
    $options         as element(opt:options),
    $custom-elem     as element(opt:custom),
    $right           as element(),
    $qtext           as xs:string
) as schema-element(cts:query)?
{
    if (impl:extension($custom-elem/opt:parse))
    then 
        let $parse-fn     := xdmp:function(
            QName($custom-elem/opt:parse/@ns, $custom-elem/opt:parse/@apply), $custom-elem/opt:parse/@at
            )
        let $parsed-query :=
            if (function-arity($parse-fn) != 2)
            then error((),"SEARCH-BADEXTENSION")
            else if (not(xdmp:function-parameter-type($parse-fn, 1) = ("xs:string", "item()*")))
            then xdmp:apply(
                $parse-fn,
                <search:custom-constraint-query>
                    <search:constraint-name>{
                        substring($qtext, 1, (string-length($qtext) - 1))
                    }</search:constraint-name>
                    {
                        $right/cts:text/(
                            element search:text {text()}
                            )
                    }
                </search:custom-constraint-query>,
                $options
                )
            else xdmp:apply($parse-fn, $qtext, $right)
        let $ctsquery :=
            typeswitch($parsed-query)
                case schema-element(cts:query)
                    return $parsed-query
                case cts:query
                    return document{$parsed-query}/element()
                default return error((),"SEARCH-BADEXTENSION")
        return
            if (exists($ctsquery/cts:annotation/@ml-internal-orig))
            then $ctsquery
            else prns:insert-into-container(
                $ctsquery,(),(),<cts:annotation ml-internal-orig="{string($right)}"/>
                )
    else error((),"SEARCH-BADEXTENSION")

};

declare function impl:joiner-constraint-collection($ps as map:map, $right as element()?, $constraint as element(), $qtext as xs:string) as schema-element(cts:query)? {
    let $prefix := fn:data($constraint/opt:collection/@prefix)
    let $text := impl:textonly($right)
    let $ctsquery := 
        element cts:collection-query {
            if (string-length($prefix) gt 0)
            then (: prefix case :)
                 attribute qtextconst { concat($qtext, $text) }
            else (: no-prefix case :)
                 (
                 attribute qtextpre {$qtext},
                 attribute qtextref {"cts:annotation"},
                 element cts:annotation { $right/@qtextpre, attribute qtextref {"following-sibling::cts:uri"}, $right/@qtextpost }
                 ),

            element cts:uri {concat(string($prefix), $text)}
            (: no options allowed :)
        }
    return $ctsquery
};

declare function impl:joiner-constraint-range-bucketed($ps as map:map, $bucket-name as xs:string, $constraint as element(), $qtext as xs:string, $qtext-rhs as xs:string) as schema-element(cts:query)? {
    impl:construct-bucketed-range-query($ps,$constraint/opt:range,$bucket-name,$qtext,$qtext-rhs)
};

declare function impl:construct-bucketed-range-query(
    $ps as map:map,
    $range-elem as element(search:range),
    $bucket-name as xs:string,
    $qtext as xs:string?,
    $qtext-rhs as xs:string?
)
{
    let $matched-bucket := $range-elem/(opt:bucket|opt:computed-bucket)[@name eq $bucket-name][1]
    let $label := 
        if (exists($qtext))
        then concat($qtext, $qtext-rhs)
        else $bucket-name
    let $qtextconst := 
        if (exists($qtext))
        then attribute qtextconst {$label}
        else attribute qtextconst {$bucket-name}
    let $type := impl:range-type($range-elem)
    let $lt := 
        if ($matched-bucket/@lt)
        then 
            if ($range-elem/opt:attribute)
            then
                element cts:element-attribute-range-query {
                    $qtextconst,
                    attribute operator {"&lt;"},
                    impl:spec-cts-element-list($range-elem/opt:element),
                    impl:spec-cts-attribute-list($range-elem/opt:attribute),
                    element cts:value {
                        attribute xsi:type {$type},
                        impl:compute-bucket-boundary($matched-bucket/@lt,
                                                     $type,
                                                     ($matched-bucket/@lt-anchor, $matched-bucket/@anchor)[1],
                                                     local-name($matched-bucket),
                                                     $range-elem/opt:anchor)
                    },
                    if ($range-elem/@collation)
                    then <cts:option>{concat("collation=",data($range-elem/@collation))}</cts:option>
                    else (),
                    for $range-option in $range-elem/opt:range-option/string()
                    return <cts:option>{$range-option}</cts:option>
                }
            else
                if ($range-elem/opt:element)
                then
                    element cts:element-range-query {
                        $qtextconst,
                        attribute operator {"&lt;"},
                        impl:spec-cts-element-list($range-elem/opt:element),
                        element cts:value {
                            attribute xsi:type {$type},
                            impl:compute-bucket-boundary($matched-bucket/@lt,
                                                         $type,
                                                          ($matched-bucket/@lt-anchor, $matched-bucket/@anchor)[1],
                                                         local-name($matched-bucket),
                                                         $range-elem/opt:anchor)
                        },
                        if ($range-elem/@collation)
                        then <cts:option>{concat("collation=",data($range-elem/@collation))}</cts:option>
                        else (),
                        for $range-option in $range-elem/opt:range-option/string()
                        return <cts:option>{$range-option}</cts:option>
                    }
            else
                if ($range-elem/opt:field)
                then 
                    element cts:field-range-query {
                        $qtextconst,
                        attribute operator {"&lt;"},
                        element cts:field { string($range-elem/opt:field/@name)},
                        element cts:value {
                            attribute xsi:type {$type},
                            impl:compute-bucket-boundary($matched-bucket/@lt,
                                                         $type,
                                                          ($matched-bucket/@lt-anchor, $matched-bucket/@anchor)[1],
                                                         local-name($matched-bucket),
                                                         $range-elem/opt:anchor)
                        },
                        if ($range-elem/@collation)
                        then <cts:option>{concat("collation=",data($range-elem/@collation))}</cts:option>
                        else (),
                        for $range-option in $range-elem/opt:range-option/string()
                        return <cts:option>{$range-option}</cts:option>

                    }
                 else 
                    if ($range-elem/opt:path-index)
                    then 
                        element cts:path-range-query {
                            $qtextconst,
                            attribute operator {"&lt;"},
                            $range-elem/opt:path-index/(
                                element cts:path-expression {namespace::*, string(.)}
                                ),
                            element cts:value {
                                attribute xsi:type {$type},
                                impl:compute-bucket-boundary($matched-bucket/@lt,
                                                             $type,
                                                              ($matched-bucket/@lt-anchor, $matched-bucket/@anchor)[1],
                                                             local-name($matched-bucket),
                                                             $range-elem/opt:anchor)
                            },
                            if ($range-elem/@collation)
                            then <cts:option>{concat("collation=",data($range-elem/@collation))}</cts:option>
                            else (),
                            for $range-option in $range-elem/opt:range-option/string()
                            return <cts:option>{$range-option}</cts:option>
                        }
                    else ()    
        else ()
    let $ge :=  
        if ($matched-bucket/@ge)
        then
            if ($range-elem/opt:attribute)
            then
                element cts:element-attribute-range-query {
                    $qtextconst,
                    attribute operator {">="},
                    impl:spec-cts-element-list($range-elem/opt:element),
                    impl:spec-cts-attribute-list($range-elem/opt:attribute),
                    element cts:value {
                        attribute xsi:type {$type},
                        impl:compute-bucket-boundary($matched-bucket/@ge,
                                                     $type,
                                                     ($matched-bucket/@ge-anchor, $matched-bucket/@anchor)[1],
                                                     local-name($matched-bucket),
                                                     $range-elem/opt:anchor)
                    },
                    if ($range-elem/@collation)
                    then <cts:option>{concat("collation=",data($range-elem/@collation))}</cts:option>
                    else (),
                    for $range-option in $range-elem/opt:range-option/string()
                    return <cts:option>{$range-option}</cts:option>
                }
            else
                if ($range-elem/opt:element)
                then
                    element cts:element-range-query {
                        $qtextconst,
                        attribute operator {">="},
                        impl:spec-cts-element-list($range-elem/opt:element),
                        element cts:value {
                            attribute xsi:type {$type},
                            impl:compute-bucket-boundary($matched-bucket/@ge,
                                                         $type,
                                                         ($matched-bucket/@ge-anchor, $matched-bucket/@anchor)[1],
                                                         local-name($matched-bucket),
                                                         $range-elem/opt:anchor)
                        },
                        if ($range-elem/@collation)
                        then <cts:option>{concat("collation=",data($range-elem/@collation))}</cts:option>
                        else (),
                        for $range-option in $range-elem/opt:range-option/string()
                        return <cts:option>{$range-option}</cts:option>
                    }
                else 
                    if ($range-elem/opt:field)
                    then
                        element cts:field-range-query {
                            $qtextconst,
                            attribute operator {">="},
                            element cts:field { string($range-elem/opt:field/@name)},
                            element cts:value {
                                attribute xsi:type {$type},
                                impl:compute-bucket-boundary($matched-bucket/@ge,
                                                             $type,
                                                              ($matched-bucket/@ge-anchor, $matched-bucket/@anchor)[1],
                                                             local-name($matched-bucket),
                                                             $range-elem/opt:anchor)
                            },
                            if ($range-elem/@collation)
                            then <cts:option>{concat("collation=",data($range-elem/@collation))}</cts:option>
                            else (),
                            for $range-option in $range-elem/opt:range-option/string()
                            return <cts:option>{$range-option}</cts:option>
                        }
                    else 
                        if ($range-elem/opt:path-index)
                        then 
                            element cts:path-range-query {
                                $qtextconst,
                                attribute operator {">="},
                                $range-elem/opt:path-index/(
                                    element cts:path-expression {namespace::*, string(.)}
                                    ),
                                element cts:value {
                                    attribute xsi:type {$type},
                                    impl:compute-bucket-boundary($matched-bucket/@ge,
                                                                 $type,
                                                                  ($matched-bucket/@ge-anchor, $matched-bucket/@anchor)[1],
                                                                 local-name($matched-bucket),
                                                                 $range-elem/opt:anchor)
                                },
                                if ($range-elem/@collation)
                                then <cts:option>{concat("collation=",data($range-elem/@collation))}</cts:option>
                                else (),
                                for $range-option in $range-elem/opt:range-option/string()
                                return <cts:option>{$range-option}</cts:option>
                            }
                        else ()    
        else ()
    
    let $ctsquery := 
        if (empty($matched-bucket))
        then impl:msg($ps, <cts:annotation warning="SEARCH-IGNOREDQTEXT:[{$label}]"/>) (: no bucket match, ignore entire constraint :)
        else if (empty($ge))
             then $lt
             else if (empty($lt))
                  then $ge
                  else element cts:and-query { $qtextconst, $ge, $lt} 

    return
        if ($range-elem/opt:fragment-scope[. eq 'properties'])
        then <cts:properties-fragment-query qtextref="schema-element(cts:query)">{$ctsquery}</cts:properties-fragment-query>
        else
            if (impl:opts($ps)/opt:fragment-scope[. eq 'properties'] and $range-elem/opt:fragment-scope[. eq 'documents'])
            then <cts:document-fragment-query qtextref="schema-element(cts:query)">{$ctsquery}</cts:document-fragment-query>
            else $ctsquery
};

declare function impl:compute-bucket-boundary($val as attribute()?, $type as xs:string, $anchor, $bucket-element-name, $custom-anchors as element(opt:anchor)*) as item()? {
     if ($bucket-element-name eq "computed-bucket")
     then
         if ($anchor)
         then
             let $calculated-anchor := impl:resolve-anchor($anchor, $type, $custom-anchors)
             let $resolved-anchor := if ($type eq "xs:dateTime")
                then fn:adjust-dateTime-to-timezone($calculated-anchor cast as xs:dateTime,())
                else fn:adjust-date-to-timezone($calculated-anchor cast as xs:date,())
             return
                 if ($val castable as xs:dayTimeDuration)
                 then $resolved-anchor + xs:dayTimeDuration($val)
                 else $resolved-anchor + xs:yearMonthDuration($val)
         else
             if ($val)
             then error((),"SEARCH-NOANCHOR", ($bucket-element-name))
             else () (: if there is no @ge (for instance), the bucket is still valid, and no anchor needs exist :)
     else 
         if (empty($val)) 
         then () 
         else impl:cast($type,$val) 
};


declare function impl:joiner-constraint-range-operator($ps as map:map, $right as element()?, $constraint as element(), $qtext as xs:string?, $compare as xs:string?) as schema-element(cts:query)? {
    impl:construct-unbucketed-range-query($ps,$constraint/opt:range,$right,$qtext,$compare,())
};

declare function impl:construct-unbucketed-range-query(
    $ps as map:map, 
    $range-elem as element(opt:range),
    $right as element()?, 
    $qtext as xs:string?, 
    $compare as xs:string?,
    $s as xs:string?
) as schema-element(cts:query)? {
    let $range-operator :=
        if ($compare eq "LT")
        then "<"
        else if ($compare eq "LE")
        then "<="
        else if ($compare eq "GT")
        then ">"
        else if ($compare eq "GE")
        then ">="
        else if ($compare eq "NE")
        then "!="
        else "="
    let $val := 
        if (exists($right)) 
        then impl:textonly($right)
        else $s
    let $type := impl:range-type($range-elem)
    let $is-valid := xdmp:castable-as($impl:XML_SCHEMA_NS_URI, impl:type-name($type), $val)
    let $qtextpre := 
        if (exists($qtext))
        then attribute qtextpre {$qtext}
        else ()
    let $qtextref := 
        if (exists($qtext)) 
        then attribute qtextref {"cts:annotation"}
        else attribute qtextref {"cts:value"}
    let $annotation := 
        if (exists($right))
        then element cts:annotation {$right/@qtextpre, attribute qtextref {"following-sibling::cts:value"}, $right/@qtextpost}
        else ()
    let $ctsquery :=
        if ($range-elem/opt:element and $range-elem/opt:attribute)
        then
            element cts:element-attribute-range-query {
                $qtextpre,
                $qtextref,
                attribute operator {$range-operator},
                impl:spec-cts-element-list($range-elem/opt:element),
                impl:spec-cts-attribute-list($range-elem/opt:attribute),
                $annotation,
                element cts:value {
                    attribute xsi:type {$type},
                    $val
                },
                if ($range-elem/@collation)
                then <cts:option>{concat("collation=",data($range-elem/@collation))}</cts:option>
                else (),
                for $range-option in $range-elem/opt:range-option/string()
                return <cts:option>{$range-option}</cts:option>
            }
        else
            if ($range-elem/opt:element)
            then
                element cts:element-range-query {
                    $qtextpre,
                    $qtextref,
                    attribute operator {$range-operator},
                    impl:spec-cts-element-list($range-elem/opt:element),
                    $annotation,
                    element cts:value {
                        attribute xsi:type {$type},
                        $val
                    },
                    if ($range-elem/@collation)
                        then <cts:option>{concat("collation=",data($range-elem/@collation))}</cts:option>
                        else (),
                    for $range-option in $range-elem/opt:range-option/string()
                    return <cts:option>{$range-option}</cts:option>
                }
            else
            if ($range-elem/opt:field)
            then
                element cts:field-range-query {
                    $qtextpre,
                    $qtextref,
                    attribute operator {$range-operator},
                    $annotation,
                    element cts:field { string($range-elem/opt:field/@name) },
                    element cts:value {
                        attribute xsi:type {$type},
                        $val
                    },
                    if ($range-elem/@collation)
                    then <cts:option>{concat("collation=",data($range-elem/@collation))}</cts:option>
                    else (),
                    for $range-option in $range-elem/opt:range-option/string()
                    return <cts:option>{$range-option}</cts:option>
                }
            else 
            if ($range-elem/opt:path-index)
            then 
                element cts:path-range-query {
                    $qtextpre,
                    $qtextref,
                    attribute operator {$range-operator},
                    $annotation,
                    $range-elem/opt:path-index/(
                        element cts:path-expression {namespace::*, string(.)}
                        ),
                    element cts:value {
                        attribute xsi:type {$type},
                        $val
                    },
                    if ($range-elem/@collation)
                    then <cts:option>{concat("collation=",data($range-elem/@collation))}</cts:option>
                    else (),
                    for $range-option in $range-elem/opt:range-option/string()
                    return <cts:option>{$range-option}</cts:option>
                }
            else ()    
                
         return
             if ($is-valid)
             then
                 if ($range-elem/opt:fragment-scope[. eq 'properties'])
                 then <cts:properties-fragment-query qtextref="schema-element(cts:query)">{$ctsquery}</cts:properties-fragment-query>
                 else
                     if (impl:opts($ps)/opt:fragment-scope[. eq 'properties'] and $range-elem/opt:fragment-scope[. eq 'documents'])
                    then <cts:document-fragment-query qtextref="schema-element(cts:query)">{$ctsquery}</cts:document-fragment-query>
                    else $ctsquery
             else impl:msg($ps, <cts:annotation warning="SEARCH-IGNOREDQTEXT:[{$qtext}{$val}] is not a valid range constraint against type {$type}"/>)
};


declare function impl:joiner-constraint-value($ps as map:map, $right as element()?, $constraint as element(), $qtext as xs:string?) as schema-element(cts:query)? {
    impl:construct-value-query($ps,$constraint/opt:value,$right,$qtext,())
};

declare function impl:construct-value-query(
    $ps as map:map,
    $value-elem as element(opt:value), 
    $right as element()?, 
    $qtext as xs:string?,
    $s as xs:string?
) as schema-element(cts:query)? 
{
    let $qtextpre := 
        if (exists($qtext))
        then attribute qtextpre {$qtext}
        else ()
    let $qtextref := 
        if (exists($qtext))
        then attribute qtextref {"cts:annotation"}
        else attribute qtextref {"cts:text"}
    let $annotation := 
        if (exists($right)) 
        then element cts:annotation {$right/@qtextpre, attribute qtextref {"following-sibling::cts:text"}, $right/@qtextpost}
        else ()
    let $text := 
        if (exists($right))
        then impl:make-text-with-options(impl:textonly($right), $value-elem/opt:term-option)
        else impl:make-text-with-options($s,$value-elem/opt:term-option)
    let $weight := 
        if ($value-elem/opt:weight) 
        then attribute weight {$value-elem/opt:weight/string()} 
        else ()
        
    let $ctsquery := 
        if ($value-elem/opt:element and $value-elem/opt:attribute) then
            element cts:element-attribute-value-query {
                $weight,
                $qtextpre,
                $qtextref,
                impl:spec-cts-element-list($value-elem/opt:element),
                impl:spec-cts-attribute-list($value-elem/opt:attribute),
                $annotation,
                $text
            }
        else
            if ($value-elem/opt:element)
            then
                element cts:element-value-query {
                    $weight,
                    $qtextpre,
                    $qtextref,
                    impl:spec-cts-element-list($value-elem/opt:element),
                    $annotation,
                    $text
                }
        else
            element cts:field-value-query {
                $weight,
                $qtextpre,
                $qtextref,
                element cts:field { string($value-elem/opt:field/@name) },
                $annotation,
                $text               
            }
    return
        if ($value-elem/opt:fragment-scope[. eq 'properties'])
        then <cts:properties-fragment-query qtextref="schema-element(cts:query)">{$ctsquery}</cts:properties-fragment-query>
        else
            if (impl:opts($ps)/opt:fragment-scope[. eq 'properties'] and $value-elem/opt:fragment-scope[. eq 'documents'])
            then <cts:document-fragment-query qtextref="schema-element(cts:query)">{$ctsquery}</cts:document-fragment-query>
            else $ctsquery 
};

declare function impl:joiner-constraint-word($ps as map:map, $right as element()?, $constraint as element(), $qtext as xs:string?) as schema-element(cts:query)? {
    impl:construct-word-query($ps, $constraint/opt:word,$right,$qtext,())
};

declare function impl:construct-word-query(
    $ps as map:map,
    $word-elem as element(opt:word),
    $right as element()?,  (: this comes from constraint parsing :)
    $qtext as xs:string?, (: this comes from constraint parsing :)
    $s as xs:string?  (: this comes from term parsing :)
) as schema-element(cts:query)?
{
    let $qtextpre := 
        if (exists($qtext))
        then attribute qtextpre {$qtext}
        else ()
    let $qtextref := 
        if (exists($qtext))
        then attribute qtextref {"cts:annotation"}
        else attribute qtextref {"cts:text"}
    let $annotation := 
        if (exists($right)) 
        then element cts:annotation {$right/@qtextpre, attribute qtextref {"following-sibling::cts:text"}, $right/@qtextpost}
        else ()
    let $text := 
        if (exists($right))
        then impl:make-text-with-options(impl:textonly($right),$word-elem/opt:term-option)
        else impl:make-text-with-options($s,$word-elem/opt:term-option)
    let $weight := 
        if ($word-elem/opt:weight) 
        then attribute weight {$word-elem/opt:weight/string()} 
        else ()
    
    let $ctsquery := 
        if ($word-elem/opt:element and $word-elem/opt:attribute)
        then 
            element cts:element-attribute-word-query {
            $weight,
            $qtextpre,
            $qtextref,
            impl:spec-cts-element-list($word-elem/opt:element),
            impl:spec-cts-attribute-list($word-elem/opt:attribute),
            $annotation,
            $text
        }
        else if ($word-elem/opt:element)
             then
                 element cts:element-word-query {
                 $weight,
                 $qtextpre,
                 $qtextref,
                 impl:spec-cts-element-list($word-elem/opt:element),
                 $annotation,
                 $text
            }
             else if ($word-elem/opt:field)
                  then
                      element cts:field-word-query {
                      $weight,
                      $qtextpre,
                      $qtextref,
                      $annotation,
                      element cts:field {data($word-elem/opt:field/@name)},
                      $text
                  }
                  else 
                      element cts:word-query {
                      $weight,
                      $qtextpre,
                      $qtextref,
                      $annotation,
                      $text
                  }
    return
        if ($word-elem/opt:fragment-scope[. eq 'properties'])
        then <cts:properties-fragment-query qtextref="schema-element(cts:query)">{$ctsquery}</cts:properties-fragment-query>
        else
            if (impl:opts($ps)/opt:fragment-scope[. eq 'properties'] and $word-elem/opt:fragment-scope[. eq 'documents'])
            then <cts:document-fragment-query qtextref="schema-element(cts:query)">{$ctsquery}</cts:document-fragment-query>
            else $ctsquery
};

declare function impl:joiner-constraint-element($ps as map:map, $right as schema-element(cts:query), $constraint as element(), $qtext as xs:string) as schema-element(cts:query)? {
    let $ctsquery :=
        element cts:element-query {
            attribute qtextpre { concat($qtext) },
            attribute qtextref { "schema-element(cts:query)" },
            impl:spec-cts-element-list($constraint/opt:element-query),
            $right
        }
    return
        if ($constraint/opt:element-query/opt:fragment-scope[. eq 'properties'])
        then <cts:properties-fragment-query qtextref="schema-element(cts:query)">{$ctsquery}</cts:properties-fragment-query>
        else
            if (impl:opts($ps)/opt:fragment-scope[. eq 'properties'] and $constraint/opt:element-query/opt:fragment-scope[. eq 'documents'])
            then <cts:document-fragment-query qtextref="schema-element(cts:query)">{$ctsquery}</cts:document-fragment-query>
            else $ctsquery
};

declare function impl:joiner-constraint-container($ps as map:map, $right as schema-element(cts:query), $constraint as element(), $qtext as xs:string) as schema-element(cts:query)? {
    let $container := $constraint/opt:container
    let $ctsquery :=
        element cts:element-query {
            attribute qtextpre { concat($qtext) },
            attribute qtextref { "schema-element(cts:query)" },
            impl:spec-cts-element-list($container/opt:element),
            $right
        }
    return
        if ($container/opt:fragment-scope[. eq 'properties'])
        then <cts:properties-fragment-query qtextref="schema-element(cts:query)">{$ctsquery}</cts:properties-fragment-query>
        else
            if (impl:opts($ps)/opt:fragment-scope[. eq 'properties'] and $container/opt:fragment-scope[. eq 'documents'])
            then <cts:document-fragment-query qtextref="schema-element(cts:query)">{$ctsquery}</cts:document-fragment-query>
            else $ctsquery
};

declare function impl:joiner-constraint-geo-elem($ps as map:map, $right as schema-element(cts:query), $constraint as element(), $qtext as xs:string) as schema-element(cts:query)? {
    let $encoded-geo-query := impl:textonly($right)
    let $type := if (starts-with($encoded-geo-query, "@"))
                 then "cts:circle"
                 else if (starts-with($encoded-geo-query, "["))
                 then "cts:box"
                 else "cts:point"                 
    let $pair := $constraint/opt:geo-elem
    return
    if ($pair/opt:parent)
    then
        element cts:element-child-geospatial-query {
            attribute qtextpre { $qtext },
            attribute qtextref { "cts:annotation" },
            impl:spec-cts-element-list($pair/opt:parent),
            impl:spec-cts-child-list($pair/opt:element),
            element cts:annotation {
                attribute qtextpre { $right/@qtextpre },
                attribute qtextpost { $right/@qtextpost },
                attribute qtextref { "following-sibling::cts:region" }
                },
            element cts:region {
                attribute xsi:type { $type },
                $encoded-geo-query
                },
            for $opt in $pair/opt:geo-option
            return element cts:option { string($opt) } 
        }
    else
        element cts:element-geospatial-query {
            attribute qtextpre { $qtext },
            attribute qtextref { "cts:annotation" },
            impl:spec-cts-element-list($pair/opt:element),
            element cts:annotation {
                attribute qtextpre { $right/@qtextpre },
                attribute qtextpost { $right/@qtextpost },
                attribute qtextref { "following-sibling::cts:region" }
                },
            element cts:region {
                attribute xsi:type { $type },
                $encoded-geo-query
                },
            for $opt in $pair/opt:geo-option
            return element cts:option { string($opt) } 
      }
};

declare function impl:joiner-constraint-geo-path($ps as map:map, $right as schema-element(cts:query), $constraint as element(), $qtext as xs:string) as schema-element(cts:query)? {
    let $encoded-geo-query := impl:textonly($right)
    let $type := if (starts-with($encoded-geo-query, "@"))
                 then "cts:circle"
                 else if (starts-with($encoded-geo-query, "["))
                 then "cts:box"
                 else "cts:point"                 
    let $path := $constraint/opt:geo-path
    return
        (: todo namspaces :)
        element cts:path-geospatial-query {
            attribute qtextpre { $qtext },
            attribute qtextref { "cts:annotation" },
            $path/opt:path-index/(
                element cts:path-expression {namespace::*, string(.)}
                ),
            element cts:annotation {
                attribute qtextpre { $right/@qtextpre },
                attribute qtextpost { $right/@qtextpost },
                attribute qtextref { "following-sibling::cts:region" }
                },
            element cts:region {
                attribute xsi:type { $type },
                $encoded-geo-query
                },
            for $opt in $path/opt:geo-option
            return element cts:option { string($opt) } 
        }
};

declare function impl:joiner-constraint-geo-elem-pair($ps as map:map, $right as schema-element(cts:query), $constraint as element(), $qtext as xs:string) as schema-element(cts:query)? {
    let $encoded-geo-query := impl:textonly($right)
    let $type := if (starts-with($encoded-geo-query, "@"))
                 then "cts:circle"
                 else if (starts-with($encoded-geo-query, "["))
                 then "cts:box"
                 else "cts:point"                 
    let $pair := $constraint/opt:geo-elem-pair
    return
        element cts:element-pair-geospatial-query {
            attribute qtextpre { $qtext },
            attribute qtextref { "cts:annotation" },
            impl:spec-cts-element-list($pair/opt:parent),
            impl:spec-cts-latitude-list($pair/opt:lat),
            impl:spec-cts-longitude-list($pair/opt:lon),
            element cts:annotation {
                attribute qtextpre { $right/@qtextpre },
                attribute qtextpost { $right/@qtextpost },
                attribute qtextref { "following-sibling::cts:region" }
                },
            element cts:region {
                attribute xsi:type { $type },
                $encoded-geo-query
                },
            for $opt in $pair/opt:geo-option
            return element cts:option { string($opt) } 
        }
};

declare function impl:joiner-constraint-geo-attr-pair($ps as map:map, $right as schema-element(cts:query), $constraint as element(), $qtext as xs:string) as schema-element(cts:query)? {
    let $encoded-geo-query := impl:textonly($right)
    let $type := if (starts-with($encoded-geo-query, "@"))
                 then "cts:circle"
                 else if (starts-with($encoded-geo-query, "["))
                 then "cts:box"
                 else "cts:point"                 
    let $pair := $constraint/opt:geo-attr-pair
    return
        element cts:element-attribute-pair-geospatial-query {
            attribute qtextpre { $qtext },
            attribute qtextref { "cts:annotation" },
            impl:spec-cts-element-list($pair/opt:parent),
            impl:spec-cts-latitude-list($pair/opt:lat),
            impl:spec-cts-longitude-list($pair/opt:lon),
            element cts:annotation {
                attribute qtextpre { $right/@qtextpre },
                attribute qtextpost { $right/@qtextpost },
                attribute qtextref { "following-sibling::cts:region" }
                },
            element cts:region {
                attribute xsi:type { $type },
                $encoded-geo-query
                },
            for $opt in $pair/opt:geo-option
            return element cts:option { string($opt) } 
        }
};

declare function impl:joiner-constraint-geo-json-property($ps as map:map, $right as schema-element(cts:query), $constraint as element(), $qtext as xs:string) as schema-element(cts:query)? {
    let $encoded-geo-query := impl:textonly($right)
    let $type := if (starts-with($encoded-geo-query, "@"))
                 then "cts:circle"
                 else if (starts-with($encoded-geo-query, "["))
                 then "cts:box"
                 else "cts:point"                 
    let $pair := $constraint/opt:geo-json-property
    return
    if ($pair/opt:parent-property)
    then
        element cts:json-property-child-geospatial-query {
            attribute qtextpre { $qtext },
            attribute qtextref { "cts:annotation" },
            element cts:property { string($pair/opt:parent-property) },
            element cts:child-property { string($pair/opt:json-property) },
            element cts:annotation {
                attribute qtextpre { $right/@qtextpre },
                attribute qtextpost { $right/@qtextpost },
                attribute qtextref { "following-sibling::cts:region" }
                },
            element cts:region {
                attribute xsi:type { $type },
                $encoded-geo-query
                },
            for $opt in $pair/opt:geo-option
            return element cts:option { string($opt) } 
        }
    else
        element cts:json-property-geospatial-query {
            attribute qtextpre { $qtext },
            attribute qtextref { "cts:annotation" },
            element cts:property { string($pair/opt:json-property) },
            element cts:annotation {
                attribute qtextpre { $right/@qtextpre },
                attribute qtextpost { $right/@qtextpost },
                attribute qtextref { "following-sibling::cts:region" }
                },
            element cts:region {
                attribute xsi:type { $type },
                $encoded-geo-query
                },
            for $opt in $pair/opt:geo-option
            return element cts:option { string($opt) } 
      }
};

declare function impl:joiner-constraint-geo-json-property-pair($ps as map:map, $right as schema-element(cts:query), $constraint as element(), $qtext as xs:string) as schema-element(cts:query)? {
    let $encoded-geo-query := impl:textonly($right)
    let $type := if (starts-with($encoded-geo-query, "@"))
                 then "cts:circle"
                 else if (starts-with($encoded-geo-query, "["))
                 then "cts:box"
                 else "cts:point"                 
    let $pair := $constraint/opt:geo-json-property-pair
    return
        element cts:json-property-pair-geospatial-query {
            attribute qtextpre { $qtext },
            attribute qtextref { "cts:annotation" },
            element cts:property { string($pair/opt:parent-property) },
            element cts:latitude-property { string($pair/opt:lat-property) },
            element cts:longitude-property { string($pair/opt:lon-property) },
            element cts:annotation {
                attribute qtextpre { $right/@qtextpre },
                attribute qtextpost { $right/@qtextpost },
                attribute qtextref { "following-sibling::cts:region" }
                },
            element cts:region {
                attribute xsi:type { $type },
                $encoded-geo-query
                },
            for $opt in $pair/opt:geo-option
            return element cts:option { string($opt) } 
        }
};

declare function impl:joiner-constraint-properties($ps as map:map, $right as schema-element(cts:query), $constraint as element(), $qtext as xs:string) as schema-element(cts:query)? {
    element cts:properties-fragment-query {
        attribute qtextpre { concat($qtext) },
        attribute qtextref { "schema-element(cts:query)" },
        $right
    }
};

declare function impl:term-term($ps as map:map, $term-elem as element()?) as schema-element(cts:query)? {
    impl:do-term(string(tdop:gt($ps)), $term-elem, $ps),
    tdop:advance($ps)
};

declare function impl:do-term($s as xs:string, $ps as map:map?) as element()? {
    impl:do-term($s, impl:opts($ps)/opt:term, $ps)
};

declare function impl:do-term($s as xs:string, $term-elem as element()?, $ps as map:map?) as element()? {
    (: NB: fragment-scope is exclusive with @ref or any on-the-spot specifiers (opt:range, etc.) which can contain their own fragment-scope if needed :)
    if (exists($term-elem/opt:default)) then
        let $options := impl:opts($ps)
        let $constraint-ref := $options/opt:term/opt:default/@ref/string(.)
        let $default := 
            let $constraint :=
                if (empty($constraint-ref)) then ()
                else
                    let $constraint-def := $options/opt:constraint[@name eq $constraint-ref]
                    return
                        if (exists($constraint-def))
                        then $constraint-def
                        else impl:msg($ps,<cts:annotation warning="SEARCH-INVALIDREF:[{$constraint-ref}] is not a valid constraint name, defaulting to word query"/>) 
            let $allowed-child := $options/opt:term/opt:default/(opt:word|opt:value|opt:range)
            let $conflict := 
                if (empty($constraint) or empty($allowed-child)) then ()
                else impl:msg($ps,<cts:annotation warning="SEARCH-INVALIDDEFAULT: Conflicting default term configuration, using @ref"/>)
            return ($constraint/*,$allowed-child)[1]

        return 
            typeswitch($default)
                case element(opt:range) return 
                    if (exists($default/(opt:bucket|opt:computed-bucket))) then impl:construct-bucketed-range-query($ps,$default,$s,(),()) 
                    else  impl:construct-unbucketed-range-query($ps,$default,(),(),(),$s)
                case element(opt:value) return impl:construct-value-query($ps,$default,(),(),$s)  
                case element(opt:word) return impl:construct-word-query($ps, $default,(),(),$s)  
                case element(opt:custom)
                    return impl:custom-parse(
                        $options,
                        $default,
                        <cts:word-query qtextref="cts:text"><cts:text>{$s}</cts:text></cts:word-query>,
                        $constraint-ref
                        )
                default return impl:do-default-term($s,$ps,$term-elem) (: fall back to default term treatment if no def is found :)
    else impl:do-default-term($s,$ps,$term-elem)
};

declare function impl:do-default-term($s as xs:string, $ps as map:map?, $term-elem as element()?) as element() {
    let $wq := <cts:word-query qtextref="cts:text">
        { if ($term-elem/opt:weight) then attribute weight {$term-elem/opt:weight/string()} else () }
        { impl:make-text-with-options($s, $term-elem/opt:term-option) }
    </cts:word-query>
    return
        if ($term-elem/opt:fragment-scope[. eq 'properties'])
        then <cts:properties-fragment-query qtextref="schema-element(cts:query)">{$wq}</cts:properties-fragment-query>
        else
            if ((if (exists($ps)) then impl:opts($ps) else ())/opt:fragment-scope[. eq 'properties'] and $term-elem/opt:fragment-scope[. eq 'documents'])
            then <cts:document-fragment-query qtextref="schema-element(cts:query)">{$wq}</cts:document-fragment-query>
            else $wq
};

declare function impl:make-text-with-options($text as xs:string, $opts as element(opt:term-option)*) as element()* {
    let $lang := $opts[starts-with(., "lang=")]
    return (
        <cts:text>
            { if ($lang)
              then
                attribute xml:lang { substring-after($lang, "lang=", $impl:UNICODE-CP-COLLATION) }
              else
                ()
            }
            { $text }
        </cts:text>,
        for $opt in $opts where not(starts-with($opt, "lang=")) return <cts:option>{$opt/string()}</cts:option>
    )
};

declare function impl:starter-dispatch($ps as map:map) as schema-element(cts:query)? {
    let $symbol := impl:symbol-lookup($ps)
    let $opts := impl:opts($ps)
    let $felem := 
        if ($symbol[self::opt:starter]/@apply) then $symbol
        else ($opts/opt:term, <opt:term/>)[1]
    let $fname := $felem/@apply
    let $telem := $opts/opt:term[1]
    return
        if ($symbol/self::opt:quotation)
        then
            if (impl:extension($felem))
            then
                (: this partially duplicates code in starter-quotation :)
                let $fn := xdmp:function(QName($felem/@ns, $fname), $felem/@at)
                let $_ := tdop:advance($ps) (: to opening delim :)
                let $_ := map:put($ps, "strength", xs:integer($felem/@strength))
                let $parsed-phrase := xdmp:apply($fn, $ps, $opts)
                let $_skip-over-closing-delim := tdop:advance($ps)
                return $parsed-phrase
            else impl:starter-quotation($ps, $symbol, $telem)
        else if ($fname eq "grouping") then impl:starter-grouping($ps)
        else if ($fname eq "prefix") then impl:starter-prefix($ps)
        else if (tdop:gt($ps)/@type eq "end") then () (: this prevents an empty term at the end :)
        else if (impl:extension($felem))
             then
                 let $fn := xdmp:function(QName($felem/@ns, $fname), $felem/@at)
                 let $_ := map:put($ps, "strength", xs:integer($felem/@strength))
                 return xdmp:apply($fn, $ps, $opts)
             else impl:term-term($ps, $telem)
};

declare function impl:starter-quotation($ps as map:map, $symbol as element(opt:quotation), $term-elem as element(opt:term)?) as schema-element(cts:query)? {
    let $_ := tdop:advance($ps) (: past starting delim :)
    let $quoted-phrase := tdop:gt($ps)
    let $_ := tdop:advance($ps) (: past the phrase itself :)
    let $_ := tdop:advance($ps) (: past the closing delim :)
    let $term-query := impl:do-term($quoted-phrase, $term-elem, $ps) 
 	(: need to reconstruct and insert qtextpre and qtextpost for unparse :) 
 	return 
 	    element { node-name($term-query) } { 
 	        $term-query/@*, 
 	        attribute qtextpre {$symbol/string()}, 
 	        attribute qtextpost {$symbol/string()}, 
 	        $term-query/* 
 	    } 
};

declare function impl:starter-grouping($ps as map:map) as schema-element(cts:query)? {
    let $tok := tdop:gt($ps)
    let $_ := tdop:advance($ps) (: past opening delim :)
    let $phrase := impl:expressions($ps, 0, if ($tok/opt:starter/@delimiter) then $tok/opt:starter/@delimiter else ")")
    let $_ := tdop:advance($ps) (: past closing delim :)
    return $phrase
};

declare function impl:starter-prefix($ps as map:map) as schema-element(cts:query)? {
    let $sym := impl:symbol-lookup($ps)
    let $_ := tdop:advance($ps)
    let $right := tdop:expression($ps, $sym/@strength)
    return
        element { xs:QName($sym/@element) } {
            attribute qtextstart {concat($sym/string(), if ($sym/@tokenize eq 'word') then " " else "")},
            $sym/@strength,
            $right,
            for $opt in $sym/@options/tokenize(normalize-space(.), "\s") return <cts:option>{$opt}</cts:option>
        }
};

(: == Search ================================= :)
declare function impl:do-query(
    $qtext        as xs:string*,
    $deltaoptions as element(opt:options)?,
    $query        as item()?, 
    $start        as xs:unsignedLong?, 
    $page-length  as xs:unsignedLong?, 
    $raw-results  as xs:boolean
) {
    impl:do-query($qtext,$deltaoptions,$query,$start,$page-length,$raw-results,())
};

declare function impl:do-query(
    $qtext        as xs:string*,
    $deltaoptions as element(opt:options)?,
    $query        as item()?, 
    $start        as xs:unsignedLong?, 
    $page-length  as xs:unsignedLong?, 
    $raw-results  as xs:boolean,
    $default-func as map:map?
) {
    let $querydef := impl:prepare-querydef(
        $qtext,$deltaoptions,$query,$start,$page-length,$raw-results,$default-func
        )

    (: cache improves the performance of facet resolution :)
    let $return-results as xs:boolean := map:get($querydef,"return-results")
    let $retrieve-results             := ($return-results or $raw-results)
    let $results := 
        if ($retrieve-results)
        then impl:apply-search($querydef)
        else ()
    return (
        if ($raw-results) then ()
        else impl:do-response(
            $querydef,$results,impl:do-result-estimate($querydef,$retrieve-results,$results)
            ),

        if ($raw-results)
        then impl:extract-paths($querydef, $results)
        else ()
        )
};

declare function impl:do-convert-query(
    $deltaoptions as element(opt:options)?,
    $query        as element()?
) as cts:query?
{
    typeswitch($query)
    case schema-element(cts:query) return
        impl:convert-cts-query($deltaoptions,$query)
    case element(search:query) return
        impl:convert-structured-query($deltaoptions,$query)
    case cts:query return
        $query
    default return
        if (empty($query)) then ()
        else error((),"SEARCH-INVALARGS","query to convert must be search:query or cts:query")
};
declare private function impl:convert-cts-query(
    $deltaoptions as element(opt:options)?,
    $query        as schema-element(cts:query)
) as cts:query?
{
    let $nsmap         := map:map()
    let $path-bindings := impl:build-path-ns-bindings($nsmap,$query)
    let $nsbindings    := (
        if (empty($deltaoptions)) then ()
            else impl:build-ns-bindings($nsmap,$deltaoptions),
        $path-bindings
        )
    return impl:cts-query($query,$nsbindings)
};
declare private function impl:convert-structured-query(
    $deltaoptions as element(opt:options)?,
    $query        as element(search:query)
) as cts:query?
{
    let $init-options := impl:merge-options($default-options, $deltaoptions)
    let $cts-query    := ast:cts-query(ast:to-query($query,$init-options))
    return
        switch(count($cts-query))
        case 0 return ()
        case 1 return $cts-query
        default return cts:and-query($cts-query)
};

declare function impl:prepare-querydef(
    $deltaoptions as element(opt:options)?,
    $query        as item()?
) as map:map
{
    impl:prepare-querydef((),$deltaoptions,$query,(),(),false(),())
};

declare function impl:prepare-querydef(
    $qtext        as xs:string*,
    $deltaoptions as element(opt:options)?,
    $query        as item()?, 
    $start        as xs:unsignedLong?, 
    $page-length  as xs:unsignedLong?, 
    $raw-results  as xs:boolean,
    $default-func as map:map?
) as map:map
{
    let $t-minus-0 := xdmp:elapsed-time()

    (: merge incoming options once with defaults before parsing :)
    let $init-options := impl:merge-options($default-options, $deltaoptions)   

    let $nsmap         := map:map()
    let $path-bindings := impl:build-path-ns-bindings($nsmap,$query)
    let $nsbindings    := (
        if (empty($deltaoptions)) then ()
            else impl:build-ns-bindings($nsmap,$deltaoptions),
        $path-bindings
        )

    (: initialize a map that has query(cts), init-options,
    :  path-namespaces, annotations in it :)
    let $querydef :=
        typeswitch($query)
        case element(search:query) return
            ast:to-query($query,$init-options)
        case schema-element(cts:query) return
            map:map()
            =>map:with("qtext",$qtext)
            =>map:with("annotations",$query//cts:annotation)
            =>map:with("query",impl:cts-query($query,$nsbindings))
            =>map:with("options",$init-options)
            =>map:with("path-bindings",$path-bindings)
        case cts:query return
            map:map()
            =>map:with("qtext",$qtext)
(: TODO: extract annotations from cts:query
         skip subsequent operations that can't operate on cts:query'
 :)
            =>map:with("annotations",())
            =>map:with("query",$query)
            =>map:with("options",$init-options)
            =>map:with("path-bindings",$path-bindings)
        default return
            ast:to-query(ast:do-tokenize-parse-nomerge($qtext, $init-options, false()),$init-options)
            =>map:with("qtext",$qtext)

    (: create and merge final options with any state contained in the parsed query :)
    let $options := impl:apply-state($init-options,$querydef)

    (: proceed with query processing :) 
    let $return-results := head(($options/opt:return-results/xs:boolean(.), true()))
    let $return-facets  := head(($options/opt:return-facets/xs:boolean(.),  true()))
    let $return-metrics := head(($options/opt:return-metrics/xs:boolean(.), true()))

    let $extra-cts := $options/opt:additional-query/*
    let $quality-weight := $options/opt:quality-weight/data()
    let $forests := for $forest in $options/opt:forest return xs:unsignedLong($forest)
    let $page-length := if (empty($page-length)) then xs:integer($options/opt:page-length) else $page-length
    let $page-length := if ($page-length lt 0) then 0 else $page-length
    
    let $start := ($start[. ge 1], 1)[1]
    let $end := $start + $page-length - 1
    
    let $log := if (not($debug:DEBUG)) then () else debug:log(
        concat( "In impl:prepare-querydef, params: ", 
                   "qtext=", string-join($qtext, ","),
                   "query=",  xdmp:quote($query), 
                   "options=", xdmp:quote($options), 
                   "start=", $start, 
                   "end=", $end,
                   "raw-results=", $raw-results))

    let $parsed-query := map:get($querydef,"query")

    let $combined-query :=
        (: force no namespace
           TODO: or maybe xdmp:with-namespaces()?
         :)
        let $extra := impl:cts-query(<x xmlns="">{$extra-cts}</x>/*,$nsbindings)
        return  
            if (exists($parsed-query) and exists($extra-cts))
            then cts:and-query(($parsed-query,$extra)) 
            else ($parsed-query,$extra) 

    let $facetable  := $options/opt:constraint[(
        opt:collection|opt:range|opt:custom|opt:geo-elem-pair[opt:heatmap]|
        opt:geo-attr-pair[opt:heatmap]|opt:geo-elem[opt:heatmap]|
        opt:geo-json-property[opt:heatmap]|opt:geo-json-property[opt:heatmap]
        )[empty(@facet) or @facet eq true()]]
    let $search-opt := distinct-values((
            $options/opt:search-option/string(.),
            if ((empty($facetable) or $return-facets eq false())) then ()
            else "faceted"
            ))
    let $weight     :=
        if (exists($quality-weight)) 
            then $quality-weight 
            else xs:double($default-options/opt:quality-weight) 

    (: Update options (after applying state), add other bits for query processing.
     : Initialized map contains:  
     : $query (cts)
     : $options 
     : $annotations (parse warnings, etc)
     : $path-namespaces 
     :) 
    return (
        map:put($querydef, "default-func",   $default-func),
        map:put($querydef, "options",        $options),
        map:put($querydef, "nsmap",          $nsmap),
        map:put($querydef, "nsbindings",     $nsbindings),
        map:put($querydef, "combined-query", $combined-query),
        map:put($querydef, "search-opt",     $search-opt),
        map:put($querydef, "quality-weight", $weight),
        map:put($querydef, "forests",        $forests),
        map:put($querydef, "start",          $start),
        map:put($querydef, "end",            $end),
        map:put($querydef, "page-length",    $page-length),
        map:put($querydef, "return-results", $return-results),
        map:put($querydef, "return-facets",  $return-facets),
        map:put($querydef, "return-metrics", $return-metrics),
        map:put($querydef, "t-minus-0",      $t-minus-0),

        $querydef
        )
};

(: This is called directly from bulk read implementation, 
 : take care with refactoring :)
declare function impl:do-result-estimate(
    $querydef  as map:map,
    $retrieved as xs:boolean,
    $results   as node()*
) as xs:unsignedLong?
{
    let $start       as xs:unsignedLong := map:get($querydef, "start")
    let $page-length as xs:unsignedLong := map:get($querydef, "page-length")
    return
        (: when $start is 1, and there are no results, and we requested results
         : then we know the total is zero :)
        if ($start eq 1 and $page-length gt 0 and empty($results) and $retrieved)
        then 0
        else if (exists($results)) then
            let $resultCount := count($results)
            return
                (: when we have fewer results than the page length
                 : then we know the total from the start and results :)
                if ($resultCount < $page-length)
                then ($start + $resultCount) - 1
                else cts:remainder($results[1]) + $start - 1
        (: cache on estimate improves the performance of facet resolution; 
         : touching the estimate should force eager evaluation; also
         : provide estimate when getting metrics :)
        else if (map:get($querydef,"return-facets") or
            map:get($querydef,"return-metrics"))
        then impl:apply-search($querydef,"xdmp:estimate") + 0
        else () (: this is a corner case; total but no facets, results, or metrics :) 
};

declare function impl:do-response(
    $querydef as map:map,
    $results  as node()*,
    $estimate as xs:integer?
) {
    let $qtext          as xs:string*            := map:get($querydef,"qtext")
    let $default-func   as map:map?              := map:get($querydef, "default-func")
    let $options        as element(opt:options)? := map:get($querydef, "options")
    let $search-options as xs:string*            := map:get($querydef, "search-opt")
    let $quality-weight as xs:double?            := map:get($querydef, "quality-weight")
    let $nsmap          as map:map?              := map:get($querydef, "nsmap")
    let $nsbindings     as xs:string*            := map:get($querydef, "nsbindings")
    let $combined-query as cts:query             := map:get($querydef, "combined-query")
    let $forests        as xs:unsignedLong*      := map:get($querydef, "forests")
    let $start          as xs:unsignedLong       := map:get($querydef, "start")
    let $end            as xs:unsignedLong       := map:get($querydef, "end")
    let $page-length    as xs:unsignedLong       := map:get($querydef, "page-length")
    let $return-results as xs:boolean            := map:get($querydef, "return-results")
    let $return-facets  as xs:boolean            := map:get($querydef, "return-facets")
    let $return-metrics as xs:boolean            := map:get($querydef, "return-metrics")
    let $t-minus-0      as xs:dayTimeDuration    := map:get($querydef, "t-minus-0")
    let $path-bindings  as xs:string*            := map:get($querydef, "path-bindings")

    let $query-resolution-time := xdmp:elapsed-time() - $t-minus-0
 
    let $debug-mode         := head(($options/opt:debug/xs:boolean(.),              false()))
    let $return-constraints := head(($options/opt:return-constraints/xs:boolean(.), false()))
    let $return-plan        := head(($options/opt:return-plan/xs:boolean(.),        false()))
    let $return-qtext       := head(($options/opt:return-qtext/xs:boolean(.),       true()))
    let $return-query       := head(($options/opt:return-query/xs:boolean(.),       false()))
    let $return-similar     := head(($options/opt:return-similar/xs:boolean(.),     false()))
    let $extract-docs       :=
        if (map:get($querydef,"extract-inline") eq false()) then ()
        else head($options/opt:extract-document-data)

    let $warnings := ast:get-annotations($querydef)/@warning 
    let $timestamp := ast:get-annotations($querydef)/@timestamp

    let $transform-results := $options/opt:transform-results
    let $snippet-name      := $transform-results/@apply/string(.)
    let $snippet-extension := impl:extension($transform-results)
    let $snippet-format    := 
        if ($snippet-name eq "snippet" and $snippet-extension)
        then "custom"
        else $snippet-name

    let $zero-elapsed := xs:dayTimeDuration("PT0S")
    let $facet-start := $zero-elapsed
    let $facet-stop := $zero-elapsed
    let $metrics := map:map()
    return
        <search:response>{
            attribute {"snippet-format"} {$snippet-format},
            if (empty($estimate)) then ()
            else attribute {"total"} {$estimate},
            attribute {"start"} {$start},
            attribute {"page-length"} {$page-length},
            if (empty($timestamp)) then () 
            else attribute {"lsqt-query-timestamp"} {$timestamp}, 
            if (empty($extract-docs)) then ()
            else attribute {"selected"} {
                head(($extract-docs/@selected/string(.),"include"))
                },

            if (empty($results) or not($return-results)) then ()
            else
                let $snippet-func  :=
                    if (exists($snippet-name) and $snippet-extension) 
                    then xdmp:function(QName($transform-results/@ns, $snippet-name), 
                            $transform-results/@at)
                    else if (exists($default-func))
                    then map:get($default-func, "{http://marklogic.com/appservices/search}transform-results")
                    else ()
                let $resultdef     := map:map()
                return (
                    map:put($resultdef, "start",          $start),
                    map:put($resultdef, "nsmap",          $nsmap),
                    map:put($resultdef, "nsbindings",     $nsbindings),
                    map:put($resultdef, "returnSimilar",  $return-similar),
                    map:put($resultdef, "searchOptions",  $search-options),
                    if (empty($extract-docs)) then ()
                    else map:put($resultdef, "extractConfig", impl:extract-config($extract-docs)),
                    if (empty($snippet-name)) then ()
                    else map:put($resultdef, "snippetName", $snippet-name),
                    if (empty($snippet-extension)) then ()
                    else map:put($resultdef, "snippetExtension", $snippet-extension),
                    if (empty($snippet-func)) then ()
                    else map:put($resultdef, "snippetFunc", $snippet-func),
                    map:put($resultdef, "combined-query", $combined-query),

                    (: TODO: output as elements or JSON objects :)
                    impl:do-results($resultdef,$options,$results,$impl:AS_ELEMENTS,$metrics)
                    ),

            if ($return-facets) then
                let $constraints := impl:facet-constraints($options)
                return
                    if (empty($constraints)) then ()
                    else (
                        xdmp:set($facet-start, xdmp:elapsed-time()),
                        impl:do-resolve-facets(
                            $constraints, $options, $combined-query, head(($estimate,0))
                            ),
                        xdmp:set($facet-stop, xdmp:elapsed-time())
                        )
            else (),

            if ($return-plan) then
                <search:plan>{impl:apply-search($querydef,"xdmp:plan")}</search:plan>
            else (),

            if ($return-qtext)
            then 
                if ($qtext or ($qtext eq ""))
                then 
                    for $qt in $qtext 
                    return element search:qtext {$qt}
                else () 
            else (),

            if ($return-query)
            then prns:wrap(xs:QName("search:query"), ast:serialize-query($querydef))
            else (),
            
            if ($return-constraints)
            then $options/opt:constraint
            else (),

            if ($debug-mode) then (
                (: show debug messages, if any :)
                impl:do-check-options($options, true()),

                <search:report id="SEARCH-FLWOR">{impl:apply-search($querydef,(),true())}</search:report>
                )
            else (),

            if ($warnings)
            then for $warning in $warnings
                 return <search:warning id="{substring-before($warning, ":")}">{substring-after($warning, ":")}</search:warning>
            else (),
            
            if ($return-metrics) then element search:metrics {
                if (not($return-results)) then ()
                else element search:query-resolution-time {$query-resolution-time},

                if ($facet-stop eq $zero-elapsed) then ()
                else element search:facet-resolution-time {$facet-stop - $facet-start},

                if (empty($results) or not($return-results)) then ()
                else element search:snippet-resolution-time {
                    map:get($metrics,"snippet-elapsed")
                    },

                if (empty($options/opt:extract-metadata)) then ()
                else element search:metadata-resolution-time {
                    map:get($metrics,"meta-elapsed")
                    },

                if (empty($extract-docs)) then ()
                else element search:extract-resolution-time {
                    map:get($metrics,"extract-elapsed")
                    },

                element search:total-time { xdmp:elapsed-time() - $t-minus-0 }
            }
            else ()
        }</search:response>
};

declare function impl:do-results(
    $resultdef as map:map,
    $options   as element(search:options)?,
    $results   as node()+,
    $as        as xs:int,
    $metrics   as map:map?
) as item()*
{
    let $nsbindings        as xs:string*      := map:get($resultdef, "nsbindings")
    let $combined-query    as cts:query?      := map:get($resultdef, "combined-query")
    let $search-options    as xs:string*      := map:get($resultdef, "searchOptions")
    let $extract-config    as map:map?        := map:get($resultdef, "extractConfig")
    let $snippet-config    as map:map?        := map:get($resultdef, "snippetConfig")
    let $start             as xs:unsignedLong := head((map:get($resultdef, "start"), 1))
    let $return-similar    as xs:boolean      := head((map:get($resultdef, "returnSimilar"), false()))
    let $snippet-name      as xs:string?      :=
        if (empty($options)) then ()
        else map:get($resultdef, "snippetName")
    let $snippet-extension as xs:boolean?     :=
        if (empty($options)) then ()
        else map:get($resultdef, "snippetExtension")

    let $snippet-func      as function(*)?    :=
        if (empty($options)) then ()
        else map:get($resultdef, "snippetFunc")

    let $serialized-query                     :=
        if (empty($snippet-func) or empty($combined-query)) then ()
        else if ($snippet-extension)
        then <x>{$combined-query}</x>/*
        else if ($snippet-name eq "snippet")
        then <x>{$combined-query}</x>/*
        else ()

    let $result-decorator  :=
        if (empty($options)) then ()
        else $options/opt:result-decorator
    let $decorator-func    :=
        if (empty($result-decorator)) then ()
        else xdmp:function(
            QName($result-decorator/@ns, $result-decorator/@apply), $result-decorator/@at
            )
    let $decorator-arity   :=
        if (empty($decorator-func)) then ()
        else function-arity($decorator-func)
    let $extractrefs       := impl:make-extractrefs($options)
    let $extracted-results :=
        impl:extract-inline($resultdef,$results,$extract-config,$as,$metrics)
    let $relevance-trace   := ("relevance-trace" = $search-options)
    let $index-delta       := 1

        for $result at $i in $results
        let $result-uri   := xdmp:node-uri($result)
        let $index        := ($i + $start) - $index-delta
        let $path         := xdmp:describe($result)
        let $score        := cts:score($result)
        let $confidence   := cts:confidence($result)
        let $fitness      := cts:fitness($result)
        let $decoration   :=
            (: TODO: support $impl:AS_OBJECTS for search response as JSON output :)
            if (empty($decorator-func)) then ()
            else if ($decorator-arity eq 2)
            then xdmp:apply($decorator-func, $result-uri, $result)
            else if ($decorator-arity eq 1)
            then xdmp:apply($decorator-func, $result-uri)
            else error((),"SEARCH-BADEXTENSION")
        let $similar-uris :=
            if (not($return-similar)) then ()
            else impl:do-similar($result,$as)
        let $snippets     :=
            if (empty($serialized-query) and empty($snippet-func) and
                (exists($snippet-config) or $snippet-name eq "snippet"))
            then impl:snippet-dispatch(
                $result,
                if (exists($snippet-config))
                    then $snippet-config
                    else impl:snip-config($options/opt:transform-results),
                $combined-query,
                $as,
                $metrics
                )
            (: TODO: support $impl:AS_OBJECTS for search response as JSON output :)
            else if (exists($options))
            then impl:snippet-dispatch(
                $options,$snippet-name,$snippet-func,$combined-query,$serialized-query,
                $nsbindings,$result,$as,$metrics
                )
            else ()
        let $extract-meta :=
            (: TODO: support $impl:AS_OBJECTS for search response as JSON output :)
            if (empty($options)) then ()
            else impl:extract-metadata($result,$options,$extractrefs,$as,$metrics)
        let $extract-data :=
            if (empty($extracted-results)) then ()
            else $extracted-results[$i]
        let $relevance    :=
            if (not($relevance-trace)) then ()
            else
                switch($as)
                case $impl:AS_OBJECTS  return cts:relevance-info($result,"object")
                case $impl:AS_ELEMENTS return cts:relevance-info($result)
                default                return ()
        return
            switch($as)
            (: TODO: verify against transform of search response as JSON :)
            case $impl:AS_OBJECTS return
                let $result-map := map:map()
                let $no-search-metadata := (
                    ($score eq 0 and $confidence eq 0 and $fitness eq 0) or
                    empty($score) or empty($confidence) or empty($fitness)
                    )
                return (
                    map:put($result-map, "index", $index),
                    map:put($result-map, "uri",   $result-uri),

                    if ($no-search-metadata) then ()
                    else map:put($result-map, "score",      $score),
                    if ($no-search-metadata) then ()
                    else map:put($result-map, "confidence", $confidence),
                    if ($no-search-metadata) then ()
                    else map:put($result-map, "fitness",    $fitness),

                    (: TODO: $decoration for search response as JSON :)
                    if (empty($similar-uris)) then ()
                    else map:put($result-map, "similar",  $similar-uris),
                    if (empty($snippets)) then ()
                    else map:put($result-map, "matches",  $snippets),
                    (: TODO: $decoration for JSON only :)
                    if (empty($extract-meta)) then ()
                    else map:put($result-map, "metadata", $extract-meta),
                    if (empty($extract-data)) then ()
                    else if ($extract-data instance of xs:int and
                        $extract-data eq $extrc:EXTRACTED_NONE)
                    then map:put($result-map, "extractedNone", true())
                    else map:put($result-map, "extracted", $extract-data),
                    if (empty($relevance)) then ()
                    else map:put($result-map, "relevance", $relevance),

                    $result-map
                    )
            case $impl:AS_ELEMENTS return
                <search:result>{
                    attribute index {$index},
                    attribute uri {$result-uri},
                    attribute path {$path},
                    attribute score {$score},
                    attribute confidence {$confidence},
                    attribute fitness {$fitness},

                    $decoration,
                    $similar-uris,
                    $snippets,
                    $extract-meta,
                    $extract-data,
                    $relevance
                }</search:result>
            default return impl:debug-errmsg("unknown case for return results")
};

declare function impl:extract-config(
    $extract-docs as element(search:extract-document-data)?
) as map:map?
{
    if (empty($extract-docs)) then ()
    else
        let $map      := map:map()
        let $paths    := $extract-docs/opt:extract-path
        let $selected := head($extract-docs)/@selected/string(.)
        return (
            if (empty($selected)) then ()
            else map:put($map, "selected", $selected),
            if (empty($paths)) then ()
            else map:put($map, "paths",    $paths),
            $map
            )
};

declare function impl:extract-paths(
    $querydef as map:map,
    $results  as node()*
) as node()*
{
    if (empty($results)) then ()
    else 
        let $options := map:get($querydef,"options")
        let $config  := impl:extract-config($options/opt:extract-document-data)
        return
            if (empty($config) or map:get($config,"selected") eq "all")
            then $results
            else extrc:extract(
                $results,impl:ns-map($querydef),$config,$impl:AS_DOCUMENTS
                )
};

declare private function impl:extract-inline(
    $querydef as map:map,
    $results  as node()+,
    $config   as map:map?,
    $as       as xs:int,
    $metrics  as map:map?
) as json:array?
{
    if (empty($config)) then ()
    else
        let $metrics-elapsed :=
            if (empty($metrics)) then ()
            else map:get($metrics,"extract-elapsed")
        let $metrics-start   :=
            if (empty($metrics)) then ()
            else xdmp:elapsed-time()
        let $extracted := extrc:extract($results,impl:ns-map($querydef),$config,$as)
        return (
            if (count($extracted) gt 1 or not($extracted instance of json:array))
            then json:to-array($extracted)
            else
                let $extracted-array := json:array()
                return (
                    json:array-push($extracted-array,$extracted),
                    $extracted-array
                    ),

            if (empty($metrics)) then ()
            else map:put($metrics, "extract-elapsed",
                if (empty($metrics-elapsed))
                then (xdmp:elapsed-time() - $metrics-start)
                else ((xdmp:elapsed-time() + $metrics-elapsed) - $metrics-start)
                )
            )
};

declare function impl:apply-state($init-options as element(opt:options),$querydef as map:map)
as element(opt:options)
{
    let $state := ast:get-annotations($querydef)[@operator-ref]
    return
        if (empty($state))
        then $init-options
        else
            let $selected-options :=
                <search:options xmlns:search="http://marklogic.com/appservices/search">
                   {for $operator in $init-options/opt:operator
                    let $state-ref := $state[@operator-ref eq $operator/@name][last()]/@state-ref
                    let $selected := 
                        if ($state-ref) 
                        then ($operator/opt:state[@name eq $state-ref])
                        else ()
                    return $selected/*}
                </search:options>
            return impl:merge-options($init-options,$selected-options)
};

declare private function impl:do-similar(
    $result as node(),
    $as     as xs:int
) as item()*
{
    let $similar-list := subsequence(
        cts:search(collection(), cts:similar-query($result)),
        1, 5)
    return
        switch($as)
        case $impl:AS_OBJECTS return
            json:to-array(
                for $similar in $similar-list
                return xdmp:node-uri($similar)
                )
        case $impl:AS_ELEMENTS return
            for $similar in $similar-list
            return
                <search:similar>{
                    xdmp:node-uri($similar)
                }</search:similar>
        default return impl:debug-errmsg("unknown case for similar")
};

declare function impl:do-estimate($query as item(), $deltaoptions as element(opt:options)) {
    let $querydef := impl:prepare-querydef($deltaoptions,$query)

    return impl:apply-search($querydef, "xdmp:estimate")
};

declare function impl:apply-search(
    $querydef as map:map
) 
{
    impl:apply-search($querydef,(),false())
};

declare function impl:apply-search(
    $querydef as map:map,
    $wrapper as xs:string?
) as item()*
{
    impl:apply-search($querydef,$wrapper,false())
};

declare function impl:apply-search(
    $querydef as map:map,
    $wrapper as xs:string?,
    $to-string as xs:boolean
) as item()*
{
    let $opts           as element(opt:options)? := map:get($querydef, "options")
    let $search-opt     as xs:string*            := map:get($querydef, "search-opt")
    let $quality-weight as xs:double?            := map:get($querydef, "quality-weight")
    let $combined-query as cts:query             := map:get($querydef, "combined-query")
    let $forests        as xs:unsignedLong*      := map:get($querydef, "forests")
    let $start          as xs:unsignedLong       := map:get($querydef, "start")
    let $end            as xs:unsignedLong       := map:get($querydef, "end")
    let $orderby        := impl:build-order-by($opts)
    let $search-options := ($search-opt,$orderby)
    let $has-se         := ($opts/opt:searchable-expression ne "fn:collection()")

    return
        switch($wrapper)
            case "xdmp:plan"
                return
                    if ($has-se)
                    then impl:eval-search-string($querydef,"xdmp:plan")
                    else
                        xdmp:plan(
                            cts:search(
                                if ($opts/opt:fragment-scope eq 'properties')
                                then xdmp:document-properties()
                                else fn:collection(),
                                $combined-query,
                                $search-options,
                                $quality-weight,
                                $forests
                        ))
            case "xdmp:estimate"
                return
                    if ($has-se)
                    then impl:eval-search-string($querydef,"xdmp:estimate")
                    else
                        xdmp:estimate(
                            cts:search(
                                if ($opts/opt:fragment-scope eq 'properties')
                                then xdmp:document-properties()
                                else fn:collection(),
                                $combined-query,
                                $search-options,
                                $quality-weight,
                                $forests
                        ))

            default return
                    if ($to-string)
                    then impl:build-search-string($querydef,())
                    else
                        if ($has-se)
                        then impl:eval-search-string($querydef,())
                        else
                            (cts:search(
                                if ($opts/opt:fragment-scope eq 'properties')
                                then xdmp:document-properties()
                                else fn:collection(),
                                $combined-query,
                                $search-options,
                                $quality-weight,
                                $forests
                            ))[$start to $end]
};

declare private function impl:eval-search-string(
    $querydef   as map:map,
    $wrapper    as xs:string?
) as item()*
{
    let $opts as element(opt:options)? := map:get($querydef, "options")
    let $nsmap         := impl:ns-map($querydef)
    let $search-string := impl:build-search-string($querydef,$wrapper)
    return (
        if (xdmp:has-privilege("http://marklogic.com/xdmp/privileges/eval-search-string", "execute")) then ()
        else
            let $expr := $opts/opt:searchable-expression/sql:trim(string(.))
            let $is-expr-valid :=
                if (not(string-length($expr) gt 0))
                then true()
                else if (empty($nsmap))
                then cts:valid-extract-path($expr)
                else cts:valid-extract-path($expr,$nsmap)
            return
                if ($is-expr-valid) then ()
                else xdmp:security-assert("http://marklogic.com/xdmp/privileges/eval-search-string", "execute"),

        if (empty($nsmap))
        then xdmp:value($search-string)
        else xdmp:with-namespaces($nsmap,
            xdmp:value($search-string)
            )
        )
};

declare function impl:ns-map(
    $querydef as map:map
) as map:map?
{
    let $nsmap as map:map? := map:get($querydef,"nsmap")
    return
        if (exists($nsmap))
        then $nsmap
        else
            let $nsbindings as xs:string* := map:get($querydef,"nsbindings")
            return
                if (empty($nsbindings)) then ()
                else
                    let $map := impl:list-to-map($nsbindings)
                    return
                        if (empty($map)) then ()
                        else (
                            map:put($querydef,"nsmap",$map),
                            $map
                            )
};

declare function impl:list-to-map(
    $key-values as xs:string*
) as map:map?
{
    let $list-count := count($key-values)
    return
        if ($list-count eq 0) then ()
        else
            let $list := json:to-array($key-values)
            let $map  := map:map()
            return (
                for $i in (1 to ($list-count idiv 2))
                let $item-num := $i * 2
                return map:put($map, $list[$item-num - 1], $list[$item-num]),

                $map
                )
};

declare function impl:build-search-string(
    $querydef as map:map,
    $wrapper as xs:string?
) as xs:string
{
    let $opts           as element(opt:options)? := map:get($querydef, "options")
    let $search-opts    as xs:string*            := map:get($querydef, "search-opt")
    let $quality-weight as xs:double?            := map:get($querydef, "quality-weight")
    let $query          as cts:query             := map:get($querydef, "combined-query")
    let $forests        as xs:unsignedLong*      := map:get($querydef, "forests")
    let $start          as xs:unsignedLong       := map:get($querydef, "start")
    let $end            as xs:unsignedLong       := map:get($querydef, "end")
    let $orderby := impl:build-order-by($opts)
    return
    concat(
        if (exists($wrapper))
        then concat($wrapper,"(")
        else "(",
        "cts:search(", 
         if ($opts/opt:fragment-scope eq "properties")
         then "xdmp:document-properties()"
         else if ($opts/opt:searchable-expression)
         then ($opts/opt:searchable-expression/string(.))
         else "fn:collection()",
         ", ", 
         $query,
         ", (",
         if (exists($search-opts)) 
         then concat("""", string-join(for $opt in $search-opts return string($opt),""","""),"""") 
         else (),
         if (exists($orderby)) 
         then concat(",",string-join(for $o in $orderby return string($o),",")) 
         else (),
         ")",
         if ($quality-weight or $quality-weight = 0) 
         then concat(", ", xs:string($quality-weight)) 
         else 
             if ($forests) 
             then concat(", ", string(xs:double($default-options/opt:quality-weight))) 
             else (), 
         if ($forests) 
         then concat(", ", 
             if (count($forests) gt 1) 
             then concat("(", string-join(for $forest in $forests return xs:string($forest), ", "), ")") 
             else $forests
             )
         else (),
         ")",
         if (exists($wrapper))
         then ")"
         else concat(")[",$start," to ",$end,"]")
    )
};

declare function impl:build-order-by(
    $options as element()
) as item()*
{
    for $term in $options/opt:sort-order
    let $direction :=
        if (data($term/@direction) = ("ascending", "descending"))
        then data($term/@direction)
        else () 
    return 
        if      ($term/opt:score)
        then cts:score-order(head(($direction,"descending")))
        else if ($term/opt:score-order)
        then cts:score-order(head(($direction,"ascending")))
        else if ($term/opt:confidence-order)
        then cts:confidence-order(head(($direction,"ascending")))
        else if ($term/opt:document-order)
        then cts:document-order(head(($direction,"ascending")))
        else if ($term/opt:fitness-order)
        then cts:fitness-order(head(($direction,"ascending")))
        else if ($term/opt:quality-order)
        then cts:quality-order(head(($direction,"ascending")))
        else if ($term/opt:unordered)
        then cts:unordered()
        else 
            let $ref := 
                try { impl:construct-reference($term) }
                catch ($e) { impl:debug-error($e) }
            return 
                if (exists($ref))
                then cts:index-order($ref,$direction)
                else error((),"SEARCH-BADORDERBY",xdmp:quote($term))
};

(: ---- facet resolution---- :)

(: ---- collection facet resolution---- :)

(: No FLWOR let/return, no return type to maintain concurrency :)

declare function impl:collection-facet-start(
  $constraint as element(opt:constraint), 
  $query as cts:query?, 
  $facet-options as xs:string*, 
  $quality-weight as xs:double?,
  $forests as xs:unsignedLong*)
{
    if ($constraint/opt:collection/@prefix)
    then cts:collection-match( concat($constraint/opt:collection/@prefix, "*"), 
            ($facet-options, "concurrent"),$query,$quality-weight,$forests)
    else cts:collections((), ($facet-options,"concurrent"),$query,$quality-weight,$forests)
};

declare function impl:collection-facet-finish(
  $constraint as element(opt:constraint), 
  $start as item()*
  )
as element(opt:facet)?
{
    let $prefix := $constraint/opt:collection/@prefix
    return
        element search:facet {
            $constraint/@name,
            attribute type {"collection"},
            for $i in $start
            let $val := 
                if ($prefix) then substring-after($i,$prefix)
                else $i
            return element search:facet-value {
                attribute name {$val},
                attribute count {cts:frequency($i)},
                $val
            }
        }
};

(: ----- bucketed range facet resolution ---- :)

(: can we do a single bucket query, or must we do a series of queries ?:)
declare function impl:continuous-facet(
    $buckets as json:object) 
as xs:boolean 
{
    let $keys := map:keys($buckets)
    let $range-check :=
        sum(for $key at $pos in $keys
            return
                if ($pos eq 1)
                then 1
                else
                    let $this := map:get($buckets,$keys[$pos])
                    let $last := map:get($buckets,$keys[$pos - 1])
                    return 
                        if ((map:get($this,"ge") eq map:get($last,"lt")) 
                            or (map:get($this,"lt") eq map:get($last,"ge")))
                        then 1
                        else 0
        )
   return
        if ($range-check eq map:count($buckets))
        then true()
        else false()
};

declare function impl:resolve-buckets(
    $constraint as element(opt:constraint)
) as json:object 
{
    let $map := json:object()
    let $type := impl:range-type($constraint/opt:range)
    let $compute := 
        for $bucket at $pos in $constraint/opt:range/(opt:bucket|opt:computed-bucket)
            let $name := $bucket/@name
            let $lt := 
                if ($bucket/@lt)
                then impl:compute-bucket-boundary($bucket/@lt, $type, ($bucket/@lt-anchor, $bucket/@anchor)[1], local-name($bucket), $constraint/opt:range/opt:anchor)
                else ()
            let $ge := 
                if ($bucket/@ge)
                then impl:compute-bucket-boundary($bucket/@ge, $type, ($bucket/@ge-anchor, $bucket/@anchor)[1], local-name($bucket), $constraint/opt:range/opt:anchor)
            else ()
            let $inner := map:map()
            let $_ := map:put($inner,"lt",$lt)
            let $_ := map:put($inner,"ge",$ge)
            let $_ := map:put($inner,"name",$name)
            let $_ := map:put($inner,"data",data($bucket))
            let $_ := map:put($map,string($pos),$inner)
            return ()
    return $map
    
};

declare function impl:resolve-discontinuous-range-facet(
  $constraint as element(opt:constraint), 
  $buckets as json:object,
  $query as cts:query?,
  $facet-options as xs:string*,
  $quality-weight as xs:double?,
  $forests as xs:unsignedLong*)
as element(search:facet)?
{
    if (not($debug:DEBUG)) then () else debug:log(("in resolve-discontinuous-range-facet")),
    element search:facet { attribute name {$constraint/@name}, attribute type {"bucketed"}, 
        let $range        := $constraint/opt:range
        let $reference    := impl:construct-reference($range)
        for $bucket-name in map:keys($buckets)
        let $bucket       := map:get($buckets,$bucket-name)
        let $lt           := map:get($bucket,"lt")
        let $ge           := map:get($bucket,"ge")
        let $bounds       := ($ge,$lt) 
        let $start        := cts:value-ranges($reference,$bounds,$facet-options,$query,$quality-weight,$forests)
        return impl:discontinuous-range-bucket-finish($bucket, $ge, $lt, $start)
        }
};

(: Discontinuous buckets are not resolved concurrently :)

declare function impl:discontinuous-range-bucket-finish(
  $bucket as map:map,
  $ge as xs:anyAtomicType?,
  $lt as xs:anyAtomicType?,
  $start as item()*)
as element(search:facet-value)?
{
     let $selected :=
            if (exists($ge) and exists($lt))
            then
                if ($start[cts:lower-bound eq $ge][cts:upper-bound eq $lt])
                then $start[cts:lower-bound eq $ge][cts:upper-bound eq $lt]
                else ()
            else
                if (exists($ge))
                then 
                    if ($start[cts:lower-bound eq $ge])
                    then $start[cts:lower-bound eq $ge]
                    else ()
                else
                    if (exists($lt))
                    then 
                        if ($start[cts:upper-bound eq $lt])
                        then $start[cts:upper-bound eq $lt]
                        else ()
                    else ()
        return 
            if ($selected) then 
            element search:facet-value {
                 attribute name { map:get($bucket,"name")},
                 attribute count {cts:frequency($selected)},
                 map:get($bucket,"data")
            }
            else ()
};

(: No FLWOR let/return, no return type to maintain concurrency :)

declare function impl:continuous-range-facet-start(
  $constraint as element(opt:constraint),
  $buckets as json:object, 
  $query as cts:query?, 
  $facet-options as xs:string*,  
  $quality-weight as xs:double?,
  $forests as xs:unsignedLong*
  )
{
    cts:value-ranges(impl:construct-reference($constraint/opt:range),
        impl:bounds($buckets,$constraint),($facet-options,"concurrent"),$query,$quality-weight,$forests)
};

(:~ Extracts element QName from range constraint :)

declare function impl:range-element(
    $constraint as element(opt:constraint)
) as xs:QName*
{
    impl:spec-qname-list($constraint/opt:range/opt:element)
};

(:~ Extracts attribute QName from range constraint :)

declare function impl:range-attribute(
    $constraint as element(opt:constraint)
) as xs:QName*
{
    impl:spec-qname-list($constraint/opt:range/opt:attribute)
};

declare function impl:range-field(
    $constraint as element(opt:constraint)
) as xs:string?
{
    let $field := $constraint/opt:range/opt:field
    return
        if (empty($field)) then () 
        else string($field/@name)
};

(: Calculates bucket boundaries :)

declare function impl:bounds(
    $buckets as json:object,
    $constraint as element(opt:constraint)
)
{
    let $constraint-type := impl:range-type($constraint/opt:range)
    let $ascending := impl:ascending-constraint($buckets,$constraint-type) 
    let $bounds := map:get(impl:compute-bounds($buckets,$constraint-type),"vals")
    return
        if ($ascending) then $bounds
        else reverse($bounds)
};

(:~
 : Check direction sensitive to the attributes expressing the constraint
 :)
declare function impl:ascending-constraint(
    $buckets as json:object,
    $constraint-type as xs:string 
) as xs:boolean
{
    (: infer direction for unbounded continuous buckets :)
    let $keys     := map:keys($buckets)
    let $first   := map:get($buckets,$keys[1])
    let $last     := map:get($buckets,$keys[map:count($buckets)])
    return
        if      (exists(map:get($first,"lt")) and empty(map:get($first,"ge"))) then true()
        else if (empty(map:get($first,"lt")) and exists(map:get($first,"ge"))) then false() 
        else if (exists(map:get($last,"lt")) and empty(map:get($last,"ge"))) then false()
        else if (empty(map:get($last,"lt")) and exists(map:get($last,"ge"))) then true()
        (: check the first two @lt values for bounded continuous buckets, preserving the order :)
        else impl:ascending(
            $buckets,
            $constraint-type 
            ) 
};

(:~
 : Checks only the first two values for direction; 
 :  since it is used after the facet has been verified as continuous.
 :)
declare function impl:ascending(
    $buckets as json:object,
    $type as xs:string
) as xs:boolean
{
    let $vals := 
        for $key in (map:keys($buckets))[1 to 3]
        let $val := map:get($buckets,"key")
        return 
            if (exists($val))
            then map:get($val,"lt")
            else ()
    return 
        if (exists($vals[2])) then
            let $val1 := impl:cast($type,$vals[1]) 
            let $val2 := impl:cast($type,$vals[2]) 
            return
                if ($val1 < $val2) then true()
                else false() 	
        else true()
};


declare function impl:compute-bounds(
    $buckets as json:object,
    $constraint-type as xs:string
) as json:object 
{
    let $bounds := json:object()
    let $ascending := impl:ascending-constraint($buckets,$constraint-type)
    let $ct := map:count($buckets)
    let $vals := 
        for $bucket-name at $pos in map:keys($buckets)
        let $bucket := map:get($buckets,$bucket-name)
        let $ge := map:get($bucket,"ge")
        let $lt := map:get($bucket,"lt")
        return 
            if ($pos eq 1)
            then
                if (exists($lt) and exists($ge)) 
                then 
                    (map:put($bounds,"left-offset",true()),
                    if ($ascending)
                    then $ge 
                    else ($lt,$ge))
                else $ge 
            else if ($pos eq $ct) 
            then
                if (exists($lt) and exists($ge))
                then 
                    if ($ascending)
                    then ($ge,$lt)
                    else $ge
                else $ge 
            else map:get(map:get($buckets,$bucket-name),"ge")
    let $_ := map:put($bounds,"vals",$vals) 
    return $bounds     
};

declare function impl:continuous-range-facet-finish(
  $constraint as element(opt:constraint),
  $buckets as json:object,
  $start as item()*)
{
    let $constraint-type := impl:range-type($constraint/opt:range)
    let $ascending := impl:ascending-constraint($buckets,$constraint-type) 
    let $bounds-map := impl:compute-bounds($buckets,$constraint-type)
    let $bounds := map:get($bounds-map,"vals")
    let $offset := 
        if (exists(map:get($bounds-map,"left-offset"))) 
        then -1 
        else 0
    let $keys := map:keys($buckets)
    let $values := 
        (: $start list is already in correct ascending or descending order :)
        for $bucket at $pos in $start
        let $bucket-position := 
            let $index :=
                if ($ascending) then
                    if  ($bucket/cts:lower-bound) then index-of($bounds,data($bucket/cts:lower-bound)) 
                    else 0
                else 
                    if ($bucket/cts:upper-bound) then index-of($bounds,data($bucket/cts:upper-bound)) 
                    else 0 
            return $index + 1 + $offset
        (: $bucket-position is descending for ascending buckets and ascending for descending buckets :)
        let $bucket-def := 
            let $selected-key := $keys[$bucket-position]
            return
                if ($selected-key)
                then map:get($buckets,$selected-key)
                else ()
        return (
            if (not($debug:DEBUG)) then () else debug:log( ("bucket ",$bucket-position,"=",xdmp:quote($bucket) ) ),
            if (exists($bucket-def)) 
            then     
                 element search:facet-value {
                     attribute name { map:get($bucket-def,"name") },
                     attribute count {cts:frequency($bucket)},
                     map:get($bucket-def,"data")
                 }
            else ()
            )
     return 
        element search:facet { 
            $constraint/@name, 
            attribute type {"bucketed"},
            $values
        }
};


(: ----- range facet resolution ---- :)

(: No FLWOR let/return, no return type to maintain concurrency :)

declare function impl:range-facet-start(
  $constraint as element(opt:constraint),
  $query as cts:query?,
  $facet-options as xs:string*,
  $quality-weight as xs:double?,
  $forests as xs:unsignedLong*
  )
{
    cts:values(impl:construct-reference($constraint/search:range),(),($facet-options,"concurrent"),$query,$quality-weight,$forests)
};

declare function impl:range-facet-finish(
  $constraint as element(opt:constraint),
  $start as item()*
  )
as element(search:facet)?
{ 
    element search:facet {
         $constraint/@name,
         attribute type {
             if ($constraint/search:range/search:path)
             then "field"
             else impl:range-type($constraint/search:range)
             },
         for $i in $start
         return 
             element search:facet-value {
                 attribute name {$i},
                 attribute count {cts:frequency($i)},
                 $i
             }
     }
};

(: ---- heatmap facet resolution---- :)

declare function impl:heatmap-lat-bounds($heatmap as element(opt:heatmap))
as xs:double*
{
    let $n := xs:double($heatmap/@n)
    let $s := xs:double($heatmap/@s)
    let $latdivs := xs:int($heatmap/@latdivs)
 
    let $lat-bound-size := abs(($n - $s) div $latdivs)
    let $lat-start  := if ($s < $n) then $s else $n 
    let $lat-bounds := for $count in (1 to $latdivs)
                       return
                         $lat-start + (($count -1) * $lat-bound-size)
    return
        $lat-bounds
};

declare function impl:heatmap-lon-bounds($heatmap as element(opt:heatmap))
as xs:double*
{
    let $e := xs:double($heatmap/@e)
    let $w := xs:double($heatmap/@w)
    let $londivs := xs:int($heatmap/@londivs)

    let $lon-bound-size := abs(($w - $e) div $londivs)
    let $lon-start  := if ($w < $e) then $w else $e
    let $lon-bounds := for $count in (1 to $londivs)
                       return
                         $lon-start + (($count - 1) * $lon-bound-size)
    return
        $lon-bounds
};

declare function impl:heatmap(
    $constraint as element(opt:constraint)
) as element(opt:heatmap)
{
    $constraint/(opt:geo-json-property-pair|opt:geo-json-property|opt:geo-elem-pair|opt:geo-attr-pair|opt:geo-elem|opt:geo-path)/opt:heatmap
};

declare function impl:point-type(
    $constraint as element(opt:constraint)
) as xs:string?
{
    let $point-type-option := $constraint/(opt:geo-json-property|opt:geo-elem|opt:geo-path)/opt:geo-option[starts-with(., "type=")]
    return 
        if ($point-type-option) then string($point-type-option) else ()
};

(: No FLWOR let/return, no return type to maintain concurrency :)

declare function impl:heatmap-elem-pair-facet-start(
  $constraint as element(opt:constraint), 
  $query as cts:query?, 
  $facet-options as xs:string*, 
  $quality-weight as xs:double?,
  $forests as xs:unsignedLong*)
{
    let $pair    := $constraint/opt:geo-elem-pair
    let $heatmap := impl:heatmap($constraint)
    return cts:element-pair-geospatial-boxes(
        impl:spec-qname-list($pair/opt:parent),
        impl:spec-qname-list($pair/opt:lat), 
        impl:spec-qname-list($pair/opt:lon),
        impl:heatmap-lat-bounds($heatmap), impl:heatmap-lon-bounds($heatmap),
        ($facet-options, "concurrent"), $query, $quality-weight, $forests
        )
};

(: No FLWOR let/return, no return type to maintain concurrency :)

declare function impl:heatmap-attr-pair-facet-start(
  $constraint as element(opt:constraint), 
  $query as cts:query?, 
  $facet-options as xs:string*, 
  $quality-weight as xs:double?,
  $forests as xs:unsignedLong*)
{
    let $pair    := $constraint/opt:geo-attr-pair
    let $heatmap := impl:heatmap($constraint)
    return cts:element-attribute-pair-geospatial-boxes(
        impl:spec-qname-list($pair/opt:parent),
        impl:spec-qname-list($pair/opt:lat),
        impl:spec-qname-list($pair/opt:lon),
        impl:heatmap-lat-bounds($heatmap),
        impl:heatmap-lon-bounds($heatmap),
        ($facet-options, "concurrent"), $query, $quality-weight, $forests)
};

(: No FLWOR let/return, no return type to maintain concurrency :)

declare function impl:heatmap-elem-facet-start(
  $constraint as element(opt:constraint), 
  $query as cts:query?, 
  $facet-options as xs:string*, 
  $quality-weight as xs:double?,
  $forests as xs:unsignedLong*)
{
    let $elem    := $constraint/opt:geo-elem
    let $parent  := $elem/opt:parent
    let $heatmap := impl:heatmap($constraint)
    return 
        if (exists($parent))
        then cts:element-child-geospatial-boxes(
            impl:spec-qname-list($parent),
            impl:spec-qname-list($elem/opt:element),
            impl:heatmap-lat-bounds($heatmap),
            impl:heatmap-lon-bounds($heatmap),
            (impl:point-type($constraint), $facet-options, "concurrent"), $query, $quality-weight, $forests)
        else cts:element-geospatial-boxes(
            impl:spec-qname-list($elem/opt:element),
            impl:heatmap-lat-bounds($heatmap),
            impl:heatmap-lon-bounds($heatmap),
            (impl:point-type($constraint), $facet-options, "concurrent"), $query, $quality-weight, $forests)
};

declare function impl:heatmap-json-property-pair-facet-start(
  $constraint as element(opt:constraint), 
  $query as cts:query?, 
  $facet-options as xs:string*, 
  $quality-weight as xs:double?,
  $forests as xs:unsignedLong*)
{
    let $heatmap := impl:heatmap($constraint)
    return cts:geospatial-boxes(
        impl:construct-reference($constraint/opt:geo-json-property-pair),
        impl:heatmap-lat-bounds($heatmap),
        impl:heatmap-lon-bounds($heatmap),
        ($facet-options, "concurrent"), $query, $quality-weight, $forests
        )
};

declare function impl:heatmap-json-property-facet-start(
  $constraint as element(opt:constraint), 
  $query as cts:query?, 
  $facet-options as xs:string*, 
  $quality-weight as xs:double?,
  $forests as xs:unsignedLong*)
{
    let $heatmap := impl:heatmap($constraint)
    return cts:geospatial-boxes(
        impl:construct-reference($constraint/opt:geo-json-property),
        impl:heatmap-lat-bounds($heatmap),
        impl:heatmap-lon-bounds($heatmap),
        (impl:point-type($constraint), $facet-options, "concurrent"), $query, $quality-weight, $forests
        )
};

declare function impl:heatmap-facet-finish(
  $constraint as element(opt:constraint), 
  $start as item()*
  )
as element(opt:boxes)?
{
    element search:boxes {
        $constraint/@name,
        for $i in $start
        return
            element search:box {
                attribute count { cts:frequency($i) },
                attribute s { cts:box-south($i) },
                attribute w { cts:box-west($i) },
                attribute n { cts:box-north($i) },
                attribute e { cts:box-east($i) }
            }
        }
};

declare function impl:heatmap-elem-pair-facet-finish(
  $constraint as element(opt:constraint), 
  $start as item()*
  )
as element(opt:boxes)?
{
    impl:heatmap-facet-finish($constraint, $start)
};

declare function impl:heatmap-attr-pair-facet-finish(
  $constraint as element(opt:constraint), 
  $start as item()*
  )
as element(opt:boxes)?
{
    impl:heatmap-facet-finish($constraint, $start)
};

declare function impl:heatmap-elem-facet-finish(
  $constraint as element(opt:constraint), 
  $start as item()*
  )
as element(opt:boxes)?
{
    impl:heatmap-facet-finish($constraint, $start)
};

declare function impl:heatmap-json-property-pair-facet-finish(
  $constraint as element(opt:constraint), 
  $start as item()*
  )
as element(opt:boxes)?
{
    impl:heatmap-facet-finish($constraint, $start)
};

declare function impl:heatmap-json-property-facet-finish(
  $constraint as element(opt:constraint), 
  $start as item()*
  )
as element(opt:boxes)?
{
    impl:heatmap-facet-finish($constraint, $start)
};

(: ----basic facet resolution logic for concurrency---- :) 
declare function impl:do-resolve-facets(
  $options     as element(opt:options),
  $query       as cts:query?,
  $estimate    as xs:unsignedLong
) as schema-element(opt:ResponseFacet)*
{
    impl:do-resolve-facets(
        impl:facet-constraints($options), $options, $query, $estimate
        )
};

declare private function impl:facet-constraints(
    $options as element(opt:options)
) as element(opt:constraint)*
{
    $options/opt:constraint[(
        opt:collection|opt:range|opt:custom
        |opt:geo-elem-pair[opt:heatmap]
        |opt:geo-attr-pair[opt:heatmap]
        |opt:geo-elem[opt:heatmap]
        |opt:geo-json-property-pair[opt:heatmap]
        |opt:geo-json-property[opt:heatmap]
        )[empty(@facet) or @facet eq true()]]
};

declare private function impl:do-resolve-facets(
    $constraints as element(opt:constraint)+,
    $options     as element(opt:options),
    $query       as cts:query?,
    $estimate    as xs:unsignedLong
) as schema-element(opt:ResponseFacet)*
{
    if ($estimate eq 0) then
        for $constraint in $constraints
        return
            if (exists($constraint/opt:range))
            then element search:facet {
                $constraint/@name,
                attribute type {
                    if (exists($constraint/opt:range/(opt:bucket|opt:computed-bucket)))
                    then "bucketed"
                    else if (exists($constraint/opt:range/opt:path))
                    then "field"
                    else impl:range-type($constraint/opt:range)
                    }
                }
            else if (exists($constraint/opt:collection))
            then element search:facet {
                $constraint/@name,
                attribute type {"collection"}
                }
            else if (exists($constraint/opt:custom))
            then element search:facet {
                $constraint/@name
                }
            else element search:boxes {
                $constraint/@name
                }
    else
        let $quality-weight := data($options/opt:quality-weight)
        let $forests := $options/opt:forest/data(.)
        let $threads := data($options/opt:concurrency-level)
        let $ct := count($constraints)
        for $iter in (1 to ceiling($ct div $threads))
        let $start := (($iter * $threads) - ($threads - 1))
        let $stop := min((($iter * $threads),$ct))
        let $subq := ($constraints)[$start to $stop]
        return reverse(impl:run-facets($subq,$query,$quality-weight,$forests,1))
};

declare function impl:run-facets(
  $constraints as element(opt:constraint)*,
  $query as cts:query?,
  $quality-weight as xs:double,
  $forests as xs:unsignedLong*,
  $pos as xs:integer)
as schema-element(opt:ResponseFacet)*
{
    let $buckets := 
       if ($constraints[$pos]/opt:range[opt:bucket|opt:computed-bucket])
       then impl:resolve-buckets($constraints[$pos])
       else ()
   let $start := impl:start-facet($constraints[$pos],$buckets,$query,$quality-weight,$forests)
   return
   (
     if ($pos < count($constraints))
     then impl:run-facets($constraints,$query,$quality-weight,$forests,$pos +1)
     else (),
     impl:finish-facet($constraints[$pos],$buckets,$start,$query,$quality-weight,$forests))
};

declare function impl:start-facet(
  $constraint as element(opt:constraint), 
  $buckets as json:object?,
  $query as cts:query?,
  $quality-weight as xs:double, 
  $forests as xs:unsignedLong*)
{
    typeswitch($constraint/(opt:collection|opt:range|opt:custom|opt:geo-json-property-pair|opt:geo-json-property|opt:geo-elem-pair|opt:geo-attr-pair|opt:geo-elem))
    case element(opt:collection)
        return impl:collection-facet-start($constraint,impl:lexicon-query($query,impl:gfs($constraint),impl:lfs($constraint)),impl:facet-options($constraint),$quality-weight,$forests)
    case element(opt:range)
        return
            if (exists($buckets))
            then         
                if (impl:continuous-facet($buckets)) 
                then impl:continuous-range-facet-start($constraint,$buckets,impl:lexicon-query($query,impl:gfs($constraint),impl:lfs($constraint)),impl:facet-options($constraint),$quality-weight,$forests)
                else () (: discontinuous facet resolution is all done in finish :) 
            else impl:range-facet-start($constraint,impl:lexicon-query($query,impl:gfs($constraint),impl:lfs($constraint)),impl:facet-options($constraint),$quality-weight,$forests)
    case element(opt:geo-elem-pair)
        return impl:heatmap-elem-pair-facet-start($constraint,impl:lexicon-query($query,impl:gfs($constraint),impl:lfs($constraint)),impl:facet-options($constraint), $quality-weight, $forests)
    case element(opt:geo-attr-pair)
        return impl:heatmap-attr-pair-facet-start($constraint,impl:lexicon-query($query,impl:gfs($constraint),impl:lfs($constraint)),impl:facet-options($constraint), $quality-weight, $forests)
    case element(opt:geo-elem)
        return impl:heatmap-elem-facet-start($constraint,impl:lexicon-query($query,impl:gfs($constraint),impl:lfs($constraint)),impl:facet-options($constraint), $quality-weight, $forests)
    case element(opt:geo-json-property-pair)
        return impl:heatmap-json-property-pair-facet-start($constraint,impl:lexicon-query($query,impl:gfs($constraint),impl:lfs($constraint)),impl:facet-options($constraint), $quality-weight, $forests)
    case element(opt:geo-json-property)
        return impl:heatmap-json-property-facet-start($constraint,impl:lexicon-query($query,impl:gfs($constraint),impl:lfs($constraint)),impl:facet-options($constraint), $quality-weight, $forests)
    case element(opt:custom)
        return
            if ($constraint/opt:custom/opt:start-facet) 
            then
                if (impl:extension($constraint/opt:custom/opt:start-facet))
                then 
                    let $start-fn := xdmp:function(QName($constraint/opt:custom/opt:start-facet/@ns, $constraint/opt:custom/opt:start-facet/@apply), $constraint/opt:custom/opt:start-facet/@at)
                    return xdmp:apply($start-fn, $constraint,impl:lexicon-query($query,impl:gfs($constraint),impl:lfs($constraint)),impl:facet-options($constraint),$quality-weight,$forests)
                else error((),"SEARCH-BADEXTENSION")
            else ()
    default return impl:debug-errmsg("unknown case for facet start")
};

declare function impl:finish-facet(
  $constraint as element(opt:constraint),
  $buckets as json:object?,
  $start as item()*,
  $query as cts:query?,
  $quality-weight as xs:double, 
  $forests as xs:unsignedLong*)
{
    let $query := impl:lexicon-query($query,impl:gfs($constraint),impl:lfs($constraint))
    let $facet-options := $constraint//opt:facet-option/string()
    return
        typeswitch($constraint/(opt:collection|opt:range|opt:custom|opt:geo-json-property-pair|opt:geo-json-property|opt:geo-elem-pair|opt:geo-attr-pair|opt:geo-elem))
        case element(opt:collection) 
            return impl:collection-facet-finish($constraint,$start)
        case element(opt:range)
            return 
                if (exists($buckets))
                then 
                    if (impl:continuous-facet($buckets)) 
                    then impl:continuous-range-facet-finish($constraint,$buckets,$start)
                    else impl:resolve-discontinuous-range-facet($constraint,$buckets,$query,impl:facet-options($constraint),$quality-weight,$forests)
                else impl:range-facet-finish($constraint,$start)
        case element(opt:geo-elem-pair)
            return impl:heatmap-elem-pair-facet-finish($constraint, $start)
        case element(opt:geo-attr-pair)
            return impl:heatmap-attr-pair-facet-finish($constraint, $start)
        case element(opt:geo-elem)
            return impl:heatmap-elem-facet-finish($constraint, $start)
        case element(opt:geo-json-property-pair)
            return impl:heatmap-json-property-pair-facet-finish($constraint, $start)
        case element(opt:geo-json-property)
            return impl:heatmap-json-property-facet-finish($constraint, $start)
        case element(opt:custom)
            return
                if ($constraint/opt:custom/opt:finish-facet)
                then
                    if (impl:extension($constraint/opt:custom/opt:finish-facet))
                    then  
                        let $finish-fn := xdmp:function(QName($constraint/opt:custom/opt:finish-facet/@ns, $constraint/opt:custom/opt:finish-facet/@apply), $constraint/opt:custom/opt:finish-facet/@at)
                        return xdmp:apply($finish-fn, $start,$constraint,$query,$facet-options,$quality-weight,$forests)
                    else error((),"SEARCH-BADEXTENSION")
                else error((),"SEARCH-NOFACET",$constraint/@name) 
        default return impl:debug-errmsg("unknown case for facet finish")
};

(: local fragment-scope :)
declare function impl:lfs(
    $constraint as element()
) as xs:string
{
    string(head($constraint/(opt:range|opt:collection|opt:word|opt:element-query|opt:container|opt:custom|opt:geo-json-property-pair|opt:geo-json-property|opt:geo-elem-pair|opt:geo-attr-pair|opt:geo-elem)/opt:fragment-scope))
};

(: global fragment-scope :)
declare function impl:gfs(
    $constraint as element()
) as xs:string
{
    if (exists($constraint/root()/opt:fragment-scope[. eq 'properties']))
    then "properties"
    else "documents"    
};

(: use impl:range-options for range constraints :)
declare function impl:facet-options(
    $constraint as element(opt:constraint)
) as xs:string*
{
    let $gfs := impl:gfs($constraint)
    let $lfs := impl:lfs($constraint)
    let $scope :=
        if ($lfs eq "properties" or ($gfs eq "properties" and $lfs ne "documents"))
        then "properties"
        else ()
    let $ctelem := $constraint/(opt:range|opt:collection|opt:custom|opt:geo-json-property-pair|opt:geo-json-property|opt:geo-elem-pair|opt:geo-attr-pair|opt:geo-elem)
    let $result := $ctelem/opt:facet-option/string() (: passing in 'properties', et.al. here should be forbidden in check-options :)
    return ($scope, $result)
};

(: Produce a cts:query suitable for passing to the *-values() functions. Avoid redundant wrapping 
:)
declare function impl:lexicon-query(
    $query as cts:query?,
    $global-frag-scope as xs:string,  (: "documents" or "properties" :)
    $loc-frag-scope as xs:string    (: "documents" , "properties", or "" :)
)
as cts:query?
{
    if (exists($query))
    then
        if ($loc-frag-scope eq "properties" and $global-frag-scope eq "documents")
        then cts:document-fragment-query($query)
        else
            if ($loc-frag-scope eq "documents" and $global-frag-scope eq "properties")
            then cts:properties-fragment-query($query)
            else $query
    else ()
};

declare function impl:build-path-ns-bindings(
    $query as item()?
)as xs:string* 
{
    impl:build-path-ns-bindings(map:map(),$query)
};

declare function impl:build-path-ns-bindings(
    $nsmap as map:map,
    $query as item()?
)as xs:string*
{
(: TODO: extract namespaces from query? :)
    if ($query instance of cts:query) then ()
    else (
        impl:collect-namespace-bindings($nsmap, $query//(cts:path-expression|search:path-index)),

        impl:list-namespace-bindings($nsmap)
        )
};

declare function impl:build-ns-bindings(
    $options as element(search:options)
) as xs:string*
{
    impl:build-ns-bindings(map:map(),$options)
};

declare function impl:build-ns-bindings(
    $nsmap   as map:map,
    $options as element(search:options)
) as xs:string*
{
    let $namers := $options//*[exists(@ns) and string(@ns) ne ""]
    for $ns at $pos in distinct-values($namers/string(@ns))
    let $name := (
        $namers[@ns eq $ns]/@name/string(.)[contains(.,":",$impl:UNICODE-CP-COLLATION)]
        )[1]
    let $prefix :=
        if (exists($name))
        then substring-before($name,":",$impl:UNICODE-CP-COLLATION)
        else concat("_", $pos)
    return map:put($nsmap, $prefix, $ns),

    impl:collect-namespace-bindings($nsmap, (
        $options/opt:searchable-expression,
        $options/opt:additional-query,
        $options//opt:path-index,
        $options/opt:extract-document-data[not(@selected eq "all")]/opt:extract-path
        )),

    impl:list-namespace-bindings($nsmap)
};

declare function impl:collect-namespace-bindings(
    $nsmap      as map:map,
    $containers as element()*
) as empty-sequence()
{
    for $container in $containers
    for $prefix in in-scope-prefixes($container)
    let $ns := namespace-uri-for-prefix($prefix,$container)
    return
        if ($prefix = ("") or
            $ns = ("http://www.w3.org/XML/1998/namespace", "http://marklogic.com/appservices/search")
            ) then ()
        else map:put($nsmap, $prefix, $ns)
};

declare function impl:list-namespace-bindings(
    $nsmap as map:map
) as xs:string*
{
    for $prefix in map:keys($nsmap)
    return ($prefix, map:get($nsmap, $prefix))
};

declare function impl:cts-query(
    $serialized-query as schema-element(cts:query)*
) as cts:query?
{
    impl:cts-query($serialized-query,())
};

declare function impl:cts-query(
    $serialized-query as schema-element(cts:query)*,
    $ns-bindings      as xs:string*
) as cts:query?
{
    switch(count($serialized-query))
    case 0 return
        ()
    case 1 return
        if (empty($ns-bindings))
        then cts:query($serialized-query)
        else xdmp:with-namespaces($ns-bindings, cts:query($serialized-query))
    default return
        cts:and-query(
            if (empty($ns-bindings)) then
                for $q in $serialized-query
                return cts:query($q)
            else
                for $q in $serialized-query
                return xdmp:with-namespaces($ns-bindings, cts:query($q))
        )
};

(: Is @apply to specifying an extension? Test for @ns and @at :)
declare function impl:extension($test-elem as element()) as xs:boolean {
    if ($test-elem/@apply and $test-elem/@ns and $test-elem/@at) then true()
    else if ($test-elem/@apply and ($test-elem/@ns or $test-elem/@at)) then error((),"SEARCH-BADEXTENSION")
    else false()
};

(: This function grabs only the children of the opt namespace.  As 
 : children of other namespaces are added to options, the XPath should
 : be changed or an additional function should be added. For example:
 : for the transform elements
:)
declare function impl:merge-options($default as element()?, $delta as element()?) as element() {
    let $subdelta := $delta/opt:*
    let $extended := $subdelta/local-name(.)
    return
        element {node-name($default)} {
            $default/@*,
            $delta/namespace::*,
            $subdelta,
            $default/opt:*[not(local-name(.) = $extended)]  
        }
};

declare function impl:doc-root(
    $node as node()?
) as node()?
{
    if ($node instance of document-node()) then
        let $root-elem := $node/element()
        return
            if (exists($root-elem))
            then $root-elem
            else
                let $root-node :=
                    $node/(text()|binary()|object-node()|array-node())
                return
                    if (exists($root-node))
                    then $root-node
                    else $node
    else $node
};

declare function impl:snip-config(
    $options as element(search:transform-results)?
) as map:map
{
    let $map       := map:map()
    let $preferred := 
        for $m in $options/(search:preferred-elements|search:preferred-matches)
        return (
            impl:spec-qname-list($m/search:element),

            for $p in $m/search:json-property
            return string($p)
            )
    return (
        if (empty($preferred)) then ()
        else map:put($map,"preferredMatches",$preferred),
        map:put($map,"maxMatches", xs:positiveInteger(head((
            $options/search:max-matches,
            $impl:default-options/search:transform-results/search:max-matches
            )))),
        map:put($map,"perMatchTokens", xs:positiveInteger(head((
            $options/search:per-match-tokens,
            $impl:default-options/search:transform-results/search:per-match-tokens
            )))),
        map:put($map,"maxSnippetChars", xs:positiveInteger(head((
            $options/search:max-snippet-chars,
            $impl:default-options/search:transform-results/search:max-snippet-chars
            )))),

        $map
        )
};

declare function impl:resolve-anchor($anchor as xs:string, $type as xs:string, $custom-anchors as element(opt:anchor)*)
as xs:anyAtomicType?
{
    let $now := if ($type eq "xs:dateTime")
                then current-dateTime()
                else if ($type eq "xs:date")
                     then current-date()
                     else error((), "SEARCH-UNSUPPORTEDANCHOR", ())
    return
        if ($anchor eq "now") then $now
            else if ($anchor eq "start-of-month")
                 then if ($type eq "xs:date") then functx:first-day-of-month($now) else xs:dateTime(functx:first-day-of-month($now))
                 else if ($anchor eq "start-of-day")
                      then if ($type eq "xs:date")then $now else xs:dateTime(xs:date($now))
                      else if ($anchor eq "start-of-year") 
                           then if ($type eq "xs:date") then functx:first-day-of-year($now) else xs:dateTime(functx:first-day-of-year($now))
                           else 
                               let $custom-anchor := $custom-anchors[@name eq $anchor]
                               return
                                   if ($custom-anchor)
                                   then
                                        if (impl:extension($custom-anchor)) 
                                        then
                                            let $fnobj := xdmp:function(QName($custom-anchor/@ns, $custom-anchor/@apply), $custom-anchor/@at)
                                            return xdmp:apply($fnobj, $type)
                                        else error((),"SEARCH-BADEXTENSION")
                                   else error((), "SEARCH-UNSUPPORTEDANCHOR", ()) 
};

declare function impl:empty-snippet()
{
    <search:snippet/>
};

declare function impl:metadata-snippet(
  $result as node(),
  $opts   as element(opt:transform-results)
) as element(search:snippet)
{
    <search:snippet>{
        snip:match(
            if (exists(xdmp:node-database($result)))
            then xdmp:describe($result)
            else xdmp:path($result),
            (),
            $impl:AS_ELEMENTS
            ),
        
        let $matches    := $opts/(opt:preferred-elements|opt:preferred-matches)
        let $elements   := $matches/opt:element
        let $properties := $matches/opt:json-property
        let $qnames     :=
            if (empty($elements) and empty($properties))
            (: special case: if no preferred-elements, then look for prop:last-modified :)
            then QName("http://marklogic.com/xdmp/property", "last-modified")
            else (
                impl:spec-qname-list($elements),

                for $property in $properties
                return QName("", $property/string(.))
                )
        return $result//node()[node-name(.) = $qnames]
    }</search:snippet>
};

declare function impl:snippet-dispatch(
    $result         as node(),
    $snippet-config as map:map?,
    $snippet-query  as cts:query?,
    $as             as xs:int,
    $metrics        as map:map?
) {
    let $metrics-elapsed :=
        if (empty($metrics)) then ()
        else map:get($metrics,"snippet-elapsed")
    let $metrics-start   :=
        if (empty($metrics)) then ()
        else xdmp:elapsed-time()
    return (
        snip:make-snippet(
            impl:doc-root($result),$snippet-query,$snippet-config,$as
            ),

        if (empty($metrics)) then ()
        else map:put($metrics, "snippet-elapsed",
            if (empty($metrics-elapsed))
            then (xdmp:elapsed-time() - $metrics-start)
            else ((xdmp:elapsed-time() + $metrics-elapsed) - $metrics-start)
            )
        )
};

declare function impl:snippet-dispatch(
    $options          as element(opt:options),
    $snippet-name     as xs:string?,
    $snippet-func     as function(*)?,
    $combined-query   as cts:query?,
    $serialized-query as schema-element(cts:query)?,
    $nsbindings       as xs:string*,
    $result           as node()?,
    $as               as xs:int,
    $metrics          as map:map?
) {
    let $metrics-elapsed :=
        if (empty($metrics)) then ()
        else map:get($metrics,"snippet-elapsed")
    let $metrics-start   :=
        if (empty($metrics)) then ()
        else xdmp:elapsed-time()
    return (
        if (exists($snippet-func)) 
        then xdmp:apply(
            $snippet-func, $result, $serialized-query, $options/opt:transform-results
            ) 
        else if (empty($snippet-name)) then ()
        else
            switch($snippet-name)
            case "snippet" return
                snip:make-snippet(
                    impl:doc-root($result),
                    $combined-query,
                    impl:snip-config($options/opt:transform-results),
                    $as
                    )
            case "raw" return
                impl:dispatch-raw-snippet(impl:doc-root($result))
            case "empty-snippet" return
                impl:empty-snippet()
            case "metadata-snippet" return
                impl:metadata-snippet($result, $options/opt:transform-results)
            default return
                error((),"SEARCH-APPLYUNDEFINED", $snippet-name),

        if (empty($metrics)) then ()
        else map:put($metrics, "snippet-elapsed",
            if (empty($metrics-elapsed))
            then (xdmp:elapsed-time() - $metrics-start)
            else ((xdmp:elapsed-time() + $metrics-elapsed) - $metrics-start)
            )
        )
};

(: This will be inside an element wrapper, so don't return binary, and send quoted json :)
declare function impl:dispatch-raw-snippet(
    $result-node as node()
)
as item()?
{
    typeswitch($result-node)
        case binary() return ()
        case object-node() return xdmp:quote($result-node) 
        default return $result-node
};

(: ========================== Simple validation of options XML =========================== :)

(:~
 : Do a bunch of checks on options
 :
 : @param $options the options
   @param $strict do additional checks of live index settings
 : @returns empty sequence on success, else one or more error strings
 :)
declare function impl:do-check-options($opt as element(opt:options), $strict as xs:boolean) as element(search:report)* {
(
    let $x := try { 
                  validate strict { $opt },
                  for $annotations in $opt//search:annotation/search:*
                  return validate strict { $annotations } 
                  }
              catch ($e) { 
                  if (matches($e/error:format-string,"Found search:json-key")) 
                  then <search:report id="SEARCH-SCHEMA">Use "json-property" instead of "json-key" to specify structures in JSON. Validation detail:  {$e/error:format-string/string()}</search:report>
                  else <search:report id="SEARCH-SCHEMAINVALID">{$e/*:format-string, $e/*:data}</search:report> }
    return if ($x/self::search:report)
           then $x
           else ()
    ,
    impl:do-check-collation-compatibility($opt),
    (: Frequently-encountered problems, including things that Schema content modelling can't catch, with helpful messages
       IMPORTANT:
           DO NOT EDIT THE XSLT STYLESHEET PASSED AS THE SECOND ARGUMENT TO schematron:validate()
           THE XSLT STYLESHEET IS COMPILED FROM THE SCHEMATRON RULES IN query-options.sch
           SEE THE COMMENT AT THE TOP OF THAT FILE FOR INSTRUCTIONS ON HOW TO COMPILE THE SCHEMATRON TO XSLT
       REPLACEMENT START :)
    schematron:validate(document {$opt}, document {
<xsl:stylesheet extension-element-prefixes="xdmp" xdmp:dialect="1.0-ml" version="2.0" xmlns:schold="http://www.ascc.net/xml/schematron" xmlns:iso="http://purl.oclc.org/dsdl/schematron" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xdmp="http://marklogic.com/xdmp" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:opt="http://marklogic.com/appservices/search">
  <!--Implementers: please note that overriding process-prolog or process-root is
    the preferred method for meta-stylesheets to use where possible. -->
  <xsl:param name="archiveDirParameter"/>
  <xsl:param name="archiveNameParameter"/>
  <xsl:param name="fileNameParameter"/>
  <xsl:param name="fileDirParameter"/>
  <xsl:variable name="document-uri"><xsl:value-of select="document-uri(/)"/></xsl:variable>
  <!--PHASES-->
  <!--PROLOG-->
  <xsl:output method="xml" omit-xml-declaration="no" standalone="yes" indent="yes" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl"/>
  <!--XSD TYPES FOR XSLT2-->
  <!--KEYS AND FUNCTIONS-->
  <!--DEFAULT RULES-->
  <!--MODE: SCHEMATRON-SELECT-FULL-PATH-->
  <!--This mode can be used to generate an ugly though full XPath for locators-->
  <xsl:template match="*|/|object-node()" mode="schematron-select-full-path"><xsl:apply-templates select="." mode="schematron-get-full-path"/></xsl:template>
  <!--MODE: SCHEMATRON-FULL-PATH-->
  <!--This mode can be used to generate an ugly though full XPath for locators-->
  <xsl:template match="*|/|object-node()" mode="schematron-get-full-path"><xsl:value-of select="xdmp:path(.)"/></xsl:template>
  <xsl:template match="@*" mode="schematron-get-full-path"><xsl:value-of select="xdmp:path(.)"/></xsl:template>
  <!--MODE: SCHEMATRON-FULL-PATH-2-->
  <!--This mode can be used to generate prefixed XPath for humans-->
  <xsl:template match="node() | @*" mode="schematron-get-full-path-2"><xsl:for-each select="ancestor-or-self::*"><xsl:text>/</xsl:text><xsl:value-of select="name(.)"/><xsl:if test="preceding-sibling::*[name(.)=name(current())]"><xsl:text>[</xsl:text><xsl:value-of select="count(preceding-sibling::*[name(.)=name(current())])+1"/><xsl:text>]</xsl:text></xsl:if></xsl:for-each><xsl:if test="not(self::*)"><xsl:text/>/@<xsl:value-of select="name(.)"/></xsl:if></xsl:template>
  <!--MODE: SCHEMATRON-FULL-PATH-3-->
  <!--This mode can be used to generate prefixed XPath for humans
	(Top-level element has index)-->
  <xsl:template match="node() | @*" mode="schematron-get-full-path-3"><xsl:for-each select="ancestor-or-self::*"><xsl:text>/</xsl:text><xsl:value-of select="name(.)"/><xsl:if test="parent::*"><xsl:text>[</xsl:text><xsl:value-of select="count(preceding-sibling::*[name(.)=name(current())])+1"/><xsl:text>]</xsl:text></xsl:if></xsl:for-each><xsl:if test="not(self::*)"><xsl:text/>/@<xsl:value-of select="name(.)"/></xsl:if></xsl:template>
  <!--MODE: GENERATE-ID-FROM-PATH -->
  <xsl:template match="/" mode="generate-id-from-path"/>
  <xsl:template match="text()" mode="generate-id-from-path"><xsl:apply-templates select="parent::*" mode="generate-id-from-path"/><xsl:value-of select="concat('.text-', 1+count(preceding-sibling::text()), '-')"/></xsl:template>
  <xsl:template match="comment()" mode="generate-id-from-path"><xsl:apply-templates select="parent::*" mode="generate-id-from-path"/><xsl:value-of select="concat('.comment-', 1+count(preceding-sibling::comment()), '-')"/></xsl:template>
  <xsl:template match="processing-instruction()" mode="generate-id-from-path"><xsl:apply-templates select="parent::*" mode="generate-id-from-path"/><xsl:value-of select="concat('.processing-instruction-', 1+count(preceding-sibling::processing-instruction()), '-')"/></xsl:template>
  <xsl:template match="@*" mode="generate-id-from-path"><xsl:apply-templates select="parent::*" mode="generate-id-from-path"/><xsl:value-of select="concat('.@', name())"/></xsl:template>
  <xsl:template match="*" mode="generate-id-from-path" priority="-0.5"><xsl:apply-templates select="parent::*" mode="generate-id-from-path"/><xsl:text>.</xsl:text><xsl:value-of select="concat('.',name(),'-',1+count(preceding-sibling::*[name()=name(current())]),'-')"/></xsl:template>
  <!--MODE: GENERATE-ID-2 -->
  <xsl:template match="/" mode="generate-id-2">U</xsl:template>
  <xsl:template match="*" mode="generate-id-2" priority="2"><xsl:text>U</xsl:text><xsl:number level="multiple" count="*"/></xsl:template>
  <xsl:template match="node()" mode="generate-id-2"><xsl:text>U.</xsl:text><xsl:number level="multiple" count="*"/><xsl:text>n</xsl:text><xsl:number count="node()"/></xsl:template>
  <xsl:template match="@*" mode="generate-id-2"><xsl:text>U.</xsl:text><xsl:number level="multiple" count="*"/><xsl:text>_</xsl:text><xsl:value-of select="string-length(local-name(.))"/><xsl:text>_</xsl:text><xsl:value-of select="translate(name(),':','.')"/></xsl:template>
  <!--Strip characters-->
  <xsl:template match="text()|number-node()|boolean-node()|null-node()" priority="-1"/>
  <!--SCHEMA SETUP-->
  <xsl:template match="/"><svrl:schematron-output title="" schemaVersion="1.0" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
      <xsl:comment><xsl:value-of select="$archiveDirParameter"/>
		 <xsl:value-of select="$archiveNameParameter"/>
		 <xsl:value-of select="$fileNameParameter"/>
		 <xsl:value-of select="$fileDirParameter"/></xsl:comment>
      <svrl:ns-prefix-in-attribute-values uri="http://marklogic.com/appservices/search" prefix="opt"/>
      <svrl:active-pattern>
	<xsl:attribute name="document"><xsl:value-of select="document-uri(/)"/></xsl:attribute>
	<xsl:apply-templates/>
      </svrl:active-pattern>
      <xsl:apply-templates select="/" mode="M1"/>
    </svrl:schematron-output></xsl:template>
  <!--SCHEMATRON PATTERNS-->
  <!--PATTERN -->
  <!--RULE -->
  <xsl:template match="opt:bucket" priority="1012" mode="M1"><svrl:fired-rule context="opt:bucket" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl"/>

		<!--REPORT -->
<xsl:if test="@anchor"><svrl:successful-report test="@anchor" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	<xsl:attribute name="id">SEARCH-BUCKETSPEC_1</xsl:attribute>
	<xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	<svrl:text>If you need an @anchor, you need &lt;computed-bucket&gt; not &lt;bucket&gt;</svrl:text>
      </svrl:successful-report></xsl:if><xsl:apply-templates select="*|comment()|processing-instruction()" mode="M1"/></xsl:template>
  <!--RULE -->
  <xsl:template match="opt:constraint" priority="1011" mode="M1"><svrl:fired-rule context="opt:constraint" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl"/>

		<!--ASSERT -->
<xsl:choose>
      <xsl:when test="count(opt:range|opt:value|opt:word|opt:collection|opt:properties|opt:element-query|opt:container|opt:geo-elem|opt:geo-elem-pair|opt:geo-attr-pair|opt:geo-json-property|opt:geo-json-property-pair|opt:geo-path|opt:geo-region-path|opt:custom) eq 1"/>
      <xsl:otherwise><svrl:failed-assert test="count(opt:range|opt:value|opt:word|opt:collection|opt:properties|opt:element-query|opt:container|opt:geo-elem|opt:geo-elem-pair|opt:geo-attr-pair|opt:geo-json-property|opt:geo-json-property-pair|opt:geo-path|opt:geo-region-path|opt:custom) eq 1" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	  <xsl:attribute name="id">SEARCH-SCHEMA_21</xsl:attribute>
	  <xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	  <svrl:text>Exactly 1 &lt;constraint&gt; type needs to be defined at a time.</svrl:text>
	</svrl:failed-assert></xsl:otherwise>
    </xsl:choose>

		<!--ASSERT -->
<xsl:choose>
      <xsl:when test="not(matches(@name/data(),'\s'))"/>
      <xsl:otherwise><svrl:failed-assert test="not(matches(@name/data(),'\s'))" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	  <xsl:attribute name="id">SEARCH-SCHEMA_22</xsl:attribute>
	  <xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	  <svrl:text>Constraint names cannot contain a space.</svrl:text>
	</svrl:failed-assert></xsl:otherwise>
    </xsl:choose><xsl:apply-templates select="*|comment()|processing-instruction()" mode="M1"/></xsl:template>
  <!--RULE -->
  <xsl:template match="opt:custom" priority="1010" mode="M1"><svrl:fired-rule context="opt:custom" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl"/>

		<!--ASSERT -->
<xsl:choose>
      <xsl:when test="if (@facet eq true()) then count(opt:finish-facet) eq 1 else true()"/>
      <xsl:otherwise><svrl:failed-assert test="if (@facet eq true()) then count(opt:finish-facet) eq 1 else true()" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	  <xsl:attribute name="id">SEARCH-SCHEMA_20</xsl:attribute>
	  <xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	  <svrl:text>&lt;finish-facet&gt; is required for custom constraint if @facet is true</svrl:text>
	</svrl:failed-assert></xsl:otherwise>
    </xsl:choose><xsl:apply-templates select="*|comment()|processing-instruction()" mode="M1"/></xsl:template>
  <!--RULE -->
  <xsl:template match="opt:default" priority="1009" mode="M1"><svrl:fired-rule context="opt:default" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl"/>

		<!--ASSERT -->
<xsl:choose>
      <xsl:when test="if (@ref) then count((opt:range|opt:word|opt:value)) eq 0 else true()"/>
      <xsl:otherwise><svrl:failed-assert test="if (@ref) then count((opt:range|opt:word|opt:value)) eq 0 else true()" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	  <xsl:attribute name="id">SEARCH-SCHEMA_23</xsl:attribute>
	  <xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	  <svrl:text>Default term can contain one child definition (word, value, range) or one @ref. (@ref used if both are provided)</svrl:text>
	</svrl:failed-assert></xsl:otherwise>
    </xsl:choose><xsl:apply-templates select="*|comment()|processing-instruction()" mode="M1"/></xsl:template>
  <!--RULE -->
  <xsl:template match="opt:extract-metadata" priority="1008" mode="M1"><svrl:fired-rule context="opt:extract-metadata" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl"/>

		<!--ASSERT -->
<xsl:choose>
      <xsl:when test="count(opt:constraint-value|opt:qname|opt:json-property) gt 0"/>
      <xsl:otherwise><svrl:failed-assert test="count(opt:constraint-value|opt:qname|opt:json-property) gt 0" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	  <xsl:attribute name="id">SEARCH-SCHEMA_27</xsl:attribute>
	  <xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	  <svrl:text>Must specify metadata elements to extract.</svrl:text>
	</svrl:failed-assert></xsl:otherwise>
    </xsl:choose><xsl:apply-templates select="*|comment()|processing-instruction()" mode="M1"/></xsl:template>
  <!--RULE -->
  <xsl:template match="opt:options" priority="1007" mode="M1"><svrl:fired-rule context="opt:options" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl"/>

		<!--ASSERT -->
<xsl:choose>
      <xsl:when test="count(opt:debug) le 1"/>
      <xsl:otherwise><svrl:failed-assert test="count(opt:debug) le 1" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	  <xsl:attribute name="id">SEARCH-SCHEMA_5</xsl:attribute>
	  <xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	  <svrl:text>No more than 1 &lt;debug&gt; option is allowed.</svrl:text>
	</svrl:failed-assert></xsl:otherwise>
    </xsl:choose>

		<!--ASSERT -->
<xsl:choose>
      <xsl:when test="count(opt:default-suggestion-source) le 1"/>
      <xsl:otherwise><svrl:failed-assert test="count(opt:default-suggestion-source) le 1" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	  <xsl:attribute name="id">SEARCH-SCHEMA_6</xsl:attribute>
	  <xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	  <svrl:text>No more than 1 &lt;default-suggestion-source&gt; option is allowed.</svrl:text>
	</svrl:failed-assert></xsl:otherwise>
    </xsl:choose>

		<!--ASSERT -->
<xsl:choose>
      <xsl:when test="count(opt:grammar) le 1"/>
      <xsl:otherwise><svrl:failed-assert test="count(opt:grammar) le 1" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	  <xsl:attribute name="id">SEARCH-SCHEMA_7</xsl:attribute>
	  <xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	  <svrl:text>No more than 1 &lt;grammar&gt; option is allowed.</svrl:text>
	</svrl:failed-assert></xsl:otherwise>
    </xsl:choose>

		<!--ASSERT -->
<xsl:choose>
      <xsl:when test="count(opt:page-length) le 1"/>
      <xsl:otherwise><svrl:failed-assert test="count(opt:page-length) le 1" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	  <xsl:attribute name="id">SEARCH-SCHEMA_8</xsl:attribute>
	  <xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	  <svrl:text>No more than 1 &lt;page-length&gt; option is allowed.</svrl:text>
	</svrl:failed-assert></xsl:otherwise>
    </xsl:choose>

		<!--ASSERT -->
<xsl:choose>
      <xsl:when test="count(opt:quality-weight) le 1"/>
      <xsl:otherwise><svrl:failed-assert test="count(opt:quality-weight) le 1" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	  <xsl:attribute name="id">SEARCH-SCHEMA_9</xsl:attribute>
	  <xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	  <svrl:text>No more than 1 &lt;quality-weight&gt; option is allowed.</svrl:text>
	</svrl:failed-assert></xsl:otherwise>
    </xsl:choose>

		<!--ASSERT -->
<xsl:choose>
      <xsl:when test="count(opt:return-constraints) le 1"/>
      <xsl:otherwise><svrl:failed-assert test="count(opt:return-constraints) le 1" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	  <xsl:attribute name="id">SEARCH-SCHEMA_10</xsl:attribute>
	  <xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	  <svrl:text>No more than 1 &lt;return-constraints&gt; option is allowed.</svrl:text>
	</svrl:failed-assert></xsl:otherwise>
    </xsl:choose>

		<!--ASSERT -->
<xsl:choose>
      <xsl:when test="count(opt:return-facets) le 1"/>
      <xsl:otherwise><svrl:failed-assert test="count(opt:return-facets) le 1" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	  <xsl:attribute name="id">SEARCH-SCHEMA_11</xsl:attribute>
	  <xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	  <svrl:text>No more than 1 &lt;return-facets&gt; option is allowed.</svrl:text>
	</svrl:failed-assert></xsl:otherwise>
    </xsl:choose>

		<!--ASSERT -->
<xsl:choose>
      <xsl:when test="count(opt:return-metrics) le 1"/>
      <xsl:otherwise><svrl:failed-assert test="count(opt:return-metrics) le 1" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	  <xsl:attribute name="id">SEARCH-SCHEMA_12</xsl:attribute>
	  <xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	  <svrl:text>No more than 1 &lt;return-metrics&gt; option is allowed.</svrl:text>
	</svrl:failed-assert></xsl:otherwise>
    </xsl:choose>

		<!--ASSERT -->
<xsl:choose>
      <xsl:when test="count(opt:return-plan) le 1"/>
      <xsl:otherwise><svrl:failed-assert test="count(opt:return-plan) le 1" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	  <xsl:attribute name="id">SEARCH-SCHEMA_13</xsl:attribute>
	  <xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	  <svrl:text>No more than 1 &lt;return-plan&gt; option is allowed.</svrl:text>
	</svrl:failed-assert></xsl:otherwise>
    </xsl:choose>

		<!--ASSERT -->
<xsl:choose>
      <xsl:when test="count(opt:return-qtext) le 1"/>
      <xsl:otherwise><svrl:failed-assert test="count(opt:return-qtext) le 1" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	  <xsl:attribute name="id">SEARCH-SCHEMA_14</xsl:attribute>
	  <xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	  <svrl:text>No more than 1 &lt;return-qtext&gt; option is allowed.</svrl:text>
	</svrl:failed-assert></xsl:otherwise>
    </xsl:choose>

		<!--ASSERT -->
<xsl:choose>
      <xsl:when test="count(opt:return-query) le 1"/>
      <xsl:otherwise><svrl:failed-assert test="count(opt:return-query) le 1" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	  <xsl:attribute name="id">SEARCH-SCHEMA_15</xsl:attribute>
	  <xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	  <svrl:text>No more than 1 &lt;return-query&gt; option is allowed.</svrl:text>
	</svrl:failed-assert></xsl:otherwise>
    </xsl:choose>

		<!--ASSERT -->
<xsl:choose>
      <xsl:when test="count(opt:return-results) le 1"/>
      <xsl:otherwise><svrl:failed-assert test="count(opt:return-results) le 1" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	  <xsl:attribute name="id">SEARCH-SCHEMA_16</xsl:attribute>
	  <xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	  <svrl:text>No more than 1 &lt;return-results&gt; option is allowed.</svrl:text>
	</svrl:failed-assert></xsl:otherwise>
    </xsl:choose>

		<!--ASSERT -->
<xsl:choose>
      <xsl:when test="count(opt:return-similar) le 1"/>
      <xsl:otherwise><svrl:failed-assert test="count(opt:return-similar) le 1" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	  <xsl:attribute name="id">SEARCH-SCHEMA_17</xsl:attribute>
	  <xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	  <svrl:text>No more than 1 &lt;return-similar&gt; option is allowed.</svrl:text>
	</svrl:failed-assert></xsl:otherwise>
    </xsl:choose>

		<!--ASSERT -->
<xsl:choose>
      <xsl:when test="count(opt:searchable-expression) le 1"/>
      <xsl:otherwise><svrl:failed-assert test="count(opt:searchable-expression) le 1" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	  <xsl:attribute name="id">SEARCH-SCHEMA_18</xsl:attribute>
	  <xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	  <svrl:text>No more than 1 &lt;searchable-expression&gt; option is allowed.</svrl:text>
	</svrl:failed-assert></xsl:otherwise>
    </xsl:choose>

		<!--ASSERT -->
<xsl:choose>
      <xsl:when test="count(opt:transform-results) le 1"/>
      <xsl:otherwise><svrl:failed-assert test="count(opt:transform-results) le 1" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	  <xsl:attribute name="id">SEARCH-SCHEMA_19</xsl:attribute>
	  <xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	  <svrl:text>No more than 1 &lt;transform-results&gt; option is allowed.</svrl:text>
	</svrl:failed-assert></xsl:otherwise>
    </xsl:choose><xsl:apply-templates select="*|comment()|processing-instruction()" mode="M1"/></xsl:template>
  <!--RULE -->
  <xsl:template match="opt:range" priority="1006" mode="M1"><svrl:fired-rule context="opt:range" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl"/>

		<!--ASSERT -->
<xsl:choose>
      <xsl:when test="count(opt:element|opt:field|opt:path-index|opt:json-property) ge 1"/>
      <xsl:otherwise><svrl:failed-assert test="count(opt:element|opt:field|opt:path-index|opt:json-property) ge 1" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	  <xsl:attribute name="id">SEARCH-SCHEMA_2</xsl:attribute>
	  <xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	  <svrl:text>An &lt;element&gt;, &lt;path-index&gt;, &lt;json-property&gt; or &lt;field&gt; specification is required on &lt;range&gt;.</svrl:text>
	</svrl:failed-assert></xsl:otherwise>
    </xsl:choose><xsl:apply-templates select="*|comment()|processing-instruction()" mode="M1"/></xsl:template>
  <!--RULE -->
  <xsl:template match="opt:sort-order" priority="1005" mode="M1"><svrl:fired-rule context="opt:sort-order" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl"/>

		<!--ASSERT -->
<xsl:choose>
      <xsl:when test="count(opt:element|opt:json-property|opt:field|opt:path-index) le 1 and count(opt:attribute) le 1"/>
      <xsl:otherwise><svrl:failed-assert test="count(opt:element|opt:json-property|opt:field|opt:path-index) le 1 and count(opt:attribute) le 1" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	  <xsl:attribute name="id">SEARCH-SCHEMA_1</xsl:attribute>
	  <xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	  <svrl:text>No more than one &lt;element&gt;, &lt;attribute&gt;, &lt;path-index&gt;, &lt;json-property&gt; or &lt;field&gt; specification allowed under &lt;sort-order.</svrl:text>
	</svrl:failed-assert></xsl:otherwise>
    </xsl:choose>

		<!--ASSERT -->
<xsl:choose>
      <xsl:when test="count(opt:element|opt:field|opt:json-property|opt:path-index) eq 1 or count(opt:score|opt:score-order|opt:confidence-order|opt:document-order|opt:fitness-order|opt:quality-order|opt:unordered) eq 1"/>
      <xsl:otherwise><svrl:failed-assert test="count(opt:element|opt:field|opt:json-property|opt:path-index) eq 1 or count(opt:score|opt:score-order|opt:confidence-order|opt:document-order|opt:fitness-order|opt:quality-order|opt:unordered) eq 1" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	  <xsl:attribute name="id">SEARCH-SCHEMA_4</xsl:attribute>
	  <xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	  <svrl:text>An &lt;element&gt;, &lt;json-property&gt;, &lt;field&gt;, &lt;path-index&gt;, &lt;score&gt; or other ordering specification is required on &lt;sort-order&gt;.</svrl:text>
	</svrl:failed-assert></xsl:otherwise>
    </xsl:choose><xsl:apply-templates select="*|comment()|processing-instruction()" mode="M1"/></xsl:template>
  <!--RULE -->
  <xsl:template match="opt:starter | opt:joiner" priority="1004" mode="M1"><svrl:fired-rule context="opt:starter | opt:joiner" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl"/>

		<!--ASSERT -->
<xsl:choose>
      <xsl:when test="string-length(normalize-space(.)) gt 0"/>
      <xsl:otherwise><svrl:failed-assert test="string-length(normalize-space(.)) gt 0" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	  <xsl:attribute name="id">SEARCH-EMPTYGRAMMAR_1</xsl:attribute>
	  <xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	  <svrl:text>Empty or whitespace token not allowed inside &lt;starter&gt; or &lt;joiner&gt;</svrl:text>
	</svrl:failed-assert></xsl:otherwise>
    </xsl:choose><xsl:apply-templates select="*|comment()|processing-instruction()" mode="M1"/></xsl:template>
  <!--RULE -->
  <xsl:template match="opt:term" priority="1003" mode="M1"><svrl:fired-rule context="opt:term" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl"/>

		<!--ASSERT -->
<xsl:choose>
      <xsl:when test="if (@apply and @ns and @at) then count(opt:default) eq 0 else true()"/>
      <xsl:otherwise><svrl:failed-assert test="if (@apply and @ns and @at) then count(opt:default) eq 0 else true()" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	  <xsl:attribute name="id">SEARCH-SCHEMA_24</xsl:attribute>
	  <xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	  <svrl:text>Conflict between term extension (@apply, @ns, @at) and default term definition. (@apply used if both are provided)</svrl:text>
	</svrl:failed-assert></xsl:otherwise>
    </xsl:choose><xsl:apply-templates select="*|comment()|processing-instruction()" mode="M1"/></xsl:template>
  <!--RULE -->
  <xsl:template match="opt:value" priority="1002" mode="M1"><svrl:fired-rule context="opt:value" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl"/>

		<!--ASSERT -->
<xsl:choose>
      <xsl:when test="count(opt:element|opt:field|opt:json-property) ge 1"/>
      <xsl:otherwise><svrl:failed-assert test="count(opt:element|opt:field|opt:json-property) ge 1" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	  <xsl:attribute name="id">SEARCH-SCHEMA_3</xsl:attribute>
	  <xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	  <svrl:text>An &lt;element&gt;, &lt;json-property&gt; or &lt;field&gt; specification is required on &lt;value&gt;.</svrl:text>
	</svrl:failed-assert></xsl:otherwise>
    </xsl:choose><xsl:apply-templates select="*|comment()|processing-instruction()" mode="M1"/></xsl:template>
  <!--RULE -->
  <xsl:template match="opt:values" priority="1001" mode="M1"><svrl:fired-rule context="opt:values" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl"/>

		<!--ASSERT -->
<xsl:choose>
      <xsl:when test="count((opt:uri|opt:collection|opt:range|opt:geo-elem|opt:geo-elem-pair|opt:geo-attr-pair|opt:geo-json-property|opt:geo-json-property-pair|opt:geo-path|opt:geo-region-path)) eq 1"/>
      <xsl:otherwise><svrl:failed-assert test="count((opt:uri|opt:collection|opt:range|opt:geo-elem|opt:geo-elem-pair|opt:geo-attr-pair|opt:geo-json-property|opt:geo-json-property-pair|opt:geo-path|opt:geo-region-path)) eq 1" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	  <xsl:attribute name="id">SEARCH-SCHEMA_25</xsl:attribute>
	  <xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	  <svrl:text>A &lt;uri&gt; or &lt;collection&gt; or &lt;field&gt; or &lt;range&gt; or geospatial specification is required on &lt;values&gt;.</svrl:text>
	</svrl:failed-assert></xsl:otherwise>
    </xsl:choose><xsl:apply-templates select="*|comment()|processing-instruction()" mode="M1"/></xsl:template>
  <!--RULE -->
  <xsl:template match="opt:tuples" priority="1000" mode="M1"><svrl:fired-rule context="opt:tuples" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl"/>

		<!--ASSERT -->
<xsl:choose>
      <xsl:when test="count((opt:uri|opt:collection|opt:range|opt:geo-elem|opt:geo-elem-pair|opt:geo-attr-pair|opt:geo-json-property|opt:geo-json-property-pair|opt:geo-path|opt:geo-region-path)) ge 2"/>
      <xsl:otherwise><svrl:failed-assert test="count((opt:uri|opt:collection|opt:range|opt:geo-elem|opt:geo-elem-pair|opt:geo-attr-pair|opt:geo-json-property|opt:geo-json-property-pair|opt:geo-path|opt:geo-region-path)) ge 2" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
	  <xsl:attribute name="id">SEARCH-SCHEMA_26</xsl:attribute>
	  <xsl:attribute name="location"><xsl:apply-templates select="." mode="schematron-select-full-path"/></xsl:attribute>
	  <svrl:text>Two or more &lt;uri&gt; or &lt;collection&gt; or &lt;field&gt; or &lt;range&gt; or geospatial specifications are required on &lt;tuples&gt;.</svrl:text>
	</svrl:failed-assert></xsl:otherwise>
    </xsl:choose><xsl:apply-templates select="*|comment()|processing-instruction()" mode="M1"/></xsl:template>
  <xsl:template match="text()" priority="-1" mode="M1"/>
  <xsl:template match="@*|node()" priority="-2" mode="M1"><xsl:apply-templates select="*|comment()|processing-instruction()" mode="M1"/></xsl:template>
</xsl:stylesheet>
    })/svrl:schematron-output/svrl:failed-assert/<search:report id="{substring-before(string(@id),"_")}">{svrl:text/text()}</search:report>,
    (: REPLACEMENT END
       end of frequently-encountered problems section :)

    for $n in $opt/opt:forest return impl:validate-forest($n)
    ,
    (: term-option validation :)
    for $each in ($opt/opt:term|$opt/opt:constraint/(opt:word|opt:value))
    return impl:validate-options-buffet($each/opt:term-option, "term-option")
    ,
    (: extra geospatial validation (indexes are checked by check-indexes) :)
    for $each in ($opt/(opt:constraint|opt:values|opt:tuples)/(opt:geo-json-property|opt:geo-json-property-pair|opt:geo-elem-pair|opt:geo-attr-pair|opt:geo-elem))
    return (impl:validate-options-buffet($each/opt:geo-option, "geo-option"),
            if (exists($each/(self::opt:geo-json-property-pair|self::opt:geo-elem-pair|self::opt:geo-attr-pair)) and $each/opt:geo-option[starts-with(., "type=")])
            then
              <search:report id="SEARCH-NOTYPEALLOWED">The type option only applies to geo-elem and opt:geo-json-property constraints.</search:report>
            else
                ())
    ,
    (: geospatial facet validation :)
    for $each in ($opt/opt:constraint/(opt:geo-json-property|opt:geo-json-property-pair|opt:geo-elem-pair|opt:geo-attr-pair|opt:geo-elem))
    return
        if ($each/opt:facet-option and not($each/opt:heatmap))
        then
            <search:report id="SEARCH-FACETNOHEATMAP">You must specify a heatmap if you specify facet options.</search:report>
        else
            impl:validate-options-buffet($each/opt:facet-option, "geo-facet-option")
    ,
    (: range-option validation :)
    for $each in $opt/opt:constraint/(opt:geo-elem|opt:geo-elem-pair|opt:geo-attr-pair|opt:geo-json-property|opt:geo-json-property-pair|opt:range)
    return impl:validate-options-buffet($each/opt:range-option, "range-option"),
    (: facet-option validation :)
    for $each in $opt/opt:constraint/(opt:range|opt:collection)
    return impl:validate-options-buffet($each/opt:facet-option, "facet-option"),
    (: exceptions :)
    let $n := ($opt/opt:constraint/opt:range[count(opt:bucket|opt:computed-bucket) eq 0]/opt:facet-option[. eq "empties"]) |
              ($opt/opt:constraint/opt:collection/opt:facet-option[. = ("empties", "fragment-frequency", "item-frequency") or starts-with(., "type=") or starts-with(.,"collation=") or starts-with(., "timezone=")])
    for $each in $n
    return <search:report id="SEARCH-INVALIDOPT">Unrecognized option {string($each)}</search:report>
    ,
    (: suggestion-option validation :)
    for $each in $opt/(opt:suggestion-source|opt:default-suggestion-source)
    return impl:validate-options-buffet($each/opt:suggestion-option, "suggestion-option"),
    (: exceptions :)
    let $n := $opt/(opt:suggestion-source|opt:default-suggestion-source)[@ref = $opt/opt:constraint[opt:range[opt:bucket|opt:computed-bucket]]/@name/string()]/opt:suggestion-option
    for $each in $n
    return <search:report id="SEARCH-INVALIDOPT">Suggestion options are not recognized against sources based on bucketed &lt;range&gt;</search:report>
    ,
    let $n := $opt/opt:search-option return impl:validate-options-buffet($n, "search-option")
    ,
    (: values-option validation :)
    for $each in $opt/opt:values
    return impl:validate-options-buffet($each/opt:values-option, "values-option")
    ,
    (: tuples-option validation :)
    for $each in $opt/opt:tuples
    return impl:validate-options-buffet($each/opt:values-option, "tuples-option"),
    
    for $n in $opt//opt:range
    let $type := impl:range-type($n)
    return
        if ($type castable as xs:QName and contains($type, ":", $impl:UNICODE-CP-COLLATION))
        then ()
        else <search:report id="SEARCH-QNAME">Bad datatype definition on &lt;range&gt; @type</search:report>
    ,
    for $constraint in $opt/opt:constraint[opt:range/(opt:bucket|opt:computed-bucket)]
    return impl:check-bucket-attr-order($constraint)
    ,
    if ($strict and $opt/opt:term)
    then impl:validate-stemmedness($opt/opt:term)
    else ()
    ,
    (: check for required range indices and lexicons :)
    for $cs in ($opt/opt:constraint,$opt/opt:sort-order,$opt/opt:operator/opt:state/opt:sort-order, $opt/opt:term/opt:default, $opt/(opt:values|opt:tuples))
    return impl:check-indexes($cs, $strict)
    ,
    (: check constraint/operator name uniqueness :)
    impl:check-uniqueness($opt),
    
    (: make sure default term @ref is valid :)
    if ($opt/opt:term/opt:default/@ref)
    then 
        let $ref := $opt/opt:term/opt:default/@ref/string()
        return 
            if ($opt/opt:constraint[@name eq $ref])
            then ()
            else <search:report id="SEARCH-INVALIDREF">Constraint name "{$ref}" referenced by default term not found.</search:report>
    else (),
    
    (: Make sure aggregates specified on values and tuples are resolvable :)
    if ($opt/(opt:values|opt:tuples)/opt:aggregate[not(@udf)])
    then 
        for $lex in $opt/(opt:values|opt:tuples)[opt:aggregate[not(@udf)]]
        let $references :=     
            try { 
                for $i in impl:get-refspecs($lex)
                return impl:construct-reference($i)
            }
            catch ($e) {
                () (: we'll ignore failures here;issues will be caught on index check step :)
            }
        let $aggregates := $lex/search:aggregate
        for $aggregate in $aggregates
        return (
            if (impl:aggregate-valid($references,$aggregate))
            then ()
            else <search:report id="SEARCH-INVALIDAGGREGATE">Aggregate "{$aggregate/@apply/string()}" cannot be resolved on the specified index (check configuration, name, type).</search:report>
            )
    else (),
    
    (: Make sure references in metadata extraction are valid :)
    if ($opt/opt:extract-metadata/opt:constraint-value)
    then
        for $cv in $opt/opt:extract-metadata/opt:constraint-value
        return 
            if ($opt/opt:constraint[@name eq $cv/@ref]/(search:range|search:collection|search:geo-elem|search:geo-elem-pair|search:geo-attr-pair|opt:geo-json-property|opt:geo-json-property-pair))
            then ()
            else <search:report id="SEARCH-INVALIDREF">Constraint name "{$cv/@ref/string()}" referenced by extract-metadata not found, or not of appropriate type (range, collection, or geospatial).</search:report> 
    else (),

    if (count($opt/opt:searchable-expression) != 1) then ()
    else impl:do-check-searchable-expression-syntax($opt/opt:searchable-expression),

    if (empty($opt/opt:element-query)) then ()
    else <search:report id="SEARCH-DEPRECATED">Use container with element or json-property and container-constraint-query instead of element-query and element-query-constraint.</search:report>
    )
};

declare function impl:do-check-searchable-expression-syntax(
  $searchexpr as element(opt:searchable-expression)
) as element(search:report)?
{
    let $expr := $searchexpr/sql:trim(string(.))
    return
        if (not(string-length($expr) gt 0)) then ()
        else
            let $evalexpr := string-join((
                'xquery version "1.0-ml";',

                for $prefix in in-scope-prefixes($searchexpr)
                let $nsuri := namespace-uri-for-prefix($prefix,$searchexpr)
                return
                    if ($nsuri = ("http://marklogic.com/appservices/search", "http://www.w3.org/XML/1998/namespace"))
                    then ()
                    else if ($prefix ne "")
                    then concat('declare namespace ',$prefix,'="',$nsuri,'";')
                    else (
                        concat('declare default element namespace "',$nsuri,'";'),
                        concat('declare default function namespace "',$nsuri,'";')
                        ),

                'declare option xdmp:mapping "false";',
                (: hack to end the prolog and start the expression :)
                '(),',
                $expr
                ),
                '
')
            return try {
                xdmp:eval($evalexpr,(),<options xmlns="xdmp:eval"><static-check>true</static-check></options>)
            } catch ($e) {
                <search:report id="SEARCH-INVALIDSEARCHEXPR">{$expr} searchable expression has parse error {
                    $e/error:format-string/string(.)
                    }</search:report>
            }
};

declare function impl:do-check-collation-compatibility(
  $opt as element(search:options)
) as element(search:report)*
{
    let $merged-options := impl:merge-options($default-options, $opt)
    return
      (for $starter in $merged-options//search:starter/string()
       let $str := concat($starter, $COLLATION-TEST-STRING1)
       let $abcd := substring-after($str, $starter)
       where $abcd != $COLLATION-TEST-STRING1
       return
         <search:report id="SEARCH-COLLATION">The starter "{$starter}" is incompatible with the collation in use</search:report>,

       for $joiner in $merged-options//search:joiner/string()
       let $str := concat($COLLATION-TEST-STRING1, $joiner, $COLLATION-TEST-STRING2)
       let $abcd := substring-before($str, $joiner)
       let $efgh := substring-after($str, $joiner)
       where $abcd != $COLLATION-TEST-STRING1 or $efgh != $COLLATION-TEST-STRING2
       return
         <search:report id="SEARCH-COLLATION">The joiner "{$joiner}" is incompatible with the collation in use</search:report>)
};

declare function impl:aggregate-valid(
    $references as cts:reference*,
    $aggregate as element(search:aggregate)
    
) as xs:boolean
{    
    try {
        let $resolve := val:apply-aggregate($references,$aggregate,(),(),(),(),())
        return true()
     }
    catch ($e) { false() }
};


(: Checks that forest exists and that forest is in database :)

declare function impl:validate-forest($forest as xs:unsignedLong?) 
as element(search:report)?
{
  let $test := 
    try {
        if (empty($forest)) then ()
        else
            let $forest-name := xdmp:forest-name($forest)
            return
                if (xdmp:forest-name($forest) and not(xdmp:forest-databases($forest) = xdmp:database()))
                then <search:report id="SEARCH-FORESTNID">Specified forest {$forest} not in database</search:report>
                else ()
     }
    catch ($e) { 
        if ($e/error:code = "XDMP-NOFOREST")
        then <search:report id="SEARCH-NOFOREST">No such forest {$forest}</search:report>
        else xdmp:rethrow()
    } 
    return $test 
};

declare function impl:validate-stemmedness($term-opts as element(opt:term)) {
    let $test1 :=
        try {
            let $check := 
            if ($term-opts/opt:term-option[. = ('unstemmed', 'exact')] )
                then xdmp:estimate(cts:search(fn:collection(), cts:word-query("@test@", "unstemmed")))
                else ()
            return () }
        catch($e) { <search:report id="SEARCH-UNSTEMDISABLED">Unstemmed word searches not enabled for this database</search:report> }
    
    let $test2 :=
        try {
            let $check := 
            if ($term-opts/opt:term-option[. eq 'stemmed'])
                then xdmp:estimate(cts:search(fn:collection(), cts:word-query("@test@", "stemmed")))
                else ()
            return () }
        catch($e) { <search:report id="SEARCH-STEMDISABLED">Stemmed word searches not enabled for this database</search:report> }
    return ($test1, $test2)
};

(: Quick check that each constraint or sort-order option has necessary configuration to support it; includes geo :)
declare function impl:check-indexes($opt as element(), $strict as xs:boolean) 
as element(search:report)*
{
  let $type := local-name($opt)
  let $name := 
      if ($type = ("constraint","values","tuples")) then $opt/@name
      else $opt/../@name
  let $refspecs := impl:get-refspecs($opt)
  let $test-lexicons := 
      for $refspec in $refspecs
      return 
          try { 
              let $ref := impl:construct-reference($refspec)
              return ()
          }
          catch ($e) {
              if ($strict)
              then <search:report id="SEARCH-INDEXCONFIG" name="{$name}" type="{$type}">{$e/error:format-string}</search:report>
              else ()
          } 
  let $fieldspecs := $opt//(search:value|search:word)/search:field
  let $test-fields := 
    for $field in $fieldspecs
    return 
        try {
            let $estimate := xdmp:estimate(cts:search(collection(),cts:field-word-query($field/@name,"bar")))
            return ()
        }
        catch ($e) {
            if ($strict)
            then <search:report id="SEARCH-INDEXCONFIG" name="{$name}" type="{$type}">{$e/error:format-string}</search:report>
            else ()  
        }
  return ($test-lexicons,$test-fields) 
};

declare function impl:get-refspecs(
    $spec as element()
)
{
    typeswitch($spec)
        case element(search:sort-order)
        return $spec
        default return $spec//(search:range[search:element|search:path-index|search:json-property|search:field]|
                       search:uri|search:collection|search:geo-elem|search:geo-elem-pair|search:geo-attr-pair|
                       search:geo-json-property|search:geo-json-property-pair|search:geo-path)     
};

declare function impl:range-type(
    $refspec as element()
) as xs:string?
{
    let $range-type := $refspec/@type
    return
        if (exists($range-type))
        then $range-type/string(.)
        else
            (: TODO: use reference for query :)
            let $ref := impl:construct-reference($refspec)
            return
                if (empty($ref)) then ()
                else concat("xs:",cts:reference-scalar-type(head($ref)))
};

declare function impl:reference-types(
    $references as cts:reference*
) as xs:string*
{
    for $reference in $references
    return concat("xs:",cts:reference-scalar-type($reference))
};

(: Use reference constructors to do index checking, instead of dummy queries :)
declare function impl:construct-reference(
    $refspec as element()
) as cts:reference*
{
    typeswitch($refspec)
    case element(search:range)
        return impl:range-reference($refspec)
    case element(search:range-query)
        return impl:range-reference($refspec)
    case element(search:sort-order)
        return impl:range-reference($refspec)
    case element(search:collection)
        return cts:collection-reference(impl:reference-options($refspec))
    case element(search:collection-query)
        return cts:collection-reference(impl:reference-options($refspec))
    case element(search:uri)
        return cts:uri-reference()
    case element(search:geo-path)
        return impl:geo-path-reference($refspec)
    case element(search:geo-path-query)
        return impl:geo-path-reference($refspec)
    case element(search:geo-elem)
        return impl:geo-elem-reference($refspec)
    case element(search:geo-elem-query)
        return impl:geo-elem-reference($refspec)
    case element(search:geo-elem-pair)
        return impl:geo-elem-pair-reference($refspec)
    case element(search:geo-elem-pair-query)
        return impl:geo-elem-pair-reference($refspec)
    case element(search:geo-attr-pair)
        return impl:geo-attribute-pair-reference($refspec)
    case element(search:geo-attr-pair-query)
        return impl:geo-attribute-pair-reference($refspec)
    case element(search:geo-json-property)
        return impl:geo-json-property-reference($refspec)
    case element(search:geo-json-property-query)
        return impl:geo-json-property-reference($refspec)
    case element(search:geo-json-property-pair)
        return impl:geo-json-property-pair-reference($refspec)
    case element(search:geo-json-property-pair-query)
        return impl:geo-json-property-pair-reference($refspec)
    default return impl:debug-errmsg("unknown case for construct reference")
};

(: helpers for element reference construction :)
declare function impl:range-reference(
    $refspec as element()
) as cts:reference*
{
    if (exists($refspec/search:element) and exists($refspec/search:attribute)) then
        for $elemName in impl:qname-list($refspec/search:element)
        for $attName in impl:qname-list($refspec/search:attribute)
        return cts:element-attribute-reference($elemName,$attName, impl:reference-options($refspec))
    else if (exists($refspec/search:json-property)) then
        for $propName in $refspec/search:json-property/string(.)
        return cts:json-property-reference($propName,impl:reference-options($refspec))
    else if (exists($refspec/search:element)) then
        for $elemName in impl:qname-list($refspec/search:element)
        return cts:element-reference($elemName,impl:reference-options($refspec))
    else if (exists($refspec/search:field)) then 
        for $fieldName in $refspec/search:field/@name/string(.)
        return cts:field-reference($fieldName, impl:reference-options($refspec))
    else if ($refspec/search:path-index)
    then impl:path-reference($refspec)
    else ()
};

declare function impl:geo-elem-reference(
    $refspec as element()
) as cts:reference*
{
    if (exists($refspec/search:parent)) then
        for $parentName in impl:qname-list($refspec/search:parent)
        for $elemName in impl:qname-list($refspec/search:element)
        return cts:geospatial-element-child-reference(
            $parentName,$elemName,impl:reference-options($refspec)
            )
    else
        for $elemName in impl:qname-list($refspec/search:element)
        return cts:geospatial-element-reference(
            $elemName,impl:reference-options($refspec)
            )
};

declare function impl:geo-json-property-reference(
    $refspec as element()
) as cts:reference*
{
    if (exists($refspec/search:parent-property)) then
        for $parentName in $refspec/search:parent-property/string(.)
        for $propName in $refspec/search:json-property/string(.)
        return cts:geospatial-json-property-child-reference(
            $parentName,$propName,impl:reference-options($refspec)
            )
    else
        for $propName in $refspec/search:json-property/string(.)
        return cts:geospatial-json-property-reference(
            $propName,impl:reference-options($refspec)
            )
};

declare function impl:geo-elem-pair-reference(
    $refspec as element()
) as cts:reference*
{
    for $parentName in impl:qname-list($refspec/search:parent)
    for $latName in impl:qname-list($refspec/search:lat)
    for $lonName in impl:qname-list($refspec/search:lon)
    return cts:geospatial-element-pair-reference(
            $parentName,$latName,$lonName,impl:reference-options($refspec)
            )
};

declare function impl:geo-attribute-pair-reference(
    $refspec as element()
) as cts:reference*
{
    for $parentName in impl:qname-list($refspec/search:parent)
    for $latName in impl:qname-list($refspec/search:lat)
    for $lonName in impl:qname-list($refspec/search:lon)
    return cts:geospatial-attribute-pair-reference(
            $parentName,$latName,$lonName,impl:reference-options($refspec)
            )
};

declare function impl:geo-json-property-pair-reference(
    $refspec as element()
) as cts:reference*
{
    for $parentName in $refspec/search:parent-property/string(.)
    for $latName in $refspec/search:lat-property/string(.)
    for $lonName in $refspec/search:lon-property/string(.)
    return cts:geospatial-json-property-pair-reference(
            $parentName,$latName,$lonName,impl:reference-options($refspec)
            )
};

declare function impl:geo-path-reference(
    $refspec as element()
) as cts:geospatial-path-reference+
{
    let $path-indexes := $refspec/search:path-index
    let $path-opts    := impl:reference-options($refspec)
    let $bindings     :=
        for $bindable in $path-indexes
        for $prefix in in-scope-prefixes($bindable)
        return ($prefix, namespace-uri-for-prefix($prefix,$bindable))
    return
        if (empty($bindings)) then
            for $path in $path-indexes/string(.)
            return cts:geospatial-path-reference($path,$path-opts)
        else
            for $path in $path-indexes/string(.)
            return xdmp:with-namespaces($bindings,
                cts:geospatial-path-reference($path,$path-opts)
                )
};

declare function impl:path-reference(
    $refspec as element()
) as cts:path-reference+
{
    let $path-indexes := $refspec/search:path-index
    let $path-opts    := impl:reference-options($refspec)
    let $bindings     :=
        for $bindable in $path-indexes
        for $prefix in in-scope-prefixes($bindable)
        return ($prefix, namespace-uri-for-prefix($prefix,$bindable))
    return
        if (empty($bindings)) then
            for $path in $path-indexes/string(.)
            return cts:path-reference($path,$path-opts)
        else
            for $path in $path-indexes/string(.)
            return xdmp:with-namespaces($bindings,
                cts:path-reference($path,$path-opts)
                )
};

declare function impl:reference-options(
    $refspec as element()
) as xs:string*
{ 
    (
        impl:type($refspec),
        impl:collation($refspec),
        impl:coord($refspec),
        impl:nullable($refspec),
        $refspec/search:geo-option[starts-with(.,"type=")]/string()
    )
};

(: expects search:*[exists(@name)]|search:json-property :)
declare function impl:qname-list(
    $qname-list as element()*
) as xs:QName*
{
    for $qname in $qname-list
    return impl:qname($qname)
};
declare function impl:qname(
    $qname as element()?
) as xs:QName?
{
    if ($qname instance of element(search:json-property))
    then $qname/QName("",string(.))
    else $qname/QName(string(@ns),string(@name))
};

declare function impl:spec-qname-list(
    $spec-list as element()*
) as xs:QName*
{
    for $spec in $spec-list
    return impl:spec-qname($spec)
};
declare function impl:spec-qname(
    $spec as element()?
) as xs:QName?
{
    $spec/QName(string(@ns),string(@name))
};

declare function impl:spec-cts-element-list(
    $spec-list as element()*
) as element(cts:element)*
{
    for $spec in $spec-list
    return impl:spec-cts-element($spec)
};
declare function impl:spec-cts-element(
    $spec as element()?
) as element(cts:element)?
{
    let $qname := impl:spec-qname($spec)
    return
        if (empty($qname)) then ()
        else element cts:element {$qname}
};

declare function impl:spec-cts-attribute-list(
    $spec-list as element()*
) as element(cts:attribute)*
{
    for $spec in $spec-list
    return impl:spec-cts-attribute($spec)
};
declare function impl:spec-cts-attribute(
    $spec as element()?
) as element(cts:attribute)?
{
    let $qname := impl:spec-qname($spec)
    return
        if (empty($qname)) then ()
        else element cts:attribute {$qname}
};

declare function impl:spec-cts-child-list(
    $spec-list as element()*
) as element(cts:child)*
{
    for $spec in $spec-list
    return impl:spec-cts-child($spec)
};
declare function impl:spec-cts-child(
    $spec as element()?
) as element(cts:child)?
{
    let $qname := impl:spec-qname($spec)
    return
        if (empty($qname)) then ()
        else element cts:child {$qname}
};

declare function impl:spec-cts-latitude-list(
    $spec-list as element()*
) as element(cts:latitude)*
{
    for $spec in $spec-list
    return impl:spec-cts-latitude($spec)
};
declare function impl:spec-cts-latitude(
    $spec as element()?
) as element(cts:latitude)?
{
    let $qname := impl:spec-qname($spec)
    return
        if (empty($qname)) then ()
        else element cts:latitude {$qname}
};

declare function impl:spec-cts-longitude-list(
    $spec-list as element()*
) as element(cts:longitude)*
{
    for $spec in $spec-list
    return impl:spec-cts-longitude($spec)
};
declare function impl:spec-cts-longitude(
    $spec as element()?
) as element(cts:longitude)?
{
    let $qname := impl:spec-qname($spec)
    return
        if (empty($qname)) then ()
        else element cts:longitude {$qname}
};

declare function impl:elem-qname(
    $qname as element(search:qname)?
) as xs:QName
{
    if (empty($qname/@elem-name)) then () 
    else QName(string($qname/@elem-ns),string($qname/@elem-name))
};

declare function impl:attr-qname(
    $qname as element(search:qname)?
) as xs:QName
{
    if (empty($qname/@attr-name)) then () 
    else QName(string($qname/@attr-ns),string($qname/@attr-name))
};

declare function impl:type(
    $refspec as element()?
) as xs:string?
{
    if (empty($refspec)) then ()
    else 
        let $type-name := impl:type-name($refspec/@type/string(.))
        return
            if (empty($type-name)) then ()
            else concat("type=",$type-name)
};

declare function impl:type-name(
    $type as xs:string?
) as xs:string?
{
    if (empty($type) or $type eq "") then ()
    else 
        let $type-local-name := substring-after($type,":",$impl:UNICODE-CP-COLLATION)
        return
            if (empty($type-local-name) or $type-local-name eq "")
            then $type
            else $type-local-name
};

declare function impl:collation(
    $refspec as element()
) as xs:string?
{
    if (empty($refspec/@collation) or $refspec/@collation/string() = "")
    then ()
    else concat("collation=",string($refspec/@collation))
};

declare function impl:coord(
    $refspec as element()
) as xs:string?
{ 
    if (empty($refspec/@coord) or $refspec/@coord = "")
    then ()
    else concat("coordinate-system=",string($refspec/@coord))
};

declare function impl:nullable(
    $refspec as element()
) as xs:string?
{
    if ($refspec/@nullable eq fn:true())
    then "nullable"
    else ()
};

declare function impl:check-uniqueness($options as element(opt:options))
as element(search:report)*
{
    let $nodes := $options/(opt:constraint|opt:operator)
    let $names := distinct-values($nodes/@name/string())
    where count($nodes) ne count($names)
    return
        for $name in $names
        where count($nodes[@name/string() eq $name]) > 1
        return <search:report id="SEARCH-NOTUNIQUE">Operator or constraint name "{$name}" is used more than once (must be unique).</search:report>
	,
    let $nodes := $options/opt:operator/opt:state
    let $names := distinct-values($nodes/@name/string())
    where count($nodes) ne count($names)
    return
        for $name in $names
        where count($nodes[@name/string() eq $name]) > 1
        return <search:report id="SORT-NOTUNIQUE">Sort state name "{$name}" is used more than once (must be unique).</search:report>
	,
	for $constraint in $options/opt:constraint[exists(opt:range)]
    let $nodes := $constraint/opt:range/opt:bucket
    let $names := distinct-values($nodes/@name/string())
    where count($nodes) ne count($names)
    return
        for $name in $names
        where count($nodes[@name/string() eq $name]) > 1
        return <search:report id="BUCKET-NOTUNIQUE">Bucket name "{$name}" is used more than once in the "{$constraint/@name/string()}" constraint (must be unique).</search:report>,
        
    let $nodes := $options/(opt:values|opt:tuples)
    let $names := distinct-values($nodes/@name/string())
    where count($nodes) ne count($names)
    return
        for $name in $names
        where count($nodes[@name/string() eq $name]) > 1
        return <search:report id="SEARCH-NOTUNIQUE">Values or tuples configuration name "{$name}" is used more than once (must be unique).</search:report>
        
};

(: @param opts one set of related options :)
declare function impl:validate-options-buffet($opts as element()*, $option-type as xs:string) 
as element(search:report)*
{
    let $_ := debug:assert($option-type = ("term-option", "facet-option", "suggestion-option", "search-option", "geo-option","geo-facet-option","values-option","range-option","tuples-option"))
    let $valid-options := 
    <valid-options>
        <option-group name="term-option">
           <exact-value group="1">case-sensitive</exact-value>
           <exact-value group="1">case-insensitive</exact-value>
           <exact-value group="2">diacritic-sensitive</exact-value>
           <exact-value group="2">diacritic-insensitive</exact-value>
           <exact-value group="3">punctuation-sensitive</exact-value>
           <exact-value group="3">punctuation-insensitive</exact-value>
           <exact-value group="4">whitespace-sensitive</exact-value>
           <exact-value group="4">whitespace-insensitive</exact-value>
           <exact-value group="5">stemmed</exact-value>
           <exact-value group="5">unstemmed</exact-value>
           <exact-value group="6">wildcarded</exact-value>
           <exact-value group="6">unwildcarded</exact-value>
           <exact-value group="7">synonym</exact-value>
           <exact-value group="1 2 3 4 5 6">exact</exact-value>
           <starts-with ends-with="xs:language" group="7">lang=</starts-with>
           <starts-with ends-with="xs:double" group="8">max-occurs=</starts-with>
           <starts-with ends-with="xs:double" group="9">min-occurs=</starts-with>
           <starts-with ends-with="xs:double" group="10">distance-weight=</starts-with>
           <exact-value group="11">lexicon-expand=full</exact-value>
           <exact-value group="11">lexicon-expand=prefix-postfix</exact-value>
           <exact-value group="11">lexicon-expand=off</exact-value>
           <exact-value group="11">lexicon-expand=heuristic</exact-value>
           <starts-with ends-with="xs:double" group="12">lexicon-expansion-limit=</starts-with>
           <exact-value group="13">limit-check</exact-value>
           <exact-value group="13">no-limit-check</exact-value>
        </option-group>
        <option-group name="facet-option">
           <exact-value group="1">ascending</exact-value>
           <exact-value group="1">descending</exact-value>
           <exact-value group="2">empties</exact-value>
           <exact-value group="3">any</exact-value>
           <exact-value group="3">document</exact-value>
           <exact-value group="3">locks</exact-value>
           <exact-value group="3">properties</exact-value>
           <exact-value group="4">frequency-order</exact-value>
           <exact-value group="4">item-order</exact-value>
           <exact-value group="5">fragment-frequency</exact-value>
           <exact-value group="5">item-frequency</exact-value>
           <exact-value group="6">type=int</exact-value>
           <exact-value group="6">type=unsignedInt</exact-value>
           <exact-value group="6">type=long</exact-value>
           <exact-value group="6">type=unsignedLong</exact-value>
           <exact-value group="6">type=float</exact-value>
           <exact-value group="6">type=double</exact-value>
           <exact-value group="6">type=decimal</exact-value>
           <exact-value group="6">type=dateTime</exact-value>
           <exact-value group="6">type=time</exact-value>
           <exact-value group="6">type=date</exact-value>
           <exact-value group="6">type=gYearMonth</exact-value>
           <exact-value group="6">type=gYear</exact-value>
           <exact-value group="6">type=gMonth</exact-value>
           <exact-value group="6">type=gDay</exact-value>
           <exact-value group="6">type=yearMonthDuration</exact-value>
           <exact-value group="6">type=dayTimeDuration</exact-value>
           <exact-value group="6">type=string</exact-value>
           <exact-value group="6">type=anyURI</exact-value>
           <starts-with ends-with="xs:dayTimeDuration">timezone=</starts-with>
           <starts-with ends-with="xs:unsignedLong">sample=</starts-with>
           <starts-with ends-with="xs:unsignedLong">truncate=</starts-with>
           <starts-with ends-with="xs:unsignedLong">limit=</starts-with>
           <exact-value group="7">score-logtfidf</exact-value>
           <exact-value group="7">score-logtf</exact-value>
           <exact-value group="7">score-simple</exact-value>
           <exact-value group="7">score-random</exact-value>
           <exact-value group="7">score-zero</exact-value>
           <exact-value group="8">checked</exact-value>
           <exact-value group="8">unchecked</exact-value>
           <exact-value group="9">synonym</exact-value>
           <exact-value group="10">eager</exact-value>
           <exact-value group="10">lazy</exact-value>
        </option-group>
        <option-group name="suggestion-option">
           <exact-value group="1">case-sensitive</exact-value>
           <exact-value group="1">case-insensitive</exact-value>
           <exact-value group="2">diacritic-sensitive</exact-value>
           <exact-value group="2">diacritic-insensitive</exact-value>
           <exact-value group="3">ascending</exact-value>
           <exact-value group="3">descending</exact-value>
           <exact-value group="4">any</exact-value>
           <exact-value group="4">document</exact-value>
           <exact-value group="4">locks</exact-value>
           <exact-value group="4">properties</exact-value>
           <exact-value group="5">frequency-order</exact-value>
           <exact-value group="5">item-order</exact-value>
           <exact-value group="6">fragment-frequency</exact-value>
           <exact-value group="6">item-frequency</exact-value>
           <exact-value group="7">type=int</exact-value>
           <exact-value group="7">type=unsignedInt</exact-value>
           <exact-value group="7">type=long</exact-value>
           <exact-value group="7">type=unsignedLong</exact-value>
           <exact-value group="7">type=float</exact-value>
           <exact-value group="7">type=double</exact-value>
           <exact-value group="7">type=decimal</exact-value>
           <exact-value group="7">type=dateTime</exact-value>
           <exact-value group="7">type=time</exact-value>
           <exact-value group="7">type=date</exact-value>
           <exact-value group="7">type=gYearMonth</exact-value>
           <exact-value group="7">type=gYear</exact-value>
           <exact-value group="7">type=gMonth</exact-value>
           <exact-value group="7">type=gDay</exact-value>
           <exact-value group="7">type=yearMonthDuration</exact-value>
           <exact-value group="7">type=dayTimeDuration</exact-value>
           <exact-value group="7">type=string</exact-value>
           <exact-value group="7">type=anyURI</exact-value>
           <starts-with ends-with="xs:dayTimeDuration">timezone=</starts-with>
           <starts-with ends-with="xs:unsignedLong">sample=</starts-with>
           <starts-with ends-with="xs:unsginedLong">truncate=</starts-with>
           <exact-value group="8">score-logtfidf</exact-value>
           <exact-value group="8">score-logtf</exact-value>
           <exact-value group="8">score-simple</exact-value>
           <exact-value group="8">score-random</exact-value>
           <exact-value group="8">score-zero</exact-value>
           <exact-value group="9">checked</exact-value>
           <exact-value group="9">unchecked</exact-value>
       </option-group>
       {$search-options-group}
       <option-group name="geo-option">
           <exact-value group="1">coordinate-system=wgs84</exact-value>
           <exact-value group="1">coordinate-system=etrs89</exact-value>
           <exact-value group="1">coordinate-system=raw</exact-value>
           <exact-value group="1">coordinate-system=wgs84/double</exact-value>
           <exact-value group="1">coordinate-system=etrs89/double</exact-value>
           <exact-value group="1">coordinate-system=raw/double</exact-value>
           <exact-value group="2">units=miles</exact-value>
           <exact-value group="2">units=km</exact-value>
           <exact-value group="3">boundaries-included</exact-value>
           <exact-value group="3">boundaries-excluded</exact-value>
           <exact-value group="4">boundaries-latitude-excluded</exact-value>
           <exact-value group="5">boundaries-longitude-excluded</exact-value>
           <exact-value group="6">boundaries-south-excluded</exact-value>
           <exact-value group="7">boundaries-north-excluded</exact-value>
           <exact-value group="8">boundaries-east-excluded</exact-value>
           <exact-value group="9">boundaries-west-excluded</exact-value>
           <exact-value group="10">boundaries-circle-excluded</exact-value>
           <exact-value group="11">cached</exact-value>
           <exact-value group="11">uncached</exact-value>
           <exact-value group="11">cached-incremental</exact-value>
           <exact-value group="12">type=point</exact-value>
           <exact-value group="12">type=long-lat-point</exact-value>
           <exact-value group="13">score-function=zero</exact-value>
           <exact-value group="13">score-function=reciprocal</exact-value>
           <exact-value group="13">score-function=linear</exact-value>
           <exact-value group="14">synonym</exact-value>
           <starts-with ends-with="xs:double">slope-factor=</starts-with>
        </option-group>        
        <option-group name="geo-facet-option">
           <exact-value group="1">ascending</exact-value>
           <exact-value group="1">descending</exact-value>
           <exact-value group="2">empties</exact-value>
           <exact-value group="3">any</exact-value>
           <exact-value group="3">document</exact-value>
           <exact-value group="3">locks</exact-value>
           <exact-value group="3">properties</exact-value>
           <exact-value group="4">frequency-order</exact-value>
           <exact-value group="4">item-order</exact-value>
           <exact-value group="5">fragment-frequency</exact-value>
           <exact-value group="5">item-frequency</exact-value>
           <exact-value group="6">gridded</exact-value>
           <starts-with ends-with="xs:unsignedLong">sample=</starts-with>
           <starts-with ends-with="xs:unsignedLong">truncate=</starts-with>
           <starts-with ends-with="xs:unsignedLong">limit=</starts-with>
           <starts-with ends-with="xs:anyURI">coordinate-system=</starts-with>
           <exact-value group="7">score-logtfidf</exact-value>
           <exact-value group="7">score-logtf</exact-value>
           <exact-value group="7">score-simple</exact-value>
           <exact-value group="7">score-random</exact-value>
           <exact-value group="7">score-zero</exact-value>
           <exact-value group="8">checked</exact-value>
           <exact-value group="8">unchecked</exact-value>
           <exact-value group="9">synonym</exact-value>
           <exact-value group="10">eager</exact-value>
           <exact-value group="10">lazy</exact-value>
        </option-group>
        <option-group name="values-option">
           <exact-value group="1">ascending</exact-value>
           <exact-value group="1">descending</exact-value>
           <exact-value group="2">any</exact-value>
           <exact-value group="2">document</exact-value>
           <exact-value group="2">locks</exact-value>
           <exact-value group="2">properties</exact-value>
           <exact-value group="3">frequency-order</exact-value>
           <exact-value group="3">item-order</exact-value>
           <exact-value group="4">fragment-frequency</exact-value>
           <exact-value group="4">item-frequency</exact-value>
           <exact-value group="5">type=int</exact-value>
           <exact-value group="5">type=unsignedInt</exact-value>
           <exact-value group="5">type=long</exact-value>
           <exact-value group="5">type=unsignedLong</exact-value>
           <exact-value group="5">type=float</exact-value>
           <exact-value group="5">type=double</exact-value>
           <exact-value group="5">type=decimal</exact-value>
           <exact-value group="5">type=dateTime</exact-value>
           <exact-value group="5">type=time</exact-value>
           <exact-value group="5">type=date</exact-value>
           <exact-value group="5">type=gYearMonth</exact-value>
           <exact-value group="5">type=gYear</exact-value>
           <exact-value group="5">type=gMonth</exact-value>
           <exact-value group="5">type=gDay</exact-value>
           <exact-value group="5">type=yearMonthDuration</exact-value>
           <exact-value group="5">type=dayTimeDuration</exact-value>
           <exact-value group="5">type=string</exact-value>
           <exact-value group="5">type=anyURI</exact-value>
           <!-- collation provided on range wrapper, ignore here -->
           <starts-with ends-with="xs:dayTimeDuration">timezone=</starts-with>
           <starts-with ends-with="xs:unsignedLong">limit=</starts-with>
           <starts-with ends-with="xs:unsignedLong">skip=</starts-with>
           <starts-with ends-with="xs:unsignedLong">sample=</starts-with>
           <starts-with ends-with="xs:unsginedLong">truncate=</starts-with>
           <exact-value group="6">score-logtfidf</exact-value>
           <exact-value group="6">score-logtf</exact-value>
           <exact-value group="6">score-simple</exact-value>
           <exact-value group="6">score-random</exact-value>
           <exact-value group="6">score-zero</exact-value>
           <exact-value group="7">checked</exact-value>
           <exact-value group="7">unchecked</exact-value>
           <exact-value group="8">too-many-positions-error</exact-value>
           <!-- ignore concurrent, use global option when implementing  - TODO strip in impl -->
           <!-- ignore map option - TODO strip in impl -->
           <exact-value group="9">eager</exact-value>
           <exact-value group="9">lazy</exact-value>
        </option-group>
        <option-group name="range-option">
           <exact-value group="1">score-function=zero</exact-value>
           <exact-value group="1">score-function=reciprocal</exact-value>
           <exact-value group="1">score-function=linear</exact-value>
           <starts-with ends-with="xs:double" group="2">max-occurs=</starts-with>
           <starts-with ends-with="xs:double" group="3">min-occurs=</starts-with>
           <exact-value group="4">cached</exact-value>
           <exact-value group="4">uncached</exact-value>
           <exact-value group="4">cached-incremental</exact-value>
           <exact-value group="5">synonym</exact-value>
           <starts-with ends-with="xs:double" group="6">slope-factor=</starts-with>
       </option-group>
        <option-group name="tuples-option">
           <exact-value group="1">ascending</exact-value>
           <exact-value group="1">descending</exact-value>
           <exact-value group="2">any</exact-value>
           <exact-value group="2">document</exact-value>
           <exact-value group="2">locks</exact-value>
           <exact-value group="2">properties</exact-value>
           <exact-value group="3">frequency-order</exact-value>
           <exact-value group="3">item-order</exact-value>
           <exact-value group="4">fragment-frequency</exact-value>
           <exact-value group="4">item-frequency</exact-value>
           <exact-value group="5">type=int</exact-value>
           <exact-value group="5">type=unsignedInt</exact-value>
           <exact-value group="5">type=long</exact-value>
           <exact-value group="5">type=unsignedLong</exact-value>
           <exact-value group="5">type=float</exact-value>
           <exact-value group="5">type=double</exact-value>
           <exact-value group="5">type=decimal</exact-value>
           <exact-value group="5">type=dateTime</exact-value>
           <exact-value group="5">type=time</exact-value>
           <exact-value group="5">type=date</exact-value>
           <exact-value group="5">type=gYearMonth</exact-value>
           <exact-value group="5">type=gYear</exact-value>
           <exact-value group="5">type=gMonth</exact-value>
           <exact-value group="5">type=gDay</exact-value>
           <exact-value group="5">type=yearMonthDuration</exact-value>
           <exact-value group="5">type=dayTimeDuration</exact-value>
           <exact-value group="5">type=string</exact-value>
           <exact-value group="5">type=anyURI</exact-value>
           <!-- collation provided on range wrapper, ignore here -->
           <starts-with ends-with="xs:dayTimeDuration">timezone=</starts-with>
           <starts-with ends-with="xs:unsignedLong">limit=</starts-with>
           <starts-with ends-with="xs:unsignedLong">skip=</starts-with>
           <starts-with ends-with="xs:unsignedLong">sample=</starts-with>
           <starts-with ends-with="xs:unsginedLong">truncate=</starts-with>
           <exact-value group="6">score-logtfidf</exact-value>
           <exact-value group="6">score-logtf</exact-value>
           <exact-value group="6">score-simple</exact-value>
           <exact-value group="6">score-random</exact-value>
           <exact-value group="6">score-zero</exact-value>
           <exact-value group="7">checked</exact-value>
           <exact-value group="7">unchecked</exact-value>
           <exact-value group="8">too-many-positions-error</exact-value>
           <exact-value group="9">eager</exact-value>
           <exact-value group="9">lazy</exact-value>
           <exact-value group="10">ordered</exact-value>
           <starts-with ends-with="xs:unsignedLong">proximity=</starts-with>
           <!-- ignore concurrent, use global option when implementing  - TODO strip in impl -->
        </option-group>
    </valid-options>
    let $option-group := $valid-options/option-group[@name eq $option-type]    
    let $string-opts := distinct-values(data($opts))
    let $exact-vals := $option-group/exact-value
    let $starts-withs := $option-group/starts-with
    let $groups := 
        for $opt in $string-opts
        return tokenize(($exact-vals[data(.) eq $opt]|$starts-withs[starts-with($opt, .)])/@group, " ")
    return (
        for $opt in $opts 
        let $group := $exact-vals[data(.) eq $opt]/@group
        return
            if (starts-with($opt, "collation="))
            then <search:report id="SEARCH-USEATTROPT">Search API does not recognize collation= options, use the attribute @collation instead</search:report>
            else if ($opt = data($exact-vals))
                 then ()
                 else if (some $sw in $starts-withs satisfies starts-with($opt, $sw))
                      then ()
                      else <search:report id="SEARCH-INVALIDOPT">Invalid &lt;{$option-type}&gt;: {string($opt)}</search:report>
        ,
        for $group in distinct-values($groups)
        return
            if (count($groups[. eq $group]) > 1)
            then <search:report id="SEARCH-CONFLICTOPT">Conflicting &lt;{$option-type}&gt; only one of the following allowed: {data(($exact-vals|$starts-withs)[$group = tokenize(@group, " ")])}</search:report> 
            else ()
    )
};

declare function impl:check-bucket-attr-order($constraint as element(opt:constraint))
(: as element(search:report)* :)
{
    let $buckets := impl:resolve-buckets($constraint)
    let $type := impl:range-type($constraint/opt:range)
    for $key in map:keys($buckets)
    let $bucket := map:get($buckets,$key)
    let $lt := map:get($bucket,"lt")
    let $ge := map:get($bucket,"ge")
    let $ascending := 
        try {
            if (exists($lt) and exists($ge)) 
            then 
                if (impl:cast($type,$ge) < impl:cast($type,$lt))
                then true()
                else false()
            else ()
        }
        catch ($e) {
            <search:report id="SEARCH-INVALBUCKET">Constraint: {data($constraint/@name)} Bucket: {$key} Error: {$e/error:message}</search:report>}

    return 
        if ($ascending instance of element(search:report)) 
        then $ascending
        else 
            if ($ascending eq false())
            then  <search:report id="SEARCH-INVALBUCKET">Constraint:{data($constraint/@name)} Bucket:{$key}  @ge must be less than @lt</search:report>
            else ()
};

declare function impl:do-unparse($query as element()*, $strength as xs:integer) as xs:string+ {
    if ($query/@qtextmulti eq 1)
    then for $q in $query/* return normalize-space(string-join((impl:unparse-expr($q, 0))," "))
    else normalize-space(string-join((impl:unparse-expr($query, 0))," "))
};

declare function impl:unparse-expr($query as element()*, $strength as xs:integer) as xs:string {
    normalize-space(
            if (empty($query) or $query/@qtextempty)
            then string-join(for $oper in $query/cts:annotation[@operator-ref] return impl:unparse-oper($oper), " ")
            else if ($query/@qtextref)
                 then impl:unparse-deref($query)
                 else if ($query/@qtextjoin)
                      then impl:unparse-infix($query, $strength)
                      else if ($query/@qtextconst)
                           then impl:unparse-constraint($query)
                           else if ($query/@qtextstart)
                                then impl:unparse-starter($query)
                                     else if ($query/@operator-ref)
                                          then impl:unparse-oper($query)
                                          else error((), "SEARCH-NONANNOTATED", ())
    )
};

declare function impl:unparse-oper($query as element()*) as xs:string {
    for $q in $query return $q/@qtextconst/string()
};

declare function impl:unparse-deref($query as element()) as xs:string {
    string-join((
        $query/@qtextpre/string(),
        
        if ($query/@qtextref = ("cts:text","cts:value"))
        then $query/(cts:text|cts:value)/string()
        else if ($query/@qtextref eq "cts:uri")
             then $query/cts:uri/string()
             else if ($query/@qtextref eq "cts:annotation")
                  then impl:unparse-deref($query/cts:annotation)
                  else if ($query/@qtextref eq "following-sibling::cts:text")
                       then $query/following-sibling::cts:text/string()
                       else if ($query/@qtextref eq "following-sibling::cts:value")
                            then $query/following-sibling::cts:value/string()
                            else if ($query/@qtextref eq "following-sibling::cts:uri")
                                 then $query/following-sibling::cts:uri/string()
                                 else if ($query/@qtextref eq "following-sibling::cts:region")
                                      then $query/following-sibling::cts:region/string()
                                      else if ($query/@qtextref eq "schema-element(cts:query)") (: anything else cts:* is a deref :)
                                           then
                                               let $child-query := $query/schema-element(cts:query)
                                               return
                                                   if (empty($child-query))
                                                   then concat("===*error===> can't follow ref to ", $query/@qtextref)
                                                   else impl:unparse-deref($child-query)
                                           else impl:unparse-expr($query, xs:int(($query/@strength, 0)[1])+1), (: sub-query, possibly implicit :)

        $query/@qtextpost/string()
    ), "")
};

declare function impl:unparse-infix($query as element(), $parentstr as xs:integer) as xs:string {
    let $op := ($query/@qtextjoin, "")[1]
    let $group-chars := tokenize($query/@qtextgroup, "\s+")
    let $open-group := ($group-chars[1], "(")[1]
    let $close-group := ($group-chars[2], ")")[1]
    let $kids := $query/(* except (cts:annotation[count(@operator-ref) eq 0] | cts:option))
    let $numkids := count($kids)
    let $localstr := if ($query/@strength) then xs:integer($query/@strength) else 0
    return
    string-join((
        if ($parentstr gt $localstr) then $open-group else "",
        
        for $kid at $pos in $kids return (impl:unparse-expr($kid, $localstr), if ($pos ne $numkids) then $op else ()),

        if ($parentstr gt $localstr) then $close-group else ""
    ), " ")
};

declare function impl:unparse-constraint($query as element()) as xs:string {
    $query/@qtextconst/string()
};

declare function impl:unparse-starter($query as element()) as xs:string {
    let $op := ($query/@qtextstart, "")[1]
    let $localstr := xs:integer(($query/@strength, 0)[1])
    let $kids := $query/(* except (cts:annotation[count(@operator-ref) eq 0] | cts:option))
    let $numkids := count($kids)
    return
    string-join((
        $query/@qtextstart/string(),

        for $kid at $pos in $kids return (impl:unparse-expr($kid, $localstr), if ($pos ne $numkids) then " " else ())
    ), "")
};

(:=================  remove parsed expression ==============:)

declare function impl:do-remove-constraint(
  $qtext as xs:string, 
  $partial as xs:string,
  $options as element(opt:options)?)
as xs:string?
{
    let $options := impl:merge-options($default-options,$options)
    let $full := impl:do-tokenize-parse-nomerge($qtext,$options,false())
    let $part := 
        let $parsed := impl:do-tokenize-parse-nomerge($partial,$options,false())
        return 
            if ($parsed[node-name(.) = (xs:QName("cts:and-query"),xs:QName("cts:word-query"))][cts:annotation/@operator-ref]) 
            then $parsed/cts:annotation
            else $parsed
    let $target := impl:locate($full,$part)
    let $pruned := impl:prune($full,$target)
    return (
        if (not($debug:DEBUG)) then () else debug:log(("full:",$full,"target:",$target,"pruned:",$pruned)),
        impl:do-unparse($pruned,0)
        )  
};

declare function impl:locate(
  $full as element(),
  $target as element()) 
as element()*
{
    let $nested := exists($full/../@qtextref eq "schema-element(cts:query)") and not(node-name($full/..) = (xs:QName("cts:negative"), xs:QName("cts:positive")))
    return 
        if (deep-equal($full,$target) and not($nested))
        then $full
        else 
            for $child in $full/* 
            return impl:locate($child,$target)
};

declare function impl:prune(
  $node as element(),
  $target as element()*)
as element()?
{
    let $pruned := impl:do-prune($node,$target) 
    return 
        if (empty($pruned) or deep-equal($node, $pruned)) then $pruned
        else impl:prune($pruned,$target)
};

declare function impl:do-prune(
  $node as element(), 
  $target as element()*)
as element()?
{
    if ( ($node intersect $target) ) 
    then () 
    else typeswitch($node) (: recurse through all composable queries :)
        case element(cts:and-query) return 
            impl:prune-children($node,$target) 
        case element(cts:or-query) return 
            impl:prune-children($node,$target)
        case element(cts:near-query) return
            impl:prune-children($node,$target)
        case element(cts:and-not-query) return 
            impl:prune-and-not($node,$target)
        case element(cts:boost-query) return
            impl:prune-boost($node,$target)
        case element(cts:not-in-query) return
            impl:prune-not-in($node,$target)
        case element(cts:not-query) return 
            impl:prune-child($node,$target)
        case element(cts:element-query) return 
            impl:prune-child($node,$target)
        case element(cts:properties-fragment-query) return
            impl:prune-child($node,$target)
        case element(cts:document-fragment-query) return
            impl:prune-child($node,$target)
        case element(cts:negative) return
            if (empty($node/*)) then $node else 
                element cts:negative {
                    $node/@*,
                    impl:do-prune($node/*,$target)
                    }
        case element(cts:positive) return
            if (empty($node/*)) then $node else 
                element cts:positive {
                    $node/@*,
                    impl:do-prune($node/*,$target)
                    }
        (: return leaf-level queries as-is including element and attributes with namespaces and annotations :)
        default return prns:copy($node)
};

(: used for or, and, near :)
declare function impl:prune-children(
  $node as schema-element(cts:query),
  $target as element()*)
as schema-element(cts:query)?
{
    let $children := $node/*[. instance of schema-element(cts:query)] except $target
    return (
        if (not($debug:DEBUG)) then () else debug:log(("target: ",$target," in prune-children: ",$children)),
        switch(count($children))
        case 0 return
            if (exists($node/cts:annotation except $target)) then (: handle dangling annotations :)
                element cts:and-query {
                    attribute qtextempty {"1"},
                    for $c in $node/* except $target return impl:prune($c,$target)
                    }
            else () 
        case 1 return
            if (empty($node/cts:annotation except $target)) 
            then impl:prune($children,$target)
            else element {node-name($node)} {
                $node/@*,
                for $c in $node/* except $target return impl:prune($c,$target)
                }
        default return
            element {node-name($node)} {
                $node/@*,
                for $c in $node/* except $target return impl:prune($c,$target)
                }
        )
};

(: used for not, element, properties, document-fragment queries :)
declare function impl:prune-child(
  $node as schema-element(cts:query),
  $target as element()*)
as schema-element(cts:query)?
{ 
    if (exists($node/schema-element(cts:query) except $target)) then 
        element {node-name($node)} {
            $node/@*,
            for $c in $node/* except $target return impl:prune($c,$target)
            }
    else (: handle dangling annotations :) 
        element cts:and-query {
            attribute qtextempty {"1"},
            $node/cts:annotation except $target
            } 
};

declare function impl:prune-boost(
  $node as schema-element(cts:query),
  $target as element()*)
as schema-element(cts:query)?
{
    let $matching := $node/cts:matching-query/*[. instance of schema-element(cts:query)]
    let $boosting := $node/cts:boosting-query/*[. instance of schema-element(cts:query)]
    return
        if (exists($matching intersect $target) or empty($matching)) then
            if (exists($node/cts:annotation except $target)) then (: handle dangling annotations :) 
                element cts:and-query {
                    attribute qtextempty {"1"},
                    $node/cts:annotation except $target
                    } 
            else ()
        else if (exists($boosting intersect $target) or empty($boosting)) then 
            if (exists($node/cts:annotation)) then 
                element cts:and-query {
                    $node/@*,
                    impl:prune($matching,$target), $node/cts:annotation
                    }
            else impl:prune($matching,$target)
        else 
            element {node-name($node)} { 
                $node/@*,
                for $c in $node/* except $target return impl:prune($c,$target)
                }    
};

declare function impl:prune-not-in(
  $node as schema-element(cts:query),
  $target as element()*)
as schema-element(cts:query)?
{
    let $pos := $node/cts:positive/*[. instance of schema-element(cts:query)]
    let $neg := $node/cts:negative/*[. instance of schema-element(cts:query)]
    return
        if (exists($pos intersect $target) or empty($pos)) then
            if ($node/cts:annotation except $target) then (: handle dangling annotations :) 
                element cts:and-query {
                    attribute qtextempty {"1"},
                    $node/cts:annotation except $target
                    } 
            else ()
        else if (exists($neg intersect $target) or empty($neg)) then 
            if ($node/cts:annotation) then 
                element cts:and-query {
                    $node/@*,
                    impl:prune($pos,$target), $node/cts:annotation
                    }
            else impl:prune($pos,$target)
        else 
            element {node-name($node)} { 
                $node/@*,
                for $c in $node/* except $target return impl:prune($c,$target)
                }    
};

(: special case for and-not-query ; promote positive or make not-query from negative :)
declare function impl:prune-and-not(
  $node as schema-element(cts:query),
  $target as element()*)
as schema-element(cts:query)?
{
    let $pos := $node/cts:positive/*[. instance of schema-element(cts:query)]
    let $neg := $node/cts:negative/*[. instance of schema-element(cts:query)]
    return
        if ((exists($pos intersect $target) or empty($pos)) and ($neg intersect $target or empty($neg))) then
            if (exists($node/cts:annotation except $target)) then (: handle dangling annotations :) 
                element cts:and-query {
                    attribute qtextempty {"1"},
                    $node/cts:annotation except $target
                    } 
            else ()
        else if (exists($pos intersect $target) or empty($pos)) then 
            element cts:not-query {
                attribute qtextstart {"-"},
                attribute strength {"40"},
                impl:prune($neg,$target), $node/cts:annotation
                }
        else if (exists($neg intersect $target) or empty($neg)) then 
            if ($node/cts:annotation) then 
                element cts:and-query {
                    $node/@*,
                    impl:prune($pos,$target),$node/cts:annotation
                    }
            else impl:prune($pos,$target)
        else 
            element {node-name($node)} {
                $node/@*,
                for $c in $node/* except $target return impl:prune($c,$target)
                }    
};

(: Metadata extraction :)
declare function impl:extract-metadata(
    $result      as node(),
    $options     as element(search:options),
    $extractrefs as json:array?,
    $as          as xs:int,
    $metrics     as map:map?
) as element(search:metadata)?
{
    if (empty($options/search:extract-metadata)) then ()
    else
        let $metrics-elapsed :=
            if (empty($metrics)) then ()
            else map:get($metrics,"meta-elapsed")
        let $metrics-start   :=
            if (empty($metrics)) then ()
            else xdmp:elapsed-time()
        return
            <search:metadata>{
                typeswitch($result)
                case text()   return ()
                case binary() return ()
                case document-node() return 
                    if ($result/object-node()) 
                    then impl:extract-properties($result/object-node(),$options)
                    else impl:extract-nodes($result,$options)
                default return
                    impl:extract-nodes($result,$options),

                if (empty($extractrefs)) then ()
                else impl:extract-index-values($result,$extractrefs),

                if (empty($metrics)) then ()
                else map:put($metrics, "meta-elapsed",
                    if (empty($metrics-elapsed))
                    then (xdmp:elapsed-time() - $metrics-start)
                    else ((xdmp:elapsed-time() + $metrics-elapsed) - $metrics-start)
                    )
            }</search:metadata>
};

declare function impl:extract-properties(
    $result as node(),
    $options as element(search:options)
) as element()*
{
    for $target in $options/search:extract-metadata/search:json-property
    let $prop-name := impl:qname($target)
    return
        for $prop in $result//*[node-name(.) eq $prop-name]
        return element {$prop-name} {data($prop)} 
};

(: test, verify: go to xslt for efficiency? :)
declare function impl:extract-nodes(
    $result as node(),
    $options as element(search:options)
) as element()*
{
    for $qname in $options/search:extract-metadata/search:qname
    let $elem-qname := impl:elem-qname($qname)
    return
        if ($qname/@attr-name) 
        then 
            let $attr-qname      := impl:attr-qname($qname)
            let $name-att        := attribute name {$qname/string(@attr-name)}
            let $ns-att          :=
                let $attr-ns := $qname/@attr-ns/string(.)[not(. eq "")]
                return
                    if (empty($attr-ns)) then ()
                    else attribute ns {$attr-ns}
            let $parent-name-att := attribute parent-name {$qname/string(@elem-name)}
            let $parent-ns-att   :=
                let $elem-ns := $qname/@elem-ns/string(.)[not(. eq "")]
                return
                    if (empty($elem-ns)) then ()
                    else attribute parent-ns {$elem-ns}
            for $attr in $result//*[node-name(.) eq $elem-qname]/@*[node-name(.) eq $attr-qname]
            return 
                element {"search:attribute-meta"} { 
                    $ns-att, $name-att,
                    $parent-ns-att, $parent-name-att,
                    string($attr)}
        else $result//*[node-name(.) eq $elem-qname]
};

declare function impl:extract-index-values(
    $result      as node(),
    $extractrefs as json:array
) as element()*
{
    (: TODO: single OR-related query for result documents in page :)
    let $doc-query := cts:document-query(xdmp:node-uri($result))
    for $i in 1 to json:array-size($extractrefs)
    let $index-props     := $extractrefs[$i]
    let $constraint-name := head($index-props)
    let $ref             := subsequence($index-props,2,1)
    let $scope           := subsequence($index-props,3,1)
    for $val in cts:values($ref,(),$scope,$doc-query)
    return <search:constraint-meta name="{$constraint-name}">{$val}</search:constraint-meta>
};

declare function impl:make-extractrefs(
    $options as element(search:options)?
) as json:array?
{
    if (empty($options/search:extract-metadata)) then ()
    else 
        let $extractrefs := json:array()
        return (
            for $cv in $options/search:extract-metadata/search:constraint-value
            let $constraint-name := $cv/@ref/data(.)
            let $constraint      := $options/opt:constraint[@name eq $constraint-name]
            return
                (: ignore if no valid constraint :) 
                if (empty($constraint)) then ()
                else 
                    let $gfs := impl:gfs($constraint)
                    let $lfs := impl:lfs($constraint)
                    let $scope :=
                        if ($lfs eq "properties" or
                            ($gfs eq "properties" and $lfs ne "documents"))
                        then "properties"
                        else ()
                    let $ref  := impl:construct-reference(impl:get-refspecs($constraint))
                    return json:array-push($extractrefs,($constraint-name,$ref,$scope)),

            if (json:array-size($extractrefs) eq 0) then ()
            else $extractrefs
            )
};

(: Dynamic casting is costly; use a switch for common cases for range indexes in the Admin UI :) 
declare function impl:cast(
    $type as xs:string,
    $val  as item()?
) as item()?
{
    if (empty($val)) then ()
    else
        switch($type)
        case "xs:anyURI"            return xs:anyURI($val)
        case "xs:boolean"           return xs:boolean($val)
        case "xs:date"              return xs:date($val)
        case "xs:dateTime"          return xs:dateTime($val)
        case "xs:dayTimeDuration"   return xs:dayTimeDuration($val)
        case "xs:decimal"           return xs:decimal($val)
        case "xs:double"            return xs:double($val)
        case "xs:float"             return xs:float($val)
        case "xs:gYearMonth"        return xs:gYearMonth($val)
        case "xs:gYear"             return xs:gYear($val)
        case "xs:gMonth"            return xs:gMonth($val)
        case "xs:gDay"              return xs:gDay($val)
        case "xs:int"               return xs:int($val)
        case "xs:long"              return xs:long($val)
        case "xs:string"            return xs:string($val) 
        case "xs:time"              return xs:time($val)
        case "xs:unsignedInt"       return xs:unsignedInt($val)
        case "xs:unsignedLong"      return xs:unsignedLong($val)
        case "xs:yearMonthDuration" return xs:yearMonthDuration($val)
        default return
            if (not(contains($type, ":", $impl:UNICODE-CP-COLLATION)))
            then impl:cast(concat("xs:", $type), $val)
            else hof:cast-as(
                $impl:XML_SCHEMA_NS_URI,
                substring-after($type,":",$impl:UNICODE-CP-COLLATION),
                $val
                )
};

declare variable $impl:MAX-RULE-MATCH := 1000000000000000; (: large unsigned long :)
declare function impl:raw-search(
    $structured-query as element(search:query)?, 
    $string-query as xs:string?,
    $delta-options as element(search:options)?,
    $start as xs:unsignedLong?,
    $page-length as xs:unsignedLong?
) as node()*
{
    let $page-length := if (empty($page-length)) then $impl:MAX-RULE-MATCH else $page-length
    return impl:do-query( ($string-query), $delta-options, $structured-query, $start, $page-length, true())
};

declare function impl:debug-error(
    $errors as element(error:error)*
) as empty-sequence()
{
    let $first-err  := $errors[1]
    let $format-msg := $first-err/error:format-string/string(.)
    return impl:debug-errmsg(
        if (exists($format-msg))
        then $format-msg
        else $first-err/error:message/string()
        )
};

declare function impl:debug-errmsg(
    $msg as xs:string
) as empty-sequence()
{
    debug:log("searchapi", concat("Search API error: ",$msg))
};
