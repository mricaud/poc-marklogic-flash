xquery version "1.0-ml";

(: Copyright 2002-2020 MarkLogic Corporation.  All Rights Reserved. :)

module namespace search = "http://marklogic.com/appservices/search";
declare default function namespace "http://www.w3.org/2005/xpath-functions";

import module namespace ast = "http://marklogic.com/appservices/search-ast" at "ast.xqy";
import module namespace impl = "http://marklogic.com/appservices/search-impl" at "search-impl.xqy";
import module namespace snip = "http://marklogic.com/appservices/search-snippet" at "/MarkLogic/appservices/search/snippet.xqy";
import module namespace suggest = "http://marklogic.com/appservices/search-suggest" at "/MarkLogic/appservices/search/suggest.xqy";
import module namespace val = "http://marklogic.com/appservices/search-values" at "/MarkLogic/appservices/search/values.xqy";

declare option xdmp:mapping "false";

declare function search:parse($qtext as xs:string+) as item()? {
    search:parse($qtext,$impl:default-options,())
};

declare function search:parse($qtext as xs:string+, $options as element(search:options)?) as item()? {
    search:parse($qtext,$options,()) 
};

declare function search:parse($qtext as xs:string+, $options as element(search:options)?, $output as xs:string?) as item()? {
    if ($output = ("element(search:query)", "search:query"))
    then ast:do-tokenize-parse($qtext,    head(($options, $impl:default-options)), false())
    else if ($output eq "cts:annotated-query")
    then impl:do-tokenize-parse($qtext,   head(($options, $impl:default-options)), false())
    else if ($output eq "cts:query")
    then ast:do-parse-constructed($qtext, head(($options, $impl:default-options)))
    else if ($output eq "schema-element(cts:query)" or empty($output))
    then ast:do-parse-serialize($qtext,   head(($options, $impl:default-options)))
    else fn:error((),"SEARCH-INVALARGS","output must be 'search:query', 'cts:query' or 'cts:annotated-query'")
};

declare function search:unparse($qtree as element()) as xs:string+ {
    impl:do-unparse($qtree, 0)
};

declare function search:search(
    $qtext as xs:string+
) as node()*
{
    impl:do-query($qtext, (), (), (), (), false())
};
declare function search:search(
    $qtext as xs:string+, $options as element(search:options)?
) as node()*
{
    impl:do-query($qtext, $options, (), 1, (), false())
};
declare function search:search(
    $qtext as xs:string+, $options as element(search:options)?, $start as xs:unsignedLong?
) as node()*
{
    impl:do-query($qtext, $options, (), $start, (), false())
};
declare function search:search(
    $qtext as xs:string+, $options as element(search:options)?, $start as xs:unsignedLong?, $page-length as xs:unsignedLong?
) as node()*
{
    impl:do-query($qtext, $options, (), $start, $page-length, false())
};

declare function search:resolve(
    $query as item()
) as node()*
{
    impl:do-query((), (), $query, 1, (), false())
};
declare function search:resolve(
    $query as item(), $options as element(search:options)?
) as node()*
{
    impl:do-query((), $options, $query, 1, (), false())
};
declare function search:resolve(
    $query as item(), $options as element(search:options)?, $start as xs:unsignedLong?
) as node()*
{
    impl:do-query((), $options, $query, $start, (), false())
};
declare function search:resolve(
    $query as item(), $options as element(search:options)?, $start as xs:unsignedLong?, $page-length as xs:unsignedLong?
) as node()*
{
    impl:do-query((), $options, $query, $start, $page-length, false())
};

declare function search:resolve-nodes(
    $query as item()
) as node()*
{
    impl:do-query((), (), $query, 1, (), true())
};
declare function search:resolve-nodes(
    $query as item(), $options as element(search:options)?
) as node()*
{
    impl:do-query((), $options, $query, 1, (), true())
};
declare function search:resolve-nodes(
    $query as item(), $options as element(search:options)?, $start as xs:unsignedLong?
) as node()*
{
    impl:do-query((), $options, $query, $start, (), true())
};
declare function search:resolve-nodes(
    $query as item(), $options as element(search:options)?, $start as xs:unsignedLong?, $page-length as xs:unsignedLong?
) as node()*
{
    impl:do-query((), $options, $query, $start, $page-length, true())
};

declare function search:estimate($ctsquery as item()) as xs:unsignedLong {
    impl:do-estimate($ctsquery, $impl:default-options)
};

declare function search:estimate($ctsquery as item(), $options as element(search:options)?) as xs:unsignedLong {
    impl:do-estimate($ctsquery, if (empty($options)) then <search:options/> else $options)
};

declare function search:check-options($options as element(search:options)) as element(search:report)* {
    search:check-options($options, false())
};

declare function search:check-options($options as element(search:options), $strict as xs:boolean?) as element(search:report)* {
    impl:do-check-options($options, if (empty($strict)) then false() else $strict)
};

declare function search:get-default-options() as element (search:options) {
    $impl:default-options
};

declare function search:snippet($result as node(),$ctsquery as item()) as element(search:snippet) {
    snip:do-snippet($result, $ctsquery, $impl:default-options/search:transform-results)
};

declare function search:snippet($result as node(),$ctsquery as item(), $options as element(search:transform-results)?) as element(search:snippet) {
    snip:do-snippet($result, $ctsquery, $options)
};

declare function search:suggest($qtext as xs:string+) as xs:string* {
    suggest:do-suggest($qtext, (), (), (), (), ())
};

declare function search:suggest($qtext as xs:string+, $options as element(search:options)?) as xs:string* {
    suggest:do-suggest($qtext, $options, (), (), (), ())
};

declare function search:suggest($qtext as xs:string+, $options as element(search:options)?, $limit as xs:unsignedInt?) as xs:string* {
    suggest:do-suggest($qtext, $options, $limit, (), (), ())
};

declare function search:suggest($qtext as xs:string+, $options as element(search:options)?, $limit as xs:unsignedInt?, $cursor-position as xs:unsignedInt?) as xs:string* {
    suggest:do-suggest($qtext, $options, $limit, $cursor-position, (), ())
};

declare function search:suggest($qtext as xs:string+, $options as element(search:options)?, $limit as xs:unsignedInt?, $cursor-position as xs:unsignedInt?, $focus as xs:positiveInteger?) as xs:string* {
    suggest:do-suggest($qtext, $options, $limit, $cursor-position, $focus, ())
};

declare function search:suggest(
    $qtext           as xs:string+,
    $options         as element(search:options)?,
    $limit           as xs:unsignedInt?,
    $cursor-position as xs:unsignedInt?,
    $focus           as xs:positiveInteger?,
    $query           as element(search:query)*
) as xs:string*
{
    suggest:do-suggest($qtext, $options, $limit, $cursor-position, $focus, $query)
};

(: two-arg version of search:remove-constraint() is gone: if you need this pass in search:default-options() as 3rd arg :)

declare function search:remove-constraint($qtext as xs:string, $rtext as xs:string, $options as element(search:options)?) as xs:string? {
  impl:do-remove-constraint($qtext,$rtext,$options)
};

(: Values:  lexicon-based functions :)

declare function search:values(
    $spec-name as xs:string,
    $options as element(search:options)
) as element(search:values-response)
{
    val:resolve($spec-name,$options,(),(),(),(),())
};

declare function search:values(
    $spec-name as xs:string,
    $options as element(search:options),
    $query as item()?
) as element(search:values-response)
{
    val:resolve($spec-name,$options,$query,(),(),(),())
};

declare function search:values(
    $spec-name as xs:string,
    $options as element(search:options),
    $query as item()?,
    $limit as xs:unsignedLong?
) as element(search:values-response)
{
    val:resolve($spec-name,$options,$query,$limit,(),(),())
};

declare function search:values(
    $spec-name as xs:string,
    $options as element(search:options),
    $query as item()?,
    $limit as xs:unsignedLong?,
    $start as xs:anyAtomicType?
) as element(search:values-response)
{
    val:resolve($spec-name,$options,$query,$limit,$start,(),())
};

declare function search:values(
    $spec-name as xs:string,
    $options as element(search:options),
    $query as item()?,
    $limit as xs:unsignedLong?,
    $start as xs:anyAtomicType?,
    $page-start as xs:unsignedLong?,
    $page-length as xs:unsignedLong?
) as element(search:values-response)
{
    val:resolve($spec-name,$options,$query,$limit,$start,$page-start,$page-length)
};
