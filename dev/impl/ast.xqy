xquery version "1.0-ml";

(: Copyright 2002-2020 MarkLogic Corporation.  All Rights Reserved. :)

module namespace ast = "http://marklogic.com/appservices/search-ast";
import module namespace impl = "http://marklogic.com/appservices/search-impl" at "search-impl.xqy";

declare default function namespace "http://www.w3.org/2005/xpath-functions";

import module namespace tdop     = "http://marklogic.com/parser/tdop" at "/MarkLogic/appservices/utils/tdop.xqy";
import module namespace hof      = "http://marklogic.com/higher-order" at "/MarkLogic/appservices/utils/higher-order.xqy";
import module namespace temporal = "http://marklogic.com/xdmp/temporal" at "/MarkLogic/temporal.xqy";

import schema namespace opt = "http://marklogic.com/appservices/search" at "search.xsd";
import module namespace debug = "http://marklogic.com/debug" at "/MarkLogic/appservices/utils/debug.xqy";

(: Search API AST implementation. Not for general use. See search.xqy for the public API listing :)

declare namespace cts    = "http://marklogic.com/cts";
declare namespace search = "http://marklogic.com/appservices/search";
declare namespace searchdev  = "http://marklogic.com/appservices/search/searchdev";
declare namespace error = "http://marklogic.com/xdmp/error";

declare option xdmp:mapping "false";

declare private variable $ast:UNKNOWN_INDEX       := 0;
declare private variable $ast:ATTRIBUTE_INDEX     := 1;
declare private variable $ast:ELEMENT_INDEX       := 2;
declare private variable $ast:FIELD_INDEX         := 3;
declare private variable $ast:JSON_PROPERTY_INDEX := 4;
declare private variable $ast:PATH_INDEX          := 5;

(: == Tokenized string to AST ==================================== :)

declare function ast:configure-grammar-map-for-tdop() {
    let $config := map:map()
    let $starters := map:map()
    let $joiners := map:map()
    let $_ :=
        (map:put($starters, "term", ast:starter-dispatch#1),
         map:put($starters,"delim", ast:starter-dispatch#1),
         map:put($starters,"starter",ast:starter-dispatch#1),
         map:put($starters,"end",ast:starter-dispatch#1))
    let $_ := map:put($joiners,"joiner",ast:joiner-dispatch#2)
    let $_ :=
        (map:put($config,"starters",$starters),
         map:put($config,"joiners",$joiners))
    return $config
};

(:~
   @param $toks <searchdev:tok>*
   @param $opts options
   @param $pos the starting position in the token stream (0 default)
   @return a parse tree of cts elements
:)
declare function ast:parse($toks as element(searchdev:tok)*, $opts as element(opt:options), $pos as xs:integer?) as element(search:query)? {
    let $ps := tdop:initialize-map($toks, ast:configure-grammar-map-for-tdop(), $pos, $opts)
    return  
        if ($toks[1]/@type eq "end")
        then ast:empty-dispatch($opts, ())
        else ast:do-parse($ps)
};

(: as a convenience, the 3-arity version of this function does an options merge.
   If you've already merged, then call do-tokenize-parse-nomerge :)
declare function ast:do-tokenize-parse($qtexts as xs:string*, $opts as element(opt:options), $incomplete as xs:boolean?) as element()? {
    let $merged-options := impl:merge-options($impl:default-options, $opts)
    return ast:do-tokenize-parse-nomerge($qtexts, $merged-options, $incomplete)
};

declare function ast:do-tokenize-parse-nomerge($qtexts as xs:string*, $merged-options as element(opt:options), $incomplete as xs:boolean?) as element()? {
    let $incomplete := if (empty($incomplete)) then false() else $incomplete
    return
        if (count($qtexts) gt 1)
        then <search:query>
                 { for $qtext in $qtexts return ast:parse(impl:tokenize($qtext, $merged-options, $incomplete), $merged-options, 0)/* }
             </search:query>
        else ast:parse(impl:tokenize($qtexts, $merged-options, $incomplete), $merged-options, 0)
};

declare function ast:do-parse($ps as map:map) as element()? {
    let $_ := tdop:advance($ps)
    let $finaltree := ast:expressions($ps, 0, "")
    let $annotations := map:get($ps, "operators")
    let $messages := map:get($ps, "msgs")
    return if (empty($finaltree)) then ast:empty-dispatch(impl:opts($ps), ($annotations, $messages))
    (: TODO: add option for <return-query-warnings> and return a 2nd node here :)
    else
        element { "search:query" } {
            $finaltree,
            $annotations,
            $messages
        }
};

(: handle an empty token stream :)
declare function ast:empty-dispatch($opts as element(opt:options), $annots as element()*) as element(search:query) {
    <search:query>
        {$annots}
    </search:query>
};

(: take an already-parsed word-query, and get the text back :)
(: why not unparse()? Because that does too much in cases like [format:"plain text"] :)
(: why not just the string-join? Because that doesn't work with shotgun ORs :)
declare function ast:textonly($elem as element()?) as xs:string {
    string-join($elem//(search:value|search:text), " ")
};

(: TODO:  EXTENSIONS :)
declare function ast:joiner-dispatch($ps as map:map, $left as element()?) as element()? {
    let $symbol := impl:symbol-lookup($ps)
    let $opts := impl:opts($ps)
    let $fname := $symbol[self::opt:joiner]/@apply
    return
        if (impl:extension($symbol))
        then 
             let $fo := xdmp:function(QName($symbol/@ns, $fname), $symbol/@at)
             let $_ := map:put($ps, "strength", xs:integer($symbol/@strength))
             (: for the moment, only parser extensions are using the "strength" key, assume it will be stale after this joiner :)
             return xdmp:apply($fo, $ps, $left, $opts)
        else 
            if ($fname eq "ignore" or empty($fname)) then ast:joiner-ignore($ps, $left)
            else if ($fname eq "infix") then ast:joiner-infix($ps, $left)
            else if ($fname eq "near2") then ast:joiner-near2($ps, $left)
            else if ($fname eq "boost") then ast:joiner-boost($ps, $left)
            else if ($fname eq "not-in") then ast:joiner-not-in($ps, $left)
            else if ($fname eq "constraint") then ast:joiner-constraint($ps, $left)
            else error((),"SEARCH-APPLYUNDEFINED", string($fname))
};

declare function ast:joiner-ignore($ps as map:map, $left as element()?) as element()? {
    tdop:advance($ps),
    $left
};

declare function ast:joiner-infix($ps as map:map, $left as element()?) as element()? {
    let $symbol := impl:symbol-lookup($ps)
    
    let $_ := tdop:advance($ps)
    
    (: flatten :)
    return 
        if (string($symbol) = ("AND", "OR"))
        then
            let $optimus-1 := ast:opt-pattern-1($ps, $symbol/@element, ())
            let $right := tdop:expression($ps, $symbol/@strength)
            return
                if ($right instance of element(unknown-starter))
                then $left
                else
                    element { xs:QName(concat("search:",substring-after($symbol/@element/string(),"cts:"))) } {
                        $left, $optimus-1, $right
                    }
        else
            let $right := tdop:expression($ps, $symbol/@strength)
            return
                if ($right instance of element(unknown-starter))
                then $left
                else
                    element { xs:QName(concat("search:",substring-after($symbol/@element/string(),"cts:"))) } {
                        $left, $right
                    }
};

declare function ast:opt-pattern-1(
    $ps as map:map,
    $name as xs:QName,
    $accum as schema-element(cts:query)*)
{
    ast:opt-pattern-1($ps, $name, $accum, fn:false())
};

declare function ast:opt-pattern-1(
    $ps as map:map,
    $name as xs:QName,
    $accum as element()*,
    $ext as xs:boolean)
{
    let $ext-advance := 
        if ($ext)
        then tdop:advance($ps)
        else ()
    let $tok1 := tdop:gt($ps)
    let $tok2 := tdop:gt($ps, 1)
    let $tok3 := tdop:gt($ps, 2)
    let $tok4 := tdop:gt($ps, 3)
    let $tok5 := tdop:gt($ps, 4)
    let $tok6 := tdop:gt($ps, 5)

    return
        if ($tok1/@type eq "term" and $tok2/search:joiner/@element eq $name)
        then 
            (tdop:advance($ps), 
            tdop:advance($ps), 
            ast:opt-pattern-1($ps, $name, 
                ($accum, ast:do-term($tok1,())),false()))
        else if 
            (($tok1/@type eq "term" and $tok2/search:joiner/@apply = "constraint"
            and $tok3/@type eq "term" and $tok4/search:joiner/@element eq $name)
            or
            ($tok1/@type eq "term" and $tok2/search:joiner/@apply = "constraint"
            and $tok3[@type eq "starter"]/search:quotation and $tok4/@type eq "term"
            and $tok5[@type eq "delim"]/search:quotation
            and $tok6/search:joiner/@element eq $name))
        then 
            (tdop:advance($ps),
            ast:opt-pattern-1(
                $ps, 
                $name, 
                ($accum, 
                ast:joiner-constraint($ps,
                    <search:term-query>
                        <search:text>{ $tok1/string() }</search:text>
                    </search:term-query>)),true()))
        else $accum
};

declare function ast:joiner-near2($ps as map:map, $left as element()?) as element()? {
    let $symbol := impl:symbol-lookup($ps)
    let $_ := tdop:advance($ps)
    let $expr1 := impl:simple-expression($ps)
    let $expr2 := tdop:expression($ps, $symbol/@strength)
    let $dist := if ($expr1 castable as xs:double)
                 then $expr1
                 else (10, impl:msg($ps, <search:annotation warning="SEARCH-IGNOREDQTEXT:[{$expr1}] is not a valid distance, using 10"/>))
    return
        if (empty($expr2))
        then ($left, impl:msg($ps, <search:annotation warning="SEARCH-IGNOREDQTEXT:[{string($symbol)}{$expr1}{string($expr2)}]: expected two arguments"/>))
        else
            element { xs:QName(concat("search:",substring-after($symbol/@element/string(),"cts:"))) } {
                element search:distance { $dist },
                $left, $expr2
            }
};

declare function ast:joiner-boost($ps as map:map, $left as element()?) as element()? {
    let $symbol := impl:symbol-lookup($ps)
    let $_ := tdop:advance($ps)
    let $expr1 := tdop:expression($ps, $symbol/@strength)
    return
        if (empty($left))
        then 
            ($left, impl:msg($ps, <search:annotation warning="SEARCH-IGNOREDQTEXT:[{string($symbol)} {string($expr1)}]: expected two arguments"/>))
        else
            element { xs:QName(concat("search:",substring-after($symbol/@element/string(),"cts:"))) } {
                element search:matching-query { $left },
                element search:boosting-query { $expr1 }
            }
};

declare function ast:joiner-not-in($ps as map:map, $left as element()?) as element()? {
    let $symbol := impl:symbol-lookup($ps)
    let $_ := tdop:advance($ps)
    let $expr1 := tdop:expression($ps, $symbol/@strength)
    return
        if (empty($left))
        then 
            ($left, impl:msg($ps, <search:annotation warning="SEARCH-IGNOREDQTEXT:[{string($symbol)} {string($expr1)}]: expected two arguments"/>))
        else
            element { xs:QName(concat("search:",substring-after($symbol/@element/string(),"cts:"))) } {
                element search:positive-query { $left },
                element search:negative-query { $expr1 }
            }
};

declare function ast:joiner-constraint($ps as map:map, $left as element()?) as element()? {
    let $left-str := ast:textonly($left)
    let $opts := impl:opts($ps)
    let $matched-constraint := $opts/(opt:constraint|opt:operator)[@name eq string($left-str)][1]
    let $is-bucketed := $matched-constraint/opt:range[opt:bucket|opt:computed-bucket]
    let $symbol := impl:symbol-lookup($ps)
    let $compare := $symbol/@compare/string()
    let $_ := tdop:advance($ps)
    let $right := 
        if ($is-bucketed)
        then () (: if the next token is a bucketname, no sense in doing a full tdop:expression on it :)
        else tdop:expression($ps, $symbol/@strength - 1)
    let $bucket-name :=
        if ($is-bucketed)
        then impl:simple-expression($ps)
        else ""
    let $qtext-rhs :=
        if (empty($right))
        then $bucket-name
        else ast:textonly($right)
    let $qtext :=
        if ($symbol/@tokenize eq "word")
        then concat($left-str, " ", string($symbol), " ")
        else concat($left-str, string($symbol))    
    return
        (: TODO:  This is ugly...refactor. :)
        if ($is-bucketed and empty($matched-constraint/opt:range/(opt:bucket|opt:computed-bucket)[@name eq $qtext-rhs]))
        then <search:annotation warning="SEARCH-IGNOREDQTEXT:[{concat($matched-constraint/@name/string(),":",$qtext-rhs)}]"/>
        else if ($compare and (empty($matched-constraint/opt:range) or $is-bucketed)) 
        then <search:annotation warning="SEARCH-IGNOREDQTEXT:[{concat($matched-constraint/@name/string()," ",$compare," ",$qtext-rhs)}]"/>
        else 
        typeswitch($matched-constraint)
            case empty-sequence() 
                return 
                    if ($right/search:text/contains(.," "))
                    then  
                        (impl:msg($ps,<search:annotation warning="SEARCH-IGNOREDQTEXT:dropping unrecognized constraint prefix [{$qtext}]"/>),
                        ast:do-term($qtext-rhs,()))
                    else ast:do-term(concat($qtext, $qtext-rhs),())
            case element(opt:operator)
                return ast:joiner-operator($ps,$right,$matched-constraint,$qtext)
            case element(opt:constraint) 
                return ast:resolve-constraint($matched-constraint/@name/string(),$matched-constraint/(search:* except search:annotation),$qtext-rhs,$compare,$right)
            default return <search:annotation warning="SEARCH-IGNOREDQTEXT:[concat($qtext, $qtext-rhs)]"/>
};


declare function ast:resolve-constraint(
    $name as xs:string,
    $constraint-elem as element(),
    $qtext as xs:string,
    $compare as xs:string?,
    $subquery as element()?
) as element()
{
        typeswitch($constraint-elem)
            case element(opt:range) return
                 <search:range-constraint-query>
                    <search:constraint-name>{$name}</search:constraint-name>
                    <search:value>{$qtext}</search:value>
                    { if (empty($compare)) then () else <search:range-operator>{$compare}</search:range-operator>}
                 </search:range-constraint-query>
            case element(opt:collection) return
                <search:collection-constraint-query>
                    <search:constraint-name>{$name}</search:constraint-name>
                    <search:uri>{$qtext}</search:uri>
                 </search:collection-constraint-query>
            case element(opt:word) return
                <search:word-constraint-query>
                    <search:constraint-name>{$name}</search:constraint-name>
                    <search:text>{$qtext}</search:text>
                 </search:word-constraint-query>
            case element(opt:value) return
                 <search:value-constraint-query>
                    <search:constraint-name>{$name}</search:constraint-name>
                    <search:text>{$qtext}</search:text>
                 </search:value-constraint-query>
            case element(opt:element-query) return
                <search:element-constraint-query>
                    <search:constraint-name>{$name}</search:constraint-name>
                    {$subquery}
                 </search:element-constraint-query>
            case element(opt:container) return
                <search:container-constraint-query>
                    <search:constraint-name>{$name}</search:constraint-name>
                    {$subquery}
                 </search:container-constraint-query>
            case element(opt:geo-elem-pair) return 
                ast:geospatial-constraint-query($name,$qtext)
            case element(opt:geo-elem) return
                ast:geospatial-constraint-query($name,$qtext)
            case element(opt:geo-attr-pair) return
                ast:geospatial-constraint-query($name,$qtext)
            case element(opt:geo-json-property-pair) return 
                ast:geospatial-constraint-query($name,$qtext)
            case element(opt:geo-json-property) return 
                ast:geospatial-constraint-query($name,$qtext)
            case element(opt:geo-path) return
                ast:geospatial-constraint-query($name,$qtext)
            case element(opt:geo-region-path) return
                ast:geospatial-region-constraint-query($name,$qtext)
            case element(opt:properties) return
                <search:properties-constraint-query>
                    <search:constraint-name>{$name}</search:constraint-name>
                    {$subquery}
                 </search:properties-constraint-query>
            case element(opt:custom) return
                <search:custom-constraint-query>
                    <search:constraint-name>{$name}</search:constraint-name>
                    <search:text>{$qtext}</search:text>
                 </search:custom-constraint-query>
            default return <search:annotation warning="SEARCH-IGNOREDQTEXT:[{$name}] is not a valid constraint or operator type"/>
};


declare function ast:joiner-operator($ps as map:map, $right as element(), $matched-constraint as element(), $qtext as xs:string) as element()? {
    let $operator-ref := $matched-constraint/@name
    let $state-ref := ast:textonly($right)
    return
        if ($matched-constraint/opt:state[@name eq $state-ref]) 
        then 
            impl:oper($ps,
                <search:operator-state>
                    <search:operator-name>{string($operator-ref)}</search:operator-name>
                    <search:state-name>{$state-ref}</search:state-name>
                </search:operator-state>)
        else impl:msg($ps,<search:annotation warning="SEARCH-IGNOREDQTEXT:[{concat($qtext,$state-ref)}]"/>)
};

declare function ast:starter-dispatch($ps as map:map) as element()? {

    let $symbol := impl:symbol-lookup($ps)
    let $opts := impl:opts($ps)
    let $has-starter := exists($symbol[self::opt:starter]/@apply)
    let $telem := $opts/opt:term[1]
    let $felem := 
        if ($has-starter)
        then $symbol
        else if (exists($telem))
        then $telem
        else <opt:term/>
    let $fname := $felem/@apply
    return
        if ($symbol/self::opt:quotation)
        then
            if (impl:extension($felem))
            then
                (: this partially duplicates code in starter-quotation :)
                let $fn := xdmp:function(QName($felem/@ns, $fname), $felem/@at)
                let $_ := tdop:advance($ps) (: to opening delim :)
                let $_ := map:put($ps, "strength", xs:integer($felem/@strength))
                let $parsed-phrase :=
                    if (function-arity($fn) != 2)
                    then error((),"SEARCH-BADEXTENSION")
                    else if ($has-starter)
                    then xdmp:apply($fn, $ps, $opts)
                    else
                        (: apply term parser during handle :)
                        <search:term-query>
                            <search:text>{
                                tdop:gt($ps)/string(),
                                tdop:advance($ps) (: past the phrase itself :)
                            }</search:text>
                        </search:term-query>
                let $_skip-over-closing-delim := tdop:advance($ps)
                return $parsed-phrase
            else ast:starter-quotation($ps, $symbol, $telem)
        else if ($fname eq "grouping") then ast:starter-grouping($ps)
        else if ($fname eq "prefix") then ast:starter-prefix($ps)
        else if (tdop:gt($ps)/@type eq "end") then () (: this prevents an empty term at the end :)
        else ast:term-term($ps, $telem) 
};

declare function ast:starter-quotation($ps as map:map, $symbol as element(opt:quotation), $term-elem as element(opt:term)?) as element()? {
    let $_ := tdop:advance($ps) (: past starting delim :)
    let $quoted-phrase := tdop:gt($ps)
    let $_ := tdop:advance($ps) (: past the phrase itself :)
    let $_ := tdop:advance($ps) (: past the closing delim :)
    return
        <search:term-query>
            <search:text>{ $quoted-phrase/string() }</search:text>
        </search:term-query>
};

declare function ast:starter-grouping($ps as map:map) as element()? {
    let $tok := tdop:gt($ps)
    let $_ := tdop:advance($ps) (: past opening delim :)
    let $phrase := ast:expressions($ps, 0, if ($tok/opt:starter/@delimiter) then $tok/opt:starter/@delimiter else ")")
    let $_ := tdop:advance($ps) (: past closing delim :) 
    return $phrase
};

declare function ast:starter-prefix($ps as map:map) as element()? {
    let $sym := impl:symbol-lookup($ps)
    let $_ := tdop:advance($ps)
    let $right := tdop:expression($ps, $sym/@strength)
    return
        element { xs:QName(concat("search:",substring-after($sym/@element/string(),"cts:"))) } {
            $right
        }
};

declare function ast:expressions($ps as map:map, $rbp as xs:integer, $until as xs:string) as element()? { 
    let $subexpr := tdop:expression($ps, $rbp)
    return ast:inner-expressions($ps, $subexpr, $rbp, $until)
 };
 
declare function ast:inner-expressions($ps as map:map, $left as element()?, $rbp as xs:integer, $until as xs:string) as element()? {
    if (tdop:at-end($ps) (: always stop at the end:) or
        ($until ne "" and tdop:gt($ps) eq $until) (: we hit a stopping token :) )
    then 
        if ($left instance of element(unknown-starter))
        then ast:do-term($left//text(), ())
        else $left
    else
       let $subexpr := ast:expressions($ps, $rbp, $until)
       let $implicit := 
           if (empty($subexpr))
           then <cts:and-query/>
           else (impl:opts($ps)/opt:grammar/opt:implicit/*, <cts:and-query/>)[1]
       return element { concat("search:",substring-after($implicit/name(),"cts:")) } 
       { 
           if ($left instance of element(unknown-starter))
           then ast:do-term($left//text(), ())
           else $left,
           $subexpr
       }
};

declare function ast:term-term($ps as map:map, $term-elem as element()?) as element() {
    (
    ast:do-term(string(tdop:gt($ps)),$term-elem),
    tdop:advance($ps)
    )
 };
 
 declare function ast:do-term($s as xs:string, $term-elem as element()?) as element() {
    <search:term-query>{
       <search:text>{$s}</search:text>,
       $term-elem/search:weight,
       $term-elem/search:term-option
    }</search:term-query>
 };

(: helpers for creating structured query from qtext :)
declare function ast:geospatial-constraint-query(
    $name as xs:string,
    $qtext as xs:string
) as element()
{
    let $region := 
        switch(substring($qtext,1,1))
            case "@" return ast:construct-circle($qtext)
            case "[" return ast:construct-box($qtext)
            default return ast:construct-point($qtext)
    return 
        if ($region instance of element(search:annotation))
        then $region 
        else  
            <search:geospatial-constraint-query>
                <search:constraint-name>{$name}</search:constraint-name>
                {$region}
            </search:geospatial-constraint-query> 
};

(: helpers for creating structured query from qtext :)
declare function ast:geospatial-region-constraint-query(
    $name as xs:string,
    $qtext as xs:string
) as element()
{
    let $region := 
        switch(substring($qtext,1,1))
            case "@" return ast:construct-circle($qtext)
            case "[" return ast:construct-box($qtext)
            default return ast:construct-point($qtext)
    return 
        if ($region instance of element(search:annotation))
        then $region 
        else  
            <search:geo-region-constraint-query>
                <search:constraint-name>{$name}</search:constraint-name>
                <geospatial-operator>contains</geospatial-operator>
                {$region}
            </search:geo-region-constraint-query> 
};

declare function ast:construct-circle(
    $qtext as xs:string)
as element()
{
    let $toks := tokenize($qtext,"\s+")
    return 
        if (count($toks) eq 2) 
        then 
            let $radius := substring($toks[1],2)
            let $point := ast:construct-point($toks[2])
            return 
                if ($radius castable as xs:float and $point instance of element(search:point))
                then 
                    <search:circle>
                    <search:radius>{$radius}</search:radius>
                    {$point} 
                    </search:circle>
                else <search:annotation warning="SEARCH-IGNOREDQTEXT:[Invalid text, cannot parse circle from '{$qtext}'.]"/>
        else <search:annotation warning="SEARCH-IGNOREDQTEXT:[Invalid text, cannot parse circle from '{$qtext}'.]"/>
};

declare function ast:construct-box(
    $qtext as xs:string)
as element()
{
    let $toks := tokenize(replace($qtext,"(\[|\]|\s+)",""),",")
    let $floats := 
        for $i in $toks 
        return 
            if ($i castable as xs:float) then 1 else 0
    return
        if (count($toks) eq 4 and sum($floats) eq 4)
        then 
            <search:box>
                <search:south>{$toks[1]}</search:south>
                <search:west>{$toks[2]}</search:west>
                <search:north>{$toks[3]}</search:north>
                <search:east>{$toks[4]}</search:east>
            </search:box>
        else <search:annotation warning="SEARCH-IGNOREDQTEXT:[Invalid text, cannot parse box from '{$qtext}'.]"/>
};

declare function ast:construct-point(
    $qtext as xs:string)
as element()+
{
    let $toks := tokenize(replace($qtext,"\s+",""),",")
    return
        (if (count($toks) ge 2 and 
            $toks[1] castable as xs:float and $toks[2] castable as xs:float)
        then 
            <search:point>
                <search:latitude>{$toks[1]}</search:latitude>
                <search:longitude>{$toks[2]}</search:longitude>
            </search:point> 
        else <search:annotation warning="SEARCH-IGNOREDQTEXT:[Invalid text, cannot parse geospatial point from '{$qtext}'.]"/>,
        if (count($toks) gt 2)
        then <search:annotation warning="SEARCH-IGNOREDQTEXT:[Invalid text, truncated to point from '{$qtext}'.]"/>
        else ())
}; 

(: ======= AST to CTS Query================== :)

(: tokenize, parse and serialize in one step, assuming the need to merge :)
declare function ast:do-parse-serialize(
    $qtext   as xs:string+,
    $options as element(search:options)
) as schema-element(cts:query)?
{
    ast:serialize-query(
        ast:do-parse-map($qtext, $options)
        )
};

declare function ast:do-parse-constructed(
    $qtext   as xs:string+,
    $options as element(search:options)
) as cts:query?
{
    map:get(ast:do-parse-map($qtext, $options), "query")
};

declare function ast:do-parse-map(
    $qtext   as xs:string+,
    $options as element(search:options)
) as map:map
{
    ast:to-query(
        ast:do-tokenize-parse($qtext, $options, false()),
        $options,
        true())
};
    
(: Accumulate the query and any resulting warnings and annotations in a map.  
 : Search implmentation will call this and use accessors to get the parsed query
 : and warnings. :)

(: normal code path assumes merge has been done
 : this overloaded version of to-query does a merge
 : for use in debugging :)
declare function ast:to-query(
    $query as element(search:query),
    $options as element(opt:options),
    $merge as xs:boolean
) as map:map
{
    let $opts :=
        if ($merge eq true())
        then impl:merge-options($impl:default-options, $options)
        else $options
    return ast:to-query($query,$opts)        
};  
 
declare function ast:to-query(
    $query as element(search:query),
    $options as element(opt:options)
) as map:map
{
    let $query-map := map:map()
    (: externally, search:qtext is a child of search:search but, internally, of search:query :)
    let $qtext := map:put($query-map,"qtext",$query//search:qtext)
    let $path-namespaces := map:put($query-map, "path-namespaces", $query//search:path-index/namespace::*)
    let $cts-query := ast:dispatch($query,$options,$query-map)
    let $_ := map:put($query-map,"options",$options)
    let $_ := map:put($query-map,"query",$cts-query)
    return $query-map
};  
 
declare function ast:dispatch(
    $query-elem as element(), 
    $options as element(opt:options),
    $state as map:map
) as cts:query*
{
    typeswitch($query-elem)
        case element(search:query)
        return ast:handle-query($query-elem,$options,$state)
        case element(search:term-query)
        return ast:handle-term($query-elem,$options,$state)
        case element(search:and-query)
        return ast:handle-and($query-elem,$options,$state)
        case element(search:or-query)
        return ast:handle-or($query-elem,$options,$state)
        case element(search:and-not-query)
        return ast:handle-and-not($query-elem,$options,$state)
        case element(search:boost-query)
        return ast:handle-boost($query-elem,$options,$state)
        case element(search:not-in-query)
        return ast:handle-not-in($query-elem,$options,$state)
        case element(search:not-query)
        return ast:handle-not($query-elem,$options,$state)
        case element(search:near-query)
        return ast:handle-near($query-elem,$options,$state)
        case element(search:document-fragment-query)
        return ast:handle-document-fragment($query-elem,$options,$state)
        case element(search:properties-fragment-query)
        return ast:handle-properties($query-elem,$options,$state)
        case element(search:locks-fragment-query)
        return ast:handle-locks($query-elem,$options,$state)
        case element(search:directory-query)
        return ast:handle-directory($query-elem,$state)
        case element(search:collection-query)
        return ast:handle-collection($query-elem,$state)
        case element(search:document-query)
        return ast:handle-document($query-elem,$state)
        case element(search:element-constraint-query)
        return ast:handle-element-constraint($query-elem,$options,$state)
        case element(search:container-constraint-query)
        return ast:handle-container-constraint($query-elem,$options,$state)
        case element(search:container-query)
        return ast:handle-container-query($query-elem,$options,$state)
        case element(search:properties-constraint-query)
        return ast:handle-properties-constraint($query-elem,$options,$state)
        case element(search:collection-constraint-query)
        return ast:handle-collection-constraint($query-elem,$options,$state)
        case element(search:value-constraint-query)
        return ast:handle-value-constraint($query-elem,$options,$state)
        case element(search:value-query)
        return ast:handle-value-query($query-elem,$options,$state)
        case element(search:word-constraint-query)
        return ast:handle-word-constraint($query-elem,$options,$state)
        case element(search:word-query)
        return ast:handle-word-query($query-elem,$options,$state)
        case element(search:true-query)
        return ast:handle-true-query($query-elem,$options,$state)
        case element(search:false-query)
        return ast:handle-false-query($query-elem,$options,$state)
        case element(search:operator-state)
        return ast:handle-operator-state($query-elem,$options,$state) 
        case element(search:annotation)
        return ast:add-annotation($query-elem,$state)
        case element(search:period-compare-query)
        return ast:handle-period-compare-query($query-elem,$options,$state)
        case element(search:period-range-query)
        return ast:handle-period-range-query($query-elem,$options,$state)
        case element(search:lsqt-query)
        return ast:handle-lsqt-query($query-elem,$options,$state)
        case element(search:qtext)
        return ast:handle-qtext($query-elem,$options,$state)
        case element(search:range-constraint-query)
        return ast:handle-range-constraint($query-elem,$options,$state)
        case element(search:range-query)
        return ast:handle-range-query($query-elem,$options,$state)
        case element(search:geospatial-constraint-query)
        return ast:handle-geospatial-constraint($query-elem,$options,$state)
        case element(search:geo-region-constraint-query)
        return ast:handle-geo-region-constraint($query-elem,$options,$state)
        case element(search:geo-elem-query)
        return ast:handle-geo-elem-query($query-elem,$options,$state)
        case element(search:geo-elem-pair-query)
        return ast:handle-geo-elem-pair-query($query-elem,$options,$state)
        case element(search:geo-attr-pair-query)
        return ast:handle-geo-attr-pair-query($query-elem,$options,$state)
        case element(search:geo-json-property-query)
        return ast:handle-geo-json-property-query($query-elem,$options,$state)
        case element(search:geo-json-property-pair-query)
        return ast:handle-geo-json-property-pair-query($query-elem,$options,$state)
        case element(search:geo-path-query)
        return ast:handle-geo-path-query($query-elem,$options,$state)
        case element(search:geo-region-path-query)
        return ast:handle-geo-region-path-query($query-elem,$options,$state)
        case element(search:before-query)
        return ast:handle-before-query($query-elem,$options,$state)
        case element(search:after-query)
        return ast:handle-after-query($query-elem,$options,$state)
        (: TODO:  provisional implementation, see open issue in spec :)
        case element(search:custom-constraint-query)
        return (ast:handle-custom-constraint($query-elem,$options,$state))
        default return () 
  (: Dump anything that's not a valid subquery :)
}; 

(: ==== Options-aware AST handlers ===== :)

declare function ast:handle-query(
    $query-elem as element(search:query),
    $options as element(opt:options),
    $state as map:map
) as cts:query
{    
    let $query := 
        for $q in $query-elem/*
        return ast:dispatch($q,$options,$state)
    return 
        if (empty($query))
        then ast:handle-empty($options,$state)
        else if (count($query) gt 1)
        then cts:and-query($query,()) (: implicit and if there are multiples :)
        else  $query
};    

(: ==== Composers ==== :)

declare function ast:handle-and(
    $query-elem as element(search:and-query),
    $options as element(opt:options),
    $state as map:map
) as cts:and-query?
{
    cts:and-query(
        for $q in $query-elem/*
        return ast:dispatch($q,$options,$state),
        ast:ordered($query-elem)
        )
};

declare function ast:handle-or(
    $query-elem as element(search:or-query),
    $options as element(opt:options),
    $state as map:map
) as cts:or-query?
{
    cts:or-query(
        for $q in $query-elem/*
        return ast:dispatch($q,$options,$state))
};

declare function ast:handle-and-not(
    $query-elem as element(search:and-not-query),
    $options as element(opt:options),
    $state as map:map
) as cts:and-not-query?
{
    if (empty($query-elem/search:positive-query/*)  or empty($query-elem/search:negative-query/*) 
        or count($query-elem/(search:positive-query|search:negative-query/*)) gt 2)
    then ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY: ignoring invalid and-not-query (requires positive and negative queries)"/>,$state) 
    else
        cts:and-not-query(
            ast:dispatch($query-elem/search:positive-query/*,$options,$state),
            ast:dispatch($query-elem/search:negative-query/*,$options,$state))
};

declare function ast:handle-boost(
    $query-elem as element(search:boost-query),
    $options as element(opt:options),
    $state as map:map
) as cts:boost-query?
{
    if (empty($query-elem/search:matching-query/*)  or empty($query-elem/search:boosting-query/*)
        or count($query-elem/(search:matching-query|search:boosting-query/*)) gt 2)
    then ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY: ignoring invalid boost-query (requires matching and boosting queries)"/>,$state)
    else
        cts:boost-query(
            ast:dispatch($query-elem/search:matching-query/*,$options,$state),
            ast:dispatch($query-elem/search:boosting-query/*,$options,$state))
};

declare function ast:handle-not-in(
    $query-elem as element(search:not-in-query),
    $options as element(opt:options),
    $state as map:map
) as cts:not-in-query?
{
    if (empty($query-elem/search:positive-query/*)  or empty($query-elem/search:negative-query/*)
        or count($query-elem/(search:positive-query|search:negative-query/*)) gt 2)
    then ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY: ignoring invalid not-in-query (requires positive and negative queries)"/>,$state)
    else
        cts:not-in-query(
            ast:dispatch($query-elem/search:positive-query/*,$options,$state),
            ast:dispatch($query-elem/search:negative-query/*,$options,$state))
};

declare function ast:handle-not(
    $query-elem as element(search:not-query),
    $options as element(opt:options),
    $state as map:map
) as cts:not-query?
{
    if (count(ast:subqueries($query-elem)) ne 1)
    then ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY: ignoring invalid not-query (requires one subquery)"/>,$state) 
    else
        cts:not-query(
            ast:dispatch(ast:subqueries($query-elem),$options,$state))
};

declare function ast:handle-near(
    $query-elem as element(search:near-query),
    $options as element(opt:options),
    $state as map:map
) as cts:near-query?
{
    let $minimum-distance := $query-elem/search:minimum-distance[1]
    return
    cts:near-query(
        for $q in $query-elem/search:*
        return ast:dispatch($q,$options,$state),
        $query-elem/search:distance/data(),
        if (empty($minimum-distance)) then ast:ordered($query-elem) else ("minimum-distance=" || $minimum-distance, ast:ordered($query-elem)),
        $query-elem/search:distance-weight/data())
};

(: ==== Scope ==== :)

declare function ast:handle-collection(
    $query-elem as element(search:collection-query),
    $state as map:map
) as cts:collection-query?
{
    cts:collection-query($query-elem/search:uri/string())
};

declare function ast:handle-directory(
    $query-elem as element(search:directory-query),
    $state as map:map
) as cts:directory-query?
{
    cts:directory-query(
        $query-elem/search:uri/string(),
    if ($query-elem/search:infinite eq true()) then "infinity" else "1")
};

declare function ast:handle-document(
    $query-elem as element(search:document-query),
    $state as map:map
) as cts:document-query?
{
    cts:document-query($query-elem/search:uri/string())
};

declare function ast:handle-properties(
    $query-elem as element(search:properties-fragment-query),
    $options as element(opt:options),
    $state as map:map
) as cts:properties-fragment-query?
{
    if (count(ast:subqueries($query-elem)) ne 1)
    then ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY: ignoring invalid properties-fragment-query (requires one subquery)"/>,$state) 
    else 
        cts:properties-fragment-query(
            ast:dispatch(ast:subqueries($query-elem),$options,$state))
};

declare function ast:handle-locks(
    $query-elem as element(search:locks-fragment-query),
    $options as element(opt:options),
    $state as map:map
) as cts:locks-fragment-query?
{
    if (count(ast:subqueries($query-elem)) ne 1)
    then ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY: ignoring invalid locks-fragment-query (requires one subquery)"/>,$state) 
    else
        cts:locks-fragment-query(
            ast:dispatch(ast:subqueries($query-elem),$options,$state))
};

declare function ast:handle-document-fragment(
    $query-elem as element(search:document-fragment-query),
    $options as element(opt:options),
    $state as map:map
) as cts:document-fragment-query?
{
    if (count(ast:subqueries($query-elem)) ne 1)
    then ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY: ignoring invalid document-fragment-query (requires one subquery)"/>,$state) 
    else
        cts:document-fragment-query(
            ast:dispatch(ast:subqueries($query-elem),$options,$state))
};

(: ==== search API constructs ====:)

declare function ast:handle-empty(
    $options as element(opt:options),
    $state as map:map
)
{
    let $apply := $options/opt:term/opt:empty/@apply
    return
        if ($apply = "no-results") 
        then cts:word-query((),(),())
        else cts:and-query((),())
};

declare function ast:handle-term(
    $query-elem as element(search:term-query), 
    $options as element(opt:options),
    $state as map:map
) as cts:query?
{
    if (exists($options/opt:term/@apply[string(.) ne "term"])) then
        if (impl:extension($options/opt:term)) then (
            if (empty($options/opt:term/opt:default)) then ()
            else ast:add-annotation(<search:annotation warning="SEARCH-INVALIDDEFAULT: Conflicting default term configuration, using @apply"/>,$state),

            let $fn := xdmp:function(QName($options/opt:term/@ns, $options/opt:term/@apply), $options/opt:term/@at)
            let $ctsquery :=
                if (function-arity($fn) != 2)
                then error((),"SEARCH-BADEXTENSION")
                else if (xdmp:function-parameter-type($fn, 1) ne "map:map")
                then xdmp:apply($fn,$query-elem,$options)
                else
                    let $tokens := (
                        $query-elem/search:text /
                            <searchdev:tok type="term">{string(.)}</searchdev:tok>,
                        <searchdev:tok type="end"/>
                        )
                    let $ps := map:map()
                    return
                        if (count($tokens) lt 2)
                        then error((),"SEARCH-BADEXTENSION")
                        else (
                            map:put($ps,"toknum",1),
                            map:put($ps,"toks",$tokens),

                            xdmp:apply($fn,$ps,$options/opt:term)
                            )
            return 
                typeswitch($ctsquery)
                    case cts:query
                        return $ctsquery
                    case schema-element(cts:query)
                        return impl:cts-query($ctsquery)
                    default return ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY: term does not parse to a cts query"/>,$state)
            )
        else error((),"SEARCH-BADEXTENSION", $options/opt:term/@apply)
    else
        let $global-scope   := $options/opt:fragment-scope
        let $term-scope     := $options/opt:term/opt:fragment-scope
        let $constraint-ref := $options/opt:term/opt:default/@ref/string(.)
        let $default        := 
            let $constraint :=
                if (empty($constraint-ref)) then ()
                else
                    let $constraint-def := $options/opt:constraint[@name eq $constraint-ref]
                    return
                        if (exists($constraint-def))
                        then $constraint-def
                        else ast:add-annotation(<search:annotation warning="SEARCH-INVALIDREF:[{$options/opt:term/opt:default/@ref/string()}] is not a valid constraint name, defaulting to word query"/>,$state) 
            let $allowed-child := $options/opt:term/opt:default/(opt:word|opt:value|opt:range)
            let $conflict :=
                if (empty($constraint) or empty($allowed-child)) then ()
                else ast:add-annotation(<search:annotation warning="SEARCH-INVALIDDEFAULT: Conflicting default term configuration, using @ref"/>,$state)
            return ($constraint/*,$allowed-child)[1]

        return 
            typeswitch($default)
            case element(opt:value)
                return ast:value-query($query-elem, $default, $term-scope, $global-scope, $state)
            case element(opt:word)
                return ast:word-query($query-elem, $default, $term-scope, $global-scope, true(), $state)
            case element(opt:range)
                return ast:range-query($query-elem, $default, $term-scope, $global-scope, $state)
            case element(opt:custom)
                return ast:custom-query(
                    <search:custom-constraint-query>
                        <search:constraint-name>{$constraint-ref}</search:constraint-name>
                        {$query-elem/search:text}
                    </search:custom-constraint-query>,
                    $default,
                    $state,
                    $options
                    )
            default 
                return 
                    ast:wrap-scope($term-scope,$global-scope,
                        cts:word-query(
                            $query-elem/search:text/string(),
                            let $query-options := $query-elem/search:term-option/string()
                            return
                                if (exists($query-options))
                                then $query-options
                                else $options/opt:term/opt:term-option/string(),
                            head(($query-elem/search:weight/data(),$options/opt:term/opt:weight/data()))
                        )
                    )
};

declare function ast:handle-collection-constraint(
    $query-elem as element(search:collection-constraint-query),
    $options as element(opt:options),
    $state as map:map
) as cts:collection-query?
{
    let $constraint := ast:matched-constraint($query-elem,$options,$state)
    return 
        if (empty($constraint) or empty($constraint/opt:collection))
        then ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY:[{$query-elem/search:constraint-name}] is not a valid constraint name"/>,$state) 
        else 
            let $prefix := fn:data($constraint/opt:collection/@prefix)
            let $uris := 
                for $uri in $query-elem/search:uri/string()
                return concat(string($prefix), $uri)
            return cts:collection-query($uris)
};

declare function ast:handle-element-constraint(
    $query-elem as element(search:element-constraint-query),
    $options as element(opt:options),
    $state as map:map
) as cts:query?
{
    let $constraint := ast:matched-constraint($query-elem,$options,$state)
    let $container  := $constraint/opt:element-query
    return
        if (empty($constraint) or empty($container))
        then ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY:[{$query-elem/search:constraint-name}] is not a valid constraint name"/>,$state)
        else 
            ast:element-query($query-elem, $container, $container, $options, $state)
};

declare function ast:handle-container-constraint(
    $query-elem as element(search:container-constraint-query),
    $options    as element(opt:options),
    $state      as map:map
) as cts:query?
{
    let $constraint := ast:matched-constraint($query-elem,$options,$state)
    let $container  := $constraint/opt:container
    let $container-elem := $container/(opt:element|opt:json-property)
    return
        typeswitch($container-elem)
        case element(search:element) return 
            ast:element-query($query-elem, $container-elem, $container, $options, $state)
        case element(search:json-property) return 
            ast:json-property-query($query-elem, $container-elem, $container, $options, $state)
        default return
            ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY:[{$query-elem/search:constraint-name}] is not a valid constraint name"/>,$state)
};

declare function ast:handle-container-query(
    $query-elem as element(search:container-query),
    $options    as element(opt:options),
    $state      as map:map
) as cts:query?
{
    let $container-elem := $query-elem/(search:element|search:json-property)
    return
        typeswitch($container-elem)
        case element(search:element) return 
            ast:element-query($query-elem, $container-elem, $query-elem, $options, $state)
        case element(search:json-property) return 
            ast:json-property-query($query-elem, $container-elem, $query-elem, $options, $state)
        default return
            ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY: element or json-property not specified"/>,$state)
};

declare function ast:element-query(
    $query-elem     as element(),
    $container-elem as element(),
    $scope-elem     as element(),
    $options        as element(opt:options),
    $state          as map:map
) as cts:query?
{
    let $subqueries := ast:subqueries($query-elem)
    return
        if (count($subqueries) ne 1)
        then ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY: ignoring invalid query (requires one subquery)"/>,$state)
        else 
            ast:wrap-scope(
                $scope-elem/opt:fragment-scope,
                $options/opt:fragment-scope, 
                cts:element-query(
                    impl:spec-qname-list($container-elem),
                    ast:dispatch($subqueries,$options,$state)))
};

declare function ast:json-property-query(
    $query-elem     as element(),
    $container-elem as element(),
    $scope-elem     as element(),
    $options        as element(opt:options),
    $state          as map:map
) as cts:query?
{
    let $subqueries := ast:subqueries($query-elem)
    return
        if (count($subqueries) ne 1)
        then ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY: ignoring invalid query (requires one subquery)"/>,$state)
        else
           (: TODO:  handle restriction on fragment scope here? :)
            ast:wrap-scope(
                $scope-elem/opt:fragment-scope,
                $options/opt:fragment-scope,
                cts:json-property-scope-query(
                    $container-elem/string(),
                    ast:dispatch($subqueries,$options,$state)))
};

declare function ast:handle-properties-constraint(
    $query-elem as element(search:properties-constraint-query),
    $options as element(opt:options),
    $state as map:map
) as cts:properties-fragment-query?
{
    let $constraint := ast:matched-constraint($query-elem,$options,$state)
    return 
        if (empty($constraint) or empty($constraint/opt:properties))
        then ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY:[{$query-elem/search:constraint-name}] is not a valid constraint name"/>,$state)
        else 
            if (count(ast:subqueries($query-elem)) ne 1)
            then ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY: ignoring invalid properties-constraint-query (requires one subquery)"/>,$state)
            else 
                cts:properties-fragment-query(
                    ast:dispatch(ast:subqueries($query-elem),$options,$state))
};

declare function ast:handle-value-constraint(
    $query-elem as element(search:value-constraint-query),
    $options as element(opt:options),
    $state as map:map
) as cts:query?
{
    let $constraint := ast:matched-constraint($query-elem,$options,$state)
    return 
        if (empty($constraint) or empty($constraint/opt:value))
        then ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY:[{$query-elem/search:constraint-name}] is not a valid constraint name"/>,$state)
        else ast:value-query($query-elem, $constraint/opt:value, $constraint//opt:fragment-scope, $options/opt:fragment-scope, $state)
};

declare function ast:handle-value-query(
    $query-elem as element(search:value-query),
    $options    as element(opt:options),
    $state      as map:map
) as cts:query?
{
    ast:value-query($query-elem, $query-elem,
        $query-elem/opt:fragment-scope, $options/opt:fragment-scope, $state
        )
};

declare function ast:value-query(
    $query-elem       as element(),
    $value-elem       as element(),
    $constraint-scope as element(opt:fragment-scope)?,
    $global-scope     as element(opt:fragment-scope)?,
    $state            as map:map
) as cts:query?
{
    let $term-opts :=
        let $query-term-opts :=
            typeswitch($query-elem)
            case element(search:term-query) return $query-elem/search:term-option
            default                         return ()
        return
            if (exists($query-term-opts))
            then $query-term-opts
            else $value-elem/opt:term-option
    let $term-weight :=
        let $query-term-weight := $query-elem/search:weight
        return
            if (exists($query-term-weight))
            then $query-term-weight
            else $value-elem/opt:weight
    let $query     :=
        if ($value-elem/opt:element and $value-elem/opt:attribute)             
        then 
            cts:element-attribute-value-query(
                impl:spec-qname-list($value-elem/opt:element),
                impl:spec-qname-list($value-elem/opt:attribute),
                $query-elem/search:text,
                $term-opts,
                $term-weight
            )
        else if ($value-elem/opt:element)
        then 
            cts:element-value-query(
                impl:spec-qname-list($value-elem/opt:element),
                $query-elem/search:text,
                $term-opts,
                $term-weight
            )
        else if ($value-elem/opt:field)
        then 
            cts:field-value-query(
                $value-elem/opt:field/@name,
                $query-elem/search:text,
                $term-opts,
                $term-weight
            )
        else if ($value-elem/opt:json-property)
        then
            cts:json-property-value-query(
                $value-elem/opt:json-property/string(),
                ast:json-typed-values($query-elem,$value-elem,$state),
                $term-opts,
                $term-weight
            )
        else
            ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY: index not specified"/>,$state)
    return 
        ast:wrap-scope($constraint-scope,$global-scope,$query)
};

declare function ast:handle-range-constraint(
    $query-elem as element(search:range-constraint-query),
    $options as element(opt:options),
    $state as map:map
) as cts:query?
{
    let $constraint := ast:matched-constraint($query-elem,$options,$state)
    return 
        if (empty($constraint) or empty($constraint/opt:range))
        then ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY:[{$query-elem/search:constraint-name}] is not a valid constraint name"/>,$state)
        else ast:range-query($query-elem,$constraint/opt:range,$constraint//opt:fragment-scope,$options/opt:fragment-scope,$state)
};

declare function ast:handle-range-query(
    $query-elem as element(search:range-query),
    $options    as element(opt:options),
    $state      as map:map
) as cts:query?
{
    ast:range-query($query-elem, $query-elem,
        $query-elem/opt:fragment-scope, $options/opt:fragment-scope, $state)
}; 

declare function ast:range-query(
   $query-elem       as element(),
   $range-elem       as element(),
   $constraint-scope as element(opt:fragment-scope)?,
   $global-scope     as element(opt:fragment-scope)?,
   $state            as map:map
) as cts:query?
{
     let $query :=
        if (empty($range-elem/(opt:bucket|opt:computed-bucket)))
        then ast:handle-range-constraint-operator($query-elem,$range-elem,$state)
        else ast:handle-range-constraint-bucketed($query-elem,$range-elem,$state)
    return 
        if (empty($query)) 
        then ()
        else ast:wrap-scope($constraint-scope,$global-scope,$query)
};

declare function ast:handle-period-range-query(
    $query-elem as element(),
    $options as element(opt:options),
    $state      as map:map
) as cts:query?
{
    let $global-scope := $options/opt:fragment-scope
    let $axes := $query-elem/search:axis/string()
    let $operator := $query-elem/search:temporal-operator/string()
    let $query-opts := $query-elem/search:temporal-option/string()
    let $periods := 
        for $p in $query-elem/search:period
        let $period-start := $p/search:period-start
        let $period-end := $p/search:period-end
        let $validate := (
            if ($period-start castable as xs:dateTime)
            then 1
            else ast:add-annotation(
                 <search:annotation warning="SEARCH-INVALIDQUERY:[{$period-start}] is not a valid dateTime"/>,$state),
            if (empty($period-end) or $period-end castable as xs:dateTime)
            then 1
            else ast:add-annotation(
                <search:annotation warning="SEARCH-INVALIDQUERY:[{$period-end}] is not a valid dateTime"/>,$state),
            if (empty($period-end) or $period-start/data() le $period-end/data()) 
            then 1
            else ast:add-annotation(
                <search:annotation 
                    warning="SEARCH-INVALIDQUERY: start [{$period-start}] cannot be greater than  [{$period-end}]"/>,$state)
             )
        return  
            if (count($validate) lt 3) then ()
            else if (exists($period-end))
            then cts:period($period-start/data(),$period-end/data())
            else cts:period($period-start/data())
    let $query :=  
        if (count($periods) eq count($query-elem/search:period))
        then cts:period-range-query($axes,$operator,$periods,$query-opts)    
        else ast:add-annotation(<search:annotation 
                    warning="SEARCH-IGNOREDQTEXT:dropping invalid period query"/>,$state)
    return 
        if (empty($query))
        then ()
        else ast:wrap-scope((),$global-scope,$query)

};

declare function ast:handle-range-constraint-bucketed(
    $query-elem as element(),
    $range-elem as element(),
    $state      as map:map
) as cts:query?
{
    let $matched-buckets := 
        ($range-elem/(opt:bucket|opt:computed-bucket)[@name = $query-elem/(search:value|search:text)/string()])
    let $operator := $query-elem/search:range-operator/string()
    return
        if (count($matched-buckets) gt 1)
        then 
            if ($operator eq "EQ" or empty($operator))
            then cts:or-query(
                for $matched-bucket in $matched-buckets
                return ast:handle-individual-bucket($query-elem, $range-elem, $matched-bucket, $state))
            else if ($operator eq "NE")
            then cts:and-query(
                for $matched-bucket in $matched-buckets
                return ast:handle-individual-bucket($query-elem, $range-elem, $matched-bucket, $state))
            else ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY:[{$query-elem/search:range-operator}] is not a valid operator for bucketed range queries"/>,$state)

        else ast:handle-individual-bucket($query-elem, $range-elem, $matched-buckets, $state)
};

declare private function ast:handle-individual-bucket(
    $query-elem     as element(),
    $range-elem     as element(),
    $matched-bucket as element()?,
    $state          as map:map
) as cts:query?
{
    let $range-index-type :=
        let $index-type := ast:get-range-type($range-elem)
        return (
            if ($index-type gt 0) then ()
            else ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY: index not specified"/>,$state),
            $index-type
            )
    let $type := impl:range-type($range-elem)
    let $range-operator := $query-elem/search:range-operator/string()
    let $query-opts := distinct-values((
        let $collation := $range-elem/@collation
        return 
            if (empty($collation)) then ()
            else concat("collation=",data($collation)),
        let $range-option :=
            let $query-range-option := $query-elem/search:range-option
            return
                if (exists($query-range-option))
                then $query-range-option
                else $range-elem/search:range-option 
        return $range-option/string(.)
        ))
    let $lt := 
        if (empty($matched-bucket/@lt)) then ()
        else
            let $operator := "<"
            let $val := 
                    impl:compute-bucket-boundary($matched-bucket/@lt,
                        $type,
                        ($matched-bucket/@lt-anchor, $matched-bucket/@anchor)[1],
                        local-name($matched-bucket),
                        $range-elem/opt:anchor)
            let $typed-val := impl:cast($type, $val) 
            return ast:make-range-query(
                $range-elem,$range-index-type,$operator,$type,$typed-val,$query-opts
                )
    let $ge :=  
        if (empty($matched-bucket/@ge)) then ()
        else
            let $operator := ">="
            let $val := 
                impl:compute-bucket-boundary($matched-bucket/@ge,
                    $type,
                    ($matched-bucket/@ge-anchor, $matched-bucket/@anchor)[1],
                    local-name($matched-bucket),
                    $range-elem/opt:anchor)
            let $typed-val := impl:cast($type, $val)
            return ast:make-range-query(
                $range-elem,$range-index-type,$operator,$type,$typed-val,$query-opts
                )
    let $inner-query :=
        if (empty($matched-bucket))
        then ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY:[{$query-elem/(search:value|search:text)}] is not a valid bucket name"/>,$state)
        else if (exists($ge) and exists($lt))
        then cts:and-query(($ge,$lt))
        else ($ge,$lt)  
    return
        if (empty($range-operator) or $range-operator eq "EQ")
        then $inner-query
        else if ($range-operator eq "NE")
        then cts:not-query($inner-query)
        else ast:add-annotation(
            <search:annotation warning="SEARCH-INVALIDQUERY:[{$query-elem/search:range-operator}] is not a valid operator for bucketed range queries"/>,
            $state
            )
};

declare function ast:handle-range-constraint-operator(
    $query-elem as element(),
    $range-elem as element(),
    $state as map:map
) as cts:query?
{
    let $operator := $query-elem/search:range-operator/string()
    let $range-operator :=
        if ($operator eq "LT")
        then "<"
        else if ($operator eq "LE")
        then "<="
        else if ($operator eq "GT")
        then ">"
        else if ($operator eq "GE")
        then ">="
        else if ($operator eq "NE")
        then "!="
        else "="
    let $vals := $query-elem/(search:value|search:text)/string()
    let $val-count := count($vals)
    return
        if ($val-count eq 0)
        then ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY:[{$range-operator}] without value"/>,$state)
        else if ($val-count eq 1 or $range-operator = ("=", "!="))
        then ast:handle-individual-range-operator($query-elem, $range-elem, $state, $range-operator, $vals)
        else ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY:[{$range-operator}] is not a valid operator for multiple value match"/>,$state)
};

declare private function ast:handle-individual-range-operator(
    $query-elem     as element(),
    $range-elem     as element(),
    $state          as map:map,
    $range-operator as xs:string,
    $values         as item()+
) as cts:query?
{
    let $range-index-type :=
        let $index-type := ast:get-range-type($range-elem)
        return (
            if ($index-type gt 0) then ()
            else ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY: index not specified"/>,$state),
            $index-type
            )
    let $type := impl:range-type($range-elem)
    let $query-opts := distinct-values((
        let $collation := $range-elem/@collation
        return 
            if (empty($collation)) then ()
            else concat("collation=",data($collation)),
        let $range-option :=
            let $query-range-option := $query-elem/search:range-option
            return
                if (exists($query-range-option))
                then $query-range-option
                else $range-elem/search:range-option 
        return $range-option/string(.)
        ))
    let $is-valid := empty(
        let $type-name := impl:type-name($type)
        for $value in $values
        return
            if (xdmp:castable-as($impl:XML_SCHEMA_NS_URI,$type-name,$value)) then ()
            else false()
        )
    return
        if ($is-valid) then 
            let $typed-values :=
                for $value in $values
                return impl:cast($type,$value)
            return ast:make-range-query(
                $range-elem,$range-index-type,$range-operator,$type,$typed-values,$query-opts
                )
         else ast:add-annotation(
             <search:annotation warning="SEARCH-IGNOREDQTEXT:[{concat($query-elem/search:constraint-name/string(),":",$values)}] is not a valid range constraint against type {$type}"/>,
             $state
             )
};

declare private function ast:get-range-type(
    $range-elem as element()
) as xs:int
{
    if (exists($range-elem/opt:attribute))
    then $ast:ATTRIBUTE_INDEX
    else if (exists($range-elem/opt:element))
    then $ast:ELEMENT_INDEX
    else if (exists($range-elem/opt:field))
    then $ast:FIELD_INDEX
    else if (exists($range-elem/opt:json-property))
    then $ast:JSON_PROPERTY_INDEX
    else if (exists($range-elem/opt:path-index))
    then $ast:PATH_INDEX
    else $ast:UNKNOWN_INDEX
};

declare private function ast:make-range-query(
    $range-elem       as element(),
    $range-index-type as xs:int,
    $operator         as xs:string,
    $type             as xs:string,
    $value            as xs:anyAtomicType*,
    $options          as xs:string*
) as cts:query?
{
    switch ($range-index-type)
    case $ast:ATTRIBUTE_INDEX
    return cts:element-attribute-range-query(
        impl:spec-qname-list($range-elem/opt:element),
        impl:spec-qname-list($range-elem/opt:attribute),
        $operator,
        $value,
        $options
        )
    case $ast:ELEMENT_INDEX
    return cts:element-range-query(
        impl:spec-qname-list($range-elem/opt:element),
        $operator,
        $value,
        $options
        )
    case $ast:FIELD_INDEX
    return cts:field-range-query(
        $range-elem/opt:field/@name/string(),
        $operator,
        $value,
        $options
        )
    case $ast:JSON_PROPERTY_INDEX
    return cts:json-property-range-query(
        $range-elem/opt:json-property/string(),
        $operator,
        $value,
        $options
        )
    case $ast:PATH_INDEX
    return ast:path-range-query(
        $range-elem,
        $operator,
        $type,
        $value,
        $options
        )
    default return ()
};

(: Path range queries have to be constructed with namepaces :)
declare function ast:path-range-query(
    $range-elem     as element(),
    $range-operator as xs:string,
    $type           as xs:string,
    $values         as item()+,
    $query-opts     as xs:string*
) as cts:query?
{
    let $path-indexes := $range-elem/opt:path-index
    let $typed-values :=
        for $value in $values
        return impl:cast($type,$value)
    let $bindings     := ast:collect-namespace-bindings($path-indexes)
    let $paths        := $path-indexes/string(.)
    return
        if (empty($bindings)) then
            cts:path-range-query(
                $paths,$range-operator,$typed-values,$query-opts
                )
        else xdmp:with-namespaces($bindings,
            cts:path-range-query(
                $paths,$range-operator,$typed-values,$query-opts
                )
            )
};

(: differs from impl:collect-namespace-bindings() in returning string list and
   filtering more namespaces :)
declare function ast:collect-namespace-bindings(
    $bindables as element()*
) as xs:string*
{
    for $bindable in $bindables
    for $prefix in in-scope-prefixes($bindable)
    let $nsuri := namespace-uri-for-prefix($prefix,$bindable)
    where not($prefix = ("","opt","xml","search"))
    return
        if ($nsuri eq "http://marklogic.com/appservices/search") then ()
        else ($prefix, $nsuri)
};

declare function ast:handle-word-constraint(
    $query-elem as element(search:word-constraint-query),
    $options as element(opt:options),
    $state as map:map
) as cts:query?
{
    let $constraint := ast:matched-constraint($query-elem,$options,$state)
    return 
        if (empty($constraint) or empty($constraint/opt:word))
        then ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY:[{$query-elem/search:constraint-name}] is not a valid constraint name"/>,$state)
        else ast:word-query($query-elem,$constraint/opt:word,$constraint//opt:fragment-scope,$options/opt:fragment-scope, false(), $state)
};

declare function ast:handle-true-query(
    $query-elem as element(search:true-query),
    $options    as element(opt:options),
    $state      as map:map
) as cts:query?
{
    ast:true-query()
};

declare function ast:true-query(
) as cts:query?
{
    cts:true-query()
};

declare function ast:handle-false-query(
    $query-elem as element(search:false-query),
    $options    as element(opt:options),
    $state      as map:map
) as cts:query?
{
    ast:false-query()
};

declare function ast:false-query(
) as cts:query?
{
    cts:false-query()
};

declare function ast:handle-word-query(
    $query-elem as element(search:word-query),
    $options    as element(opt:options),
    $state      as map:map
) as cts:query?
{
    ast:word-query($query-elem, $query-elem,
        $query-elem/opt:fragment-scope, $options/opt:fragment-scope, false(), $state)
};

declare function ast:word-query(
    $query-elem       as element(),
    $word-elem        as element(),
    $constraint-scope as element(opt:fragment-scope)?,
    $global-scope     as element(opt:fragment-scope)?,
    $is-global        as xs:boolean,
    $state            as map:map
) as cts:query?
{
    let $term-opts :=
        let $query-term-opts :=
            typeswitch($query-elem)
            case element(search:term-query) return $query-elem/search:term-option
            default                         return ()
        return
            if (exists($query-term-opts))
            then $query-term-opts
            else $word-elem/opt:term-option
    let $term-weight :=
        let $query-term-weight := $query-elem/search:weight
        return
            if (exists($query-term-weight))
            then $query-term-weight
            else $word-elem/opt:weight
    let $query :=
        if ($word-elem/opt:element and $word-elem/opt:attribute)             
        then 
            cts:element-attribute-word-query(
                impl:spec-qname-list($word-elem/opt:element),
                impl:spec-qname-list($word-elem/opt:attribute),
                $query-elem/search:text,
                $term-opts,
                $term-weight
            )
        else if ($word-elem/opt:element)
        then 
            cts:element-word-query(
                impl:spec-qname-list($word-elem/opt:element),
                $query-elem/search:text,
                $term-opts,
                $term-weight
            )
        else if ($word-elem/opt:field)
        then 
            cts:field-word-query(
                $word-elem/opt:field/@name,
                $query-elem/search:text,
                $term-opts,
                $term-weight
            )
        else if ($word-elem/opt:json-property)
        then
            cts:json-property-word-query(
                $word-elem/opt:json-property/string(),
                $query-elem/search:text,
                $term-opts,
                $term-weight
            )
        else if ($is-global)
        then 
            cts:word-query(
                $query-elem/search:text,
                $term-opts,
                $term-weight
            )
        else
            ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY: index not specified"/>,$state)
    return 
        ast:wrap-scope($constraint-scope,$global-scope,$query)
};

declare function ast:handle-operator-state(
    $query-elem as element(search:operator-state),
    $options as element(opt:options),
    $state as map:map
) as empty-sequence()
{
    let $operator := ast:matched-operator($query-elem,$options,$state)
    let $operator-ref := $query-elem/search:operator-name/string()
    let $state-ref := $query-elem/search:state-name/string()
    return 
        if (empty($operator))
        then () (: ignored, annotation logged in match-oper if invalid :)
        else 
            if ($operator/opt:state[@name eq $state-ref]) 
            then ast:add-annotation(<cts:annotation operator-ref="{$operator-ref}" state-ref="{$state-ref}"/>,$state)
            else ast:add-annotation(<search:annotation warning="SEARCH-INVALIDSTATE:[{concat($operator-ref, ":",$state-ref)}]"/>,$state)
};

declare function ast:handle-qtext(
    $query-elem as element(search:qtext),
    $options as element(opt:options),
    $state as map:map
) as cts:query?
{
    let $sq := ast:do-tokenize-parse($query-elem/string(), $options, false())
    return ast:dispatch($sq,$options,$state)
};

declare function ast:handle-period-compare-query(
    $query-elem       as element(),
    $options as element(opt:options),
    $state            as map:map
) as cts:query?
{
    let $queryopts := $query-elem/opt:temporal-option/string() 
    let $global-scope := $options/opt:fragment-scope/string()
    let $axis1 := $query-elem/search:axis1/data()
    let $axis2 := $query-elem/search:axis2/data()
    let $temporal-operator := $query-elem/search:temporal-operator/data() 
    let $query := 
        if (exists($axis1) and exists($axis2) and exists($temporal-operator))
        then cts:period-compare-query($axis1,$temporal-operator,$axis2,$queryopts)
        else ()
    return
        if (exists($query))
        then ast:wrap-scope((),$global-scope,$query)
        else ast:add-annotation(
            <search:annotation warning="SEARCH-INVALIDQUERY: must supply axis1, axis2, and temporal-operator"/>,$state
            )
};

declare function ast:handle-lsqt-query(
    $query-elem as element(),
    $options    as element(opt:options),
    $state      as map:map
) as cts:query?
{
    let $queryopts := $query-elem/opt:temporal-option/string()
    let $global-scope := $options/opt:fragment-scope/string()
    let $temporal-collection := $query-elem/opt:temporal-collection/data()
    let $valid := ($temporal-collection = ast:temporal-collections())
    let $query := 
        if ($valid)
        then 
            let $timestamp := 
                if ($query-elem/opt:timestamp) 
                then $query-elem/opt:timestamp/data()
                else 
                    (: Task 29410, this is a work-around pending 
                     : discussion W/Fei, on Chris's list :)
                    try { temporal:get-lsqt($temporal-collection) }
                    catch ($e) { xs:dateTime("1600-01-01T00:00:00") }
            let $set-timestamp := ast:add-annotation(<search:annotation timestamp="{$timestamp}"/>,$state)         
            let $weight := $query-elem/opt:weight/data()
            return cts:lsqt-query($temporal-collection,$timestamp,$queryopts,$weight)
        else ()
    return
        if (exists($query))
        then ast:wrap-scope((),$global-scope,$query)
        else ast:add-annotation(
            <search:annotation warning="SEARCH-IGNOREDQTEXT: dropping invalid lsqt-query, must supply valid temporal-collection"/>,$state
            )
};

(: amped to temporal-admin to read files owned by temporal-admin
   same issue exists for temporal:axes()
 :)
declare private function ast:temporal-collections(
) as xs:string*
{
    (: no need to guard this private function with a security assertion :)
    temporal:collections()
};

declare function ast:handle-before-query(
    $query-elem as element(search:before-query),
    $options as element(opt:options),
    $state as map:map
) as cts:query?
{
    let $global-scope := $options/opt:fragment-scope/string()
    let $timestamp    := $query-elem/search:timestamp/data()[. instance of xs:unsignedLong]
    let $query        :=
        if (count($timestamp) != 1) then ()
        else cts:before-query($timestamp)
    return
        if (exists($query))
        then ast:wrap-scope((),$global-scope,$query)
        else ast:add-annotation(
            <search:annotation warning="SEARCH-IGNOREDQTEXT: dropping invalid before-query, must supply valid timestamp"/>,$state
            )
};
declare function ast:handle-after-query(
    $query-elem as element(search:after-query),
    $options as element(opt:options),
    $state as map:map
) as cts:query?
{
    let $global-scope := $options/opt:fragment-scope/string()
    let $timestamp    := $query-elem/search:timestamp/data()[. instance of xs:unsignedLong]
    let $query        :=
        if (count($timestamp) != 1) then ()
        else cts:after-query($timestamp)
    return
        if (exists($query))
        then ast:wrap-scope((),$global-scope,$query)
        else ast:add-annotation(
            <search:annotation warning="SEARCH-IGNOREDQTEXT: dropping invalid after-query, must supply valid timestamp"/>,$state
            )
};

declare function ast:handle-geospatial-constraint(
    $query-elem as element(search:geospatial-constraint-query),
    $options as element(opt:options),
    $state as map:map
) as cts:query?
{
    let $constraint := ast:matched-constraint($query-elem,$options,$state)
    return 
        if (empty($constraint))
        then ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY:[{$query-elem/search:constraint-name}] is not a valid constraint name"/>,$state)
        else if (not(ast:has-regions($query-elem, $state))) then ()
        else
            typeswitch($constraint/*)
            case element(opt:geo-elem)
            return ast:handle-geo-elem($query-elem,$constraint/opt:geo-elem,$state,$constraint//opt:fragment-scope,$options/opt:fragment-scope)
            case element(opt:geo-attr-pair)
            return ast:handle-geo-attr-pair($query-elem,$constraint/opt:geo-attr-pair,$state,$constraint//opt:fragment-scope,$options/opt:fragment-scope)
            case element(opt:geo-elem-pair)
            return ast:handle-geo-elem-pair($query-elem,$constraint/opt:geo-elem-pair,$state,$constraint//opt:fragment-scope,$options/opt:fragment-scope)
            case element(opt:geo-json-property)
            return ast:handle-geo-json-property($query-elem,$constraint/opt:geo-json-property,$state,$constraint//opt:fragment-scope,$options/opt:fragment-scope)
            case element(opt:geo-json-property-pair)
            return ast:handle-geo-json-property-pair($query-elem,$constraint/opt:geo-json-property-pair,$state,$constraint//opt:fragment-scope,$options/opt:fragment-scope)
            case element(opt:geo-path)
            return ast:handle-geo-path($query-elem,$constraint/opt:geo-path,$state,$constraint//opt:fragment-scope,$options/opt:fragment-scope)
            default
            return ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY:[{$query-elem/search:constraint-name}] does not specify an index"/>,$state)
};

declare function ast:handle-geo-region-constraint(
    $query-elem as element(search:geo-region-constraint-query),
    $options as element(opt:options),
    $state as map:map
) as cts:query?
{
    let $constraint := ast:matched-constraint($query-elem,$options,$state)
    return 
        if (empty($constraint))
        then ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY:[{$query-elem/search:constraint-name}] is not a valid constraint name"/>,$state)
        else if (not(ast:has-regions($query-elem, $state))) then ()
        else
            typeswitch($constraint/*)
            case element(opt:geo-region-path)
            return ast:handle-geo-region-path($query-elem,$constraint/opt:geo-region-path,$state,$constraint//opt:fragment-scope,$options/opt:fragment-scope)
            default
            return ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY:[{$query-elem/search:constraint-name}] does not specify an index"/>,$state)
};

declare function ast:handle-geo-elem-query(
    $query-elem as element(search:geo-elem-query),
    $options    as element(opt:options),
    $state      as map:map
) as cts:query?
{
    if (not(ast:has-regions($query-elem, $state))) then ()
    else ast:handle-geo-elem($query-elem,$query-elem,$state,$query-elem/opt:fragment-scope,$options/opt:fragment-scope)
};

declare function ast:handle-geo-elem-pair-query(
    $query-elem as element(search:geo-elem-pair-query),
    $options    as element(opt:options),
    $state      as map:map
) as cts:query?
{
    if (not(ast:has-regions($query-elem, $state))) then ()
    else ast:handle-geo-elem-pair($query-elem,$query-elem,$state,$query-elem/opt:fragment-scope,$options/opt:fragment-scope)
};

declare function ast:handle-geo-attr-pair-query(
    $query-elem as element(search:geo-attr-pair-query),
    $options    as element(opt:options),
    $state      as map:map
) as cts:query?
{
    if (not(ast:has-regions($query-elem, $state))) then ()
    else ast:handle-geo-attr-pair($query-elem,$query-elem,$state,$query-elem/opt:fragment-scope,$options/opt:fragment-scope)
};

declare function ast:handle-geo-json-property-query(
    $query-elem as element(search:geo-json-property-query),
    $options    as element(opt:options),
    $state      as map:map
) as cts:query?
{
    if (not(ast:has-regions($query-elem, $state))) then ()
    else ast:handle-geo-json-property($query-elem,$query-elem,$state,$query-elem/opt:fragment-scope,$options/opt:fragment-scope)
};

declare function ast:handle-geo-json-property-pair-query(
    $query-elem as element(search:geo-json-property-pair-query),
    $options    as element(opt:options),
    $state      as map:map
) as cts:query?
{
    if (not(ast:has-regions($query-elem, $state))) then ()
    else ast:handle-geo-json-property-pair($query-elem,$query-elem,$state,$query-elem/opt:fragment-scope,$options/opt:fragment-scope)
};

declare function ast:handle-geo-path-query(
    $query-elem as element(search:geo-path-query),
    $options    as element(opt:options),
    $state      as map:map
) as cts:query?
{
    if (not(ast:has-regions($query-elem, $state))) then ()
    else ast:handle-geo-path($query-elem,$query-elem,$state,$query-elem/opt:fragment-scope,$options/opt:fragment-scope)
};

declare function ast:handle-geo-region-path-query(
    $query-elem as element(search:geo-region-path-query),
    $options    as element(opt:options),
    $state      as map:map
) as cts:query?
{
    if (not(ast:has-regions($query-elem, $state))) then ()
    else ast:handle-geo-region-path($query-elem,$query-elem,$state,$query-elem/opt:fragment-scope,$options/opt:fragment-scope)
};


declare function ast:handle-geo-elem(
    $query-elem       as element(),
    $pair             as element(),
    $state            as map:map,
    $constraint-scope as element(opt:fragment-scope)?,
    $global-scope     as element(opt:fragment-scope)?
) as cts:query?
{
    let $queryopts := 
            if ($query-elem/opt:geo-option)
            then $query-elem/opt:geo-option/string()
            else $pair/opt:geo-option/string()
    let $query := 
        if (exists($pair/opt:parent)) then
            cts:element-child-geospatial-query (
                impl:spec-qname-list($pair/opt:parent),
                impl:spec-qname-list($pair/opt:element),
                ast:regions($query-elem),
                $queryopts)
        else if (exists($pair/opt:element)) then
            cts:element-geospatial-query (
                impl:spec-qname-list($pair/opt:element),
                ast:regions($query-elem),
                $queryopts
            )
        else ()
    return
        if (exists($query))
        then ast:wrap-scope($constraint-scope,$global-scope,$query)
        else ast:add-annotation(
            <search:annotation warning="SEARCH-INVALIDQUERY: index not specified"/>,$state
            )
};

declare function ast:handle-geo-elem-pair(
    $query-elem       as element(),
    $pair             as element(),
    $state            as map:map,
    $constraint-scope as element(opt:fragment-scope)?,
    $global-scope     as element(opt:fragment-scope)?
) as cts:query?
{              
    let $query := 
        if (exists($pair/opt:parent)) then
            cts:element-pair-geospatial-query(
                impl:spec-qname-list($pair/opt:parent),
                impl:spec-qname-list($pair/opt:lat),
                impl:spec-qname-list($pair/opt:lon),
                ast:regions($query-elem),
                if ($query-elem/opt:geo-option)
                then $query-elem/opt:geo-option/string()
                else $pair/opt:geo-option/string()
            )
        else ()
    return
        if (exists($query))
        then ast:wrap-scope($constraint-scope,$global-scope,$query)
        else ast:add-annotation(
            <search:annotation warning="SEARCH-INVALIDQUERY: index not specified"/>,$state
            )
};

declare function ast:handle-geo-attr-pair(
    $query-elem       as element(),
    $pair             as element(),
    $state            as map:map,
    $constraint-scope as element(opt:fragment-scope)?,
    $global-scope     as element(opt:fragment-scope)?
) as cts:query?
{             
    let $query := 
        if (exists($pair/opt:parent)) then
            cts:element-attribute-pair-geospatial-query(
                impl:spec-qname-list($pair/opt:parent),
                impl:spec-qname-list($pair/opt:lat),
                impl:spec-qname-list($pair/opt:lon),
                ast:regions($query-elem),
                if ($query-elem/opt:geo-option)
                then $query-elem/opt:geo-option/string()
                else $pair/opt:geo-option/string()
            )
        else ()
    return
        if (exists($query))
        then ast:wrap-scope($constraint-scope,$global-scope,$query)
        else ast:add-annotation(
            <search:annotation warning="SEARCH-INVALIDQUERY: index not specified"/>,$state
            )
};

declare function ast:handle-geo-json-property(
    $query-elem       as element(),
    $pair             as element(),
    $state            as map:map,
    $constraint-scope as element(opt:fragment-scope)?,
    $global-scope     as element(opt:fragment-scope)?
) as cts:query?
{
    let $queryopts := 
            if ($query-elem/opt:geo-option)
            then $query-elem/opt:geo-option/string()
            else $pair/opt:geo-option/string()
    let $query := 
        if (exists($pair/opt:parent-property)) then
            cts:json-property-child-geospatial-query (
                $pair/opt:parent-property/string(.),
                $pair/opt:json-property/string(.),
                ast:regions($query-elem),
                $queryopts)
        else if (exists($pair/opt:json-property)) then
            cts:json-property-geospatial-query (
                $pair/opt:json-property/string(.),
                ast:regions($query-elem),
                $queryopts
            )
        else ()
    return
        if (exists($query))
        then ast:wrap-scope($constraint-scope,$global-scope,$query)
        else ast:add-annotation(
            <search:annotation warning="SEARCH-INVALIDQUERY: index not specified"/>,$state
            )
};

declare function ast:handle-geo-json-property-pair(
    $query-elem       as element(),
    $pair             as element(),
    $state            as map:map,
    $constraint-scope as element(opt:fragment-scope)?,
    $global-scope     as element(opt:fragment-scope)?
) as cts:query?
{              
    let $query := 
        if (exists($pair/opt:parent-property)) then
            cts:json-property-pair-geospatial-query(
                $pair/opt:parent-property/string(.),
                $pair/opt:lat-property/string(.),
                $pair/opt:lon-property/string(.),
                ast:regions($query-elem),
                if ($query-elem/opt:geo-option)
                then $query-elem/opt:geo-option/string()
                else $pair/opt:geo-option/string()
            )
        else ()
    return
        if (exists($query))
        then ast:wrap-scope($constraint-scope,$global-scope,$query)
        else ast:add-annotation(
            <search:annotation warning="SEARCH-INVALIDQUERY: index not specified"/>,$state
            )
};

declare function ast:handle-geo-path(
    $query-elem       as element(),
    $geo-path         as element(),
    $state            as map:map,
    $constraint-scope as element(opt:fragment-scope)?,
    $global-scope     as element(opt:fragment-scope)?
) as cts:query?
{             
    let $path-indexes := $geo-path/opt:path-index
    let $regions      := ast:regions($query-elem)
    let $geo-options  :=
            if ($query-elem/opt:geo-option)
            then $query-elem/opt:geo-option/string()
            else $geo-path/opt:geo-option/string()
    let $bindings     := ast:collect-namespace-bindings($path-indexes)
    let $paths        := $path-indexes/string(.)
    return ast:wrap-scope($constraint-scope,$global-scope,
        if (empty($bindings)) then
            cts:path-geospatial-query(
                $paths,$regions,$geo-options
                )
        else xdmp:with-namespaces($bindings,
            cts:path-geospatial-query(
                $paths,$regions,$geo-options
                )
            )
        )
};

declare function ast:handle-geo-region-path(
    $query-elem       as element(),
    $geo-path         as element(),
    $state            as map:map,
    $constraint-scope as element(opt:fragment-scope)?,
    $global-scope     as element(opt:fragment-scope)?
) as cts:query?
{             
    let $path-indexes := $geo-path/opt:path-index
    let $regions      := ast:regions($query-elem)
    let $coord        := if ($query-elem/@coord) then string($query-elem/@coord) else if ($geo-path/@coord) then string($geo-path/@coord) else "wgs84"
    let $operator     := if ($query-elem/search:geospatial-operator) then $query-elem/search:geospatial-operator/string() else "contains"
    let $geo-options  :=
            if ($query-elem/opt:geo-option)
            then $query-elem/opt:geo-option/string()
            else $geo-path/opt:geo-option/string()
    let $bindings     := ast:collect-namespace-bindings($path-indexes)
    let $paths        := $path-indexes/string(.)
    return ast:wrap-scope($constraint-scope,$global-scope,
        if (empty($bindings)) then
            cts:geospatial-region-query(
                for $path in $paths
                return cts:geospatial-region-path-reference($path,("coordinate-system="||$coord)),
                $operator,$regions,$geo-options
                )
        else xdmp:with-namespaces($bindings,
                cts:geospatial-region-query(
                    for $path in $paths
                    return cts:geospatial-region-path-reference($path,("coordinate-system="||$coord)),
                    $operator,$regions,$geo-options
                )
            )
        )
};

declare function ast:has-regions(
    $query-elem as element(),
    $state      as map:map
) as xs:boolean
{
    let $has-regions := exists($query-elem/(search:box|search:circle|search:point|search:polygon))
    return
        if ($has-regions)
        then true()
        else (
            let $constraint-name := $query-elem/search:constraint-name
            return ast:add-annotation(
                if (exists($constraint-name))
                    then <search:annotation warning="SEARCH-INVALIDQUERY:[{$constraint-name}] no regions specified"/>
                    else <search:annotation warning="SEARCH-INVALIDQUERY: no regions specified"/>,
                $state
                ),

            false()
            )
};

declare function ast:regions(
    $query-elem as element()
) as cts:region*
{
    for $region in $query-elem/(search:box|search:circle|search:point|search:polygon)
    return 
        typeswitch($region)
        case element(search:box)
        return ast:box($region)
        case element(search:circle)
        return ast:circle($region)
        case element(search:point)
        return ast:point($region)
        case element(search:polygon)
        return ast:polygon($region)
        default return ()
};

declare function ast:box(
    $region as element(search:box)
) as cts:box
{
   (: Casting to string and then xs:untypedAtomic 
    : is an ugly work-around for some precision issues. :)
    cts:box(xs:untypedAtomic($region/search:south/string()),
        xs:untypedAtomic($region/search:west/string()),
        xs:untypedAtomic($region/search:north/string()),
        xs:untypedAtomic($region/search:east/string())) 
};

declare function ast:circle(
    $region as element(search:circle)
) as cts:circle
{
    cts:circle($region/search:radius/xs:float(.),ast:point($region/search:point))
};

declare function ast:point(
    $region as element(search:point)
) as cts:point
{
   (: Casting to string and then xs:untypedAtomic 
    : is an ugly work-around for some precision issues. :)

    cts:point(xs:untypedAtomic($region/search:latitude/string(.)),
        xs:untypedAtomic($region/search:longitude/string(.)))
};

declare function ast:polygon(
    $region as element(search:polygon)
) as cts:polygon
{
    cts:polygon($region/search:point/ast:point(.))
};

(: separated to support custom term for future use :)
declare function ast:handle-custom-constraint(
    $query-elem as element(search:custom-constraint-query),
    $options as element(opt:options),
    $state as map:map
) as cts:query?
{
    let $constraint := ast:matched-constraint($query-elem,$options,$state)
    return 
        if (empty($constraint) or empty($constraint/opt:custom))
        then ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY:[{$query-elem/search:constraint-name}] is not a valid constraint name"/>,$state)
        else ast:custom-query($query-elem,$constraint/opt:custom,$state,$options)
};

declare function ast:custom-query(
   $query-elem as element(),
   $custom-elem as element(opt:custom),
   $state as map:map,
   $options as element(opt:options)
) as cts:query?
{
    if (impl:extension($custom-elem/opt:parse))
    then 
        let $parse-fn  := xdmp:function(QName($custom-elem/opt:parse/@ns, $custom-elem/opt:parse/@apply), $custom-elem/opt:parse/@at)
        let $ctsquery  :=
            if (function-arity($parse-fn) != 3)
            then error((),"SEARCH-BADEXTENSION")
            else if (xdmp:function-parameter-type($parse-fn, 1) ne "xs:string")
            then xdmp:apply($parse-fn,$query-elem,$options)
            else
                let $qtext := $query-elem/search:constraint-name/string(.)
                let $right := $query-elem/search:text /
                    <cts:word-query>
                        <cts:text>{string(.)}</cts:text>
                    </cts:word-query>
                return
                    if (empty($qtext) or empty($right))
                    then error((),"SEARCH-BADEXTENSION")
                    else xdmp:apply($parse-fn,concat($qtext,":"),$right,$custom-elem)
        return 
            typeswitch($ctsquery)
                case cts:query
                    return $ctsquery
                case schema-element(cts:query)
                    return impl:cts-query($ctsquery)
                default return ast:add-annotation(<search:annotation warning="SEARCH-INVALIDQUERY:[{$query-elem/search:constraint-name}] does not parse to a cts query"/>,$state)
    else error((),"SEARCH-BADEXTENSION")
};

(: === helpers and accessors ==== :)

declare function ast:get-annotations(
    $query-map as map:map
) as element()*
{
    map:get($query-map,"annotations")
};

declare function ast:add-annotation(
    $query-elem as element(),
    $query-map as map:map
) as empty-sequence()
{
    map:put($query-map,"annotations",(ast:get-annotations($query-map),$query-elem)) 
};

declare function ast:cts-query(
    $query-map as map:map
) as cts:query?
{
    map:get($query-map,"query")
};

declare function ast:serialize-query(
    $query-map as map:map
) as schema-element(cts:query)?
{
    let $annotations := map:get($query-map,"annotations")[@operator-ref]
    let $query       := map:get($query-map,"query")
    let $namespaces  :=
        for $ns in (
            map:get($query-map,"options")//search:path-index/namespace::*,       
            map:get($query-map, "path-namespaces")
            )
        return
            if (name($ns) eq "" and string($ns) eq "http://marklogic.com/appservices/search") then ()
            else $ns
    return
        if (empty($annotations)) then
            <x>{
                $namespaces,
                $query
            }</x>/*
        else
            let $serialized := <x>{$query}</x>/*
            return element {name($serialized)} {
                $namespaces, $serialized/@*, $serialized/*, $annotations
                }
};

declare function ast:matched-constraint(
    $query-elem as element(),
    $options as element(opt:options),
    $state as map:map
) as element()?
{
    $options/opt:constraint[@name eq $query-elem/search:constraint-name]
};

declare function ast:matched-operator(
    $query-elem as element(),
    $options as element(opt:options),
    $state as map:map
) as element()?
{
    let $matched-operator := $options/opt:operator[@name eq $query-elem/search:operator-name]
    return 
        if (empty($matched-operator)) 
        then ast:add-annotation(<search:annotation warning="SEARCH-INVALIDOPER:[{$query-elem/search:operator-name}] is not a valid operator name"/>,$state)
        else $matched-operator
};

declare function ast:ordered(
    $query-elem as element()
) as xs:string?
{
    if ($query-elem/search:ordered eq true()) 
    then "ordered" 
    else if ($query-elem/search:unordered eq false())
    then "unordered"
    else ()
};

declare function ast:subqueries(
    $query-elem as element()
) as element()*
{
    $query-elem/(* 
        except (search:annotation|search:constraint-name|search:weight|
            search:distance|search:distance-weight|search:uri|search:operator-state|
            search:element|search:json-property))
};

declare function ast:wrap-scope(
    $constraint-scope as element(opt:fragment-scope)?,
    $global-scope     as element(opt:fragment-scope)?,
    $query            as cts:query?
) as cts:query?
{
    if (empty($query)) then ()
    else if ($constraint-scope eq 'properties')
    then cts:properties-fragment-query($query)
    else if ($global-scope and $global-scope eq 'properties'
        and $constraint-scope and $constraint-scope eq 'documents')
    then cts:document-fragment-query($query)
    else $query
};

declare function ast:json-typed-values(
    $query-elem as element(),
    $value-elem as element(),
    $state      as map:map
) as item()*
{
    let $type :=
        let $raw-type := $value-elem/@type/string(.)
        return
            if (empty($raw-type))
            then "xs:string"
            else if ($raw-type eq "number")
            then "xs:double"
            else if ($raw-type ne "null")
            then concat("xs:",$raw-type)
            else ()
    for $value in
        if (empty($type)) then ()
        else $query-elem/search:text/string(.)
    return
        if (empty($value) or $value eq "")
        then $value
        else impl:cast($type, $value)
};
