xquery version "1.0-ml";

module namespace xf = "http://www.lefebvre-sarrut.eu/ns/xmlfirst";

(:======================================================:)
(:CUSTOM FACET:)
(:======================================================:)

(:Documentation:
  This custom facet allows to get the publishing number (numeroOrdre) of an article (childEE) while searching among article only.
  This publishing number does not appears in the article document itself but in the journal (parentEE) document(s) 
  where the article has been published
  A TDE has been setted to project xml data from articles and journals to a graph of RDF triples, espacially : 
  - the relation between article and journal : xf:hasParent
  - the publishing number of each journal : xf:META_EFL_META_numeroOrdre
  This custom facet uses the RDF graph to resolve the relation between article and publishing number
  
  To use this facet, set this constraint in your search:search options :
  <constraint name="NumeroOrdreDansRevueCtsTriples">
    <custom facet="true">
      <parse apply="parse-numeroOrdre" ns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" at="/modules/xf-facet-cts-triples_numeroOrdre.mod.xqy"/>
      <start-facet apply="start-facet-numeroOrdre" ns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" at="/modules/xf-facet-cts-triples_numeroOrdre.mod.xqy"/>
      <finish-facet apply="finish-facet-numeroOrdre" ns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" at="/modules/xf-facet-cts-triples_numeroOrdre.mod.xqy"/>
    </custom>
  </constraint>
:)

declare namespace search = "http://marklogic.com/appservices/search";

declare variable $rdf := 'http://www.w3.org/1999/02/22-rdf-syntax-ns#';
declare variable $rdfs := 'http://www.w3.org/2000/01/rdf-schema#';
declare variable $xf := 'http://www.lefebvre-sarrut.eu/ns/xmlfirst#';


(:Documentation: 
  xf:parse-numeroOrdre allows to have facetName:value in the query string 
  For example NumeroOrdreInParent:12 in this case
  How it works? 
  1) query the RDF graph with cts:triples to get all URIs of docs whose parent doc has the requested numeroOrdre
  2) return a cts:document-query with thoses URIs
  This query will be added to the search:search query which uses this facet in its options
:)
  
declare function parse-numeroOrdre(
  $constraint-qtext as xs:string, 
  $right as schema-element(cts:query),
  $custom-elem as element(search:custom)) 
as schema-element(cts:query)
{
  let $s as xs:string := string($right//cts:text/text())
  
  (:Filter every doc which meta numeroOrdre has the requested value, then get its children uri:)
  let $uris as xs:anyURI* :=
      cts:triples( ((:parentDoc:)) , sem:iri($xf||'META_EFL_META_numeroOrdre') , ($s))
      ! cts:triples( ((:doc:)), sem:iri($xf||'hasParent'), ((:parentDoc:)sem:triple-subject(.)) )
      ! cts:triples( ((:doc:)sem:triple-subject(.)), sem:iri($xf||'doc-uri'), ((:docUri:)) )
      ! sem:triple-object(.)
   
  return
    (: add qtextconst attribute so that search:unparse will work - required for some search library functions :)
    (:see http://blog.davidcassel.net/2011/08/unparsing-a-custom-facet for more explanations:)
    <cts:document-query qtextconst="{concat($constraint-qtext, string($right//cts:text))}">
      {
        for $uri in $uris return 
          <cts:uri>{$uri}</cts:uri>
      }
    </cts:document-query>
};

(:Documentation: 
  xf:start-facet-numeroOrdre generate the values of the facet, its completed by xf:finish-facet-numeroOrdre 
  which generate the good format. It's usefull to have these 2 fonction for optimisation reasons.
:)

declare function start-facet-numeroOrdre(
  $constraint as element(search:constraint),
  $query as cts:query?,
  $facet-options as xs:string*,
  $quality-weight as xs:double?,
  $forests as xs:unsignedLong*)
as item()*
{
   let $values as xs:string* :=
      (:get all documents of the current query - choose doc-uri as predicate so we have one triple by doc:)
      cts:triples( ((:doc:)), (sem:iri($xf||'doc-uri')), (),'=', (), $query )
      (:get parent documents of theses documents:) 
      ! cts:triples((:doc:)sem:triple-subject(.), (sem:iri($xf||'hasParent')), ((:parentDoc:)) )
      (:get numeroOrdre of theses parent documents:) 
      ! cts:triples((:parentDoc:)sem:triple-object(.), (sem:iri($xf||'META_EFL_META_numeroOrdre')), ((:numeroOrdre:)) )
      ! sem:triple-object(.)
      
   for $val in distinct-values($values) 
    return 
    <value name="{$val}" count="{count($values[. = $val])}"/>
};

(:Documentation:
  xf:finish-facet-numeroOrdre get the result of start-facet-numeroOrdre in $start argument
  It only format it as expected by the API
:)

declare function finish-facet-numeroOrdre(
  $start as item()*,
  $constraint as element(search:constraint), 
  $query as cts:query?,
  $facet-options as xs:string*,
  $quality-weight as xs:double?, 
  $forests as xs:unsignedLong*)
as element(search:facet)
{
  <search:facet name="{$constraint/@name}">
  {
    for $val in $start
    return
      <search:facet-value name="{$val/@name}" count="{$val/@count}">
        {string($val/@name)}
      </search:facet-value>
  }
  </search:facet>
};