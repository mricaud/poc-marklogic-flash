xquery version "1.0-ml";

module namespace xf = "http://www.lefebvre-sarrut.eu/ns/xmlfirst";

(:======================================================:)
(:CUSTOM SEMANTIC FACET:)
(:======================================================:)

(:Documentation:
  To use this facet, set this constraint in your search:search options :
  <constraint name="age">
    <custom facet="true">
      <parse apply="parse-graph" ns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" at="/modules/xf-facet-cts-triples_graph.mod.xqy"/>
      <start-facet apply="start-facet-graph" ns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" at="/modules/xf-facet-cts-triples_graph.mod.xqy"/>
      <finish-facet apply="finish-facet-graph" ns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" at="/modules/xf-facet-cts-triples_graph.mod.xqy"/>
      <annotation>
        <subject uri-predicate="http://www.lefebvre-sarrut.eu/ns/xmlfirst#doc-uri"/>
        <predicate>http://example.com/ns/person#age</predicate>
      </annotation>
    </custom>
  </constraint>
:)

declare namespace search = "http://marklogic.com/appservices/search";

(:Documentation: 
  xf:parse-graph allows to have "predicate:value" in the query string searching for documents 
  For example age:12 where "age" is a predicate in the RDF Graph
  It suppose the subject (person) also have a "uri" predicate giving the uri of the document from where the triple is out
  The result of the query are theses documents
  How it works? 
  1) query the RDF graph with cts:triples to get all URIs of docs whose graph has the requested predicate/value pair
  2) return a cts:document-query with thoses URIs
  This query will be added to the search:search query which uses this facet in its options
:)

declare function parse-graph(
  $constraint-qtext as xs:string, 
  $right as schema-element(cts:query),
  $custom-elem as element(search:custom)) 
as schema-element(cts:query)
{
  let $s as xs:string:= string($right//cts:text/text())
  
  let $facet-predicates as xs:string* := 
    $custom-elem/search:annotation/search:predicate/text()
  
  let $facet-uri-predicate as xs:string := 
    $custom-elem/search:annotation/search:subject/@uri-predicate/string(.)
  
  let $datatype := if ($custom-elem/search:annotation/search:predicate/@datatype) then ($custom-elem/search:annotation/search:predicate/@datatype/string(.)) else ''
  
  (:let $object :=  sem:typed-literal($s, sem:iri($datatype)):)
  let $object := 
      if ($datatype eq 'xs:integer') then $s cast as xs:integer
      else if ($datatype eq 'xs:decimal') then $s cast as xs:decimal
      else if ($datatype eq 'xs:double') then $s cast as xs:double
      else if ($datatype eq 'xs:float') then $s cast as xs:float
      else if ($datatype eq 'xs:date') then $s cast as xs:date
      else if ($datatype eq 'xs:time') then $s cast as xs:time
      else if ($datatype eq 'xs:dateTime') then $s cast as xs:dateTime
      else if ($datatype eq 'xs:duration') then $s cast as xs:duration
      else if ($datatype eq 'xs:string') then $s cast as xs:string
      else if ($datatype eq 'xs:boolean') then $s cast as xs:boolean
      else if ($datatype eq 'xs:anyURI') then $s cast as xs:anyURI
      else if ($datatype eq 'xs:gDay') then $s cast as xs:gDay
      else if ($datatype eq 'xs:gMonthDay') then $s cast as xs:gMonthDay
      else if ($datatype eq 'xs:gMonth') then $s cast as xs:gMonth
      else if ($datatype eq 'xs:gYearMonth') then $s cast as xs:gYearMonth
      else if ($datatype eq 'xs:gYear') then $s cast as xs:gYear
      else if ($datatype eq 'xs:yearMonthDuration') then $s cast as xs:yearMonthDuration
      else if ($datatype eq 'xs:dayTimeDuration') then $s cast as xs:dayTimeDuration
      else ($s)
  
  (:let $uris as xs:anyURI*:=
      cts:triples((), sem:iri($facet-predicates), $s)
    ! cts:triples((sem:triple-subject(.), sem:iri($facet-uri-predicate), () )
    ! sem:triple-object(.):)

  (:all triplets with the good (path-) predicate:)  
  let $triples := getPathTriples($facet-predicates)
  (:get from $triples those which have the good value:)
  let $triples := cts:triples(sem:triple-subject($triples), sem:triple-predicate($triples), $object)
  (:go back to initial object so one can get its uri:)
  let $triples := getReversePathTriples($facet-predicates, $triples)
  
  (:FIXME : ne fonctionne pas sur un path predicate, car on perd l'uri en changeant de triplet :)
  let $uris as xs:anyURI*:=
      $triples
    ! cts:triples(sem:triple-subject(.), sem:iri($facet-uri-predicate), ())
    ! sem:triple-object(.)
  
 return
    (: add qtextconst attribute so that search:unparse will work - required for some search library functions :)
    (:see http://blog.davidcassel.net/2011/08/unparsing-a-custom-facet for more explanations:)
    <cts:document-query qtextconst="{concat($constraint-qtext, string($right//cts:text))}">
      {
        for $uri in $uris return 
          <cts:uri>{$uri}</cts:uri>
      }
    </cts:document-query>
};

(:Documentation: 
  xf:start-facet-graph generate the values of the facet, its completed by xf:finish-facet-numeroOrdre 
  which generate the good format. It's usefull to have these 2 fonction for optimisation reasons.
  How it works? 
  1) query the RDF graph with cts:triples to get all requested predicate's values 
  2) Get all distinct values of the resulted predicates
:)

declare function start-facet-graph(
  $constraint as element(search:constraint),
  $query as cts:query?,
  $facet-options as xs:string*,
  $quality-weight as xs:double?,
  $forests as xs:unsignedLong*)
as item()*
{
  let $facet-predicates as xs:string* := 
    $constraint/search:custom/search:annotation/search:predicate/text()
  
  (:add the query as arg to cts:triples so it only get triples out of searched documents:)
  (:
    let $values as item()* :=
      cts:triples((), sem:iri($facet-predicates), (), '=', ('concurrent'), $query)
      ! sem:triple-object(.)
    :)
  
  let $triples := getPathTriples($facet-predicates, $query)

  (:contextualize to the current query to filter tiples:)
  (:let $triplesQuery := $triples ! cts:triples(sem:triple-subject(.), sem:triple-predicate(.), sem:triple-object(.), '=', ("concurrent"), $query):)

  let $values as item()* := $triples ! sem:triple-object(.)
    
  let $_ := if (count($facet-predicates) gt 1)
  then
    (
   (
    xdmp:log('Triples Path')
  , xdmp:log($triples)
  , xdmp:log('query')
  , xdmp:log($query)
  , xdmp:log('Triples Merged')
  
   )
  ) else ()
  
   for $val in distinct-values($values) 
    return 
    <value name="{$val}" count="{count($values[. = $val])}"/>
};

(:Documentation:
  xf:finish-facet-numeroOrdre get the result of start-facet-numeroOrdre in $start argument
  It only format it as expected by the API
:)

declare function finish-facet-graph(
  $start as item()*,
  $constraint as element(search:constraint), 
  $query as cts:query?,
  $facet-options as xs:string*,
  $quality-weight as xs:double?, 
  $forests as xs:unsignedLong*)
as element(search:facet)
{
  <search:facet name="{$constraint/@name}">
  {
    for $val in $start
    return
      <search:facet-value name="{$val/@name}" count="{$val/@count}">
        {string($val/@name)}
      </search:facet-value>
  }
  </search:facet>
};

(:1arg signature:)
declare function getPathTriples(
  $predicates as xs:string*
  )
as item()*{
  getPathTriples($predicates, (), ())
};

(:2arg signature:)
declare function getPathTriples(
  $predicates as xs:string*,
  $query as cts:query?
  )
as item()*{
  getPathTriples($predicates, $query, ())
};

declare function getPathTriples(
  $predicates as xs:string*,
  $query as cts:query?,
  $triples as sem:triple*
  )
as item()*{
  let $triples := 
    if (exists($triples)) then (
      $triples ! cts:triples(sem:triple-object(.), (sem:iri($predicates[1])), (), '=', ("concurrent"), ($query))
      )
    else (
      cts:triples((), (sem:iri($predicates[1])), (), '=', ("concurrent"), ($query))
      )
  return 
    if (exists($predicates[2]))
    then (getPathTriples(fn:remove($predicates, 1), (),  $triples))
    else ($triples)
};

declare function getReversePathTriples(
  $predicates as xs:string*,
  $triples as item()*
  )
as item()*{
    if (count($predicates)  gt 1) 
    then (
      let $triples := $triples ! cts:triples((), (), (sem:triple-subject(.)))
      return getReversePathTriples(fn:remove($predicates, count($predicates)), $triples)
      )
    else $triples
};

(:declare function getPathTriples(
  $predicates as xs:string*,
  $query as cts:query?,
  $triples as item()*
  )
as item()*{
  if (count($predicates)  gt 1) 
  then
    let $currentPredicate := $predicates[1]
    let $nextPredicates := $predicates[position() gt 1]
    let $triples := cts:triples((), (sem:iri($currentPredicate)), (), '=', ("concurrent"), $query)
    return getPathTriples($nextPredicates, $query, $triples)
  else (
   if (not(empty($triples)))
   then
    let $r := $triples ! cts:triples(sem:triple-object(.), sem:iri($predicates), (), '=', ("concurrent"), $query)
    return $r
   else
    let $r := cts:triples((), sem:iri($predicates), (), '=', ("concurrent"), $query)
    return $r
   )
};:)
