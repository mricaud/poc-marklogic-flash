xquery version "1.0-ml";

module namespace xf = "http://www.lefebvre-sarrut.eu/ns/xmlfirst";

import module namespace sem = "http://marklogic.com/semantics" at "/MarkLogic/semantics.xqy";

declare namespace search = "http://marklogic.com/appservices/search";
declare namespace cts = "http://marklogic.com/cts";
declare namespace sparql-results = "http://www.w3.org/2005/sparql-results#";
declare namespace opt = "http://marklogic.com/appservices/search";

(:======================================================:)
(:CUSTOM FACET:)
(:======================================================:)

(:Documentation:

  To use this facet, set this constraint in your search:search options :
  <constraint name="NumeroOrdreInParent">
    <custom facet="true">
     <parse apply="parse" ns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" at="/modules/xf-facet.mod.xqy"/>
     <start-facet apply="start-facet" ns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" at="/modules/xf-facet.mod.xqy"/>
     <finish-facet apply="finish-facet" ns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" at="/modules/xf-facet.mod.xqy"/>
    </custom>
  </constraint>:)

declare function parse-graph(
  $constraint-qtext as xs:string, 
  $right as schema-element(cts:query),
  $custom-elem as element(opt:custom)) 
as schema-element(cts:query)
{
  
  let $s as xs:string:= string($right//cts:text/text())
  
  let $rdf-request-property as xs:string := 
    $custom-elem/search:annotation/search:sparql/search:rdf-request-property
  
  let $rdf-uri-property as xs:string := 
    $custom-elem/search:annotation/search:sparql/search:rdf-uri-property
  
  let $sparql-prefixes as element()* := 
    $custom-elem/search:annotation/search:sparql/search:prefix
  
  let $sparqlQuery as xs:string := 
    <myQuery>#parse-graph query
      PREFIX rdf: &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#>
      PREFIX rdfs: &lt;http://www.w3.org/2000/01/rdf-schema#>
      
      {
        for $prefix in $sparql-prefixes 
          return (('PREFIX '|| $prefix/@name || ': ' || $prefix/text() || '&#10;'))
      }
      
      SELECT ?docUri ?property
      WHERE {{
        ?doc rdf:type xf:EditorialEntity .
        ?doc {$rdf-request-property} "{$s}" .
        ?doc {$rdf-uri-property} ?docUri .
      }}
    </myQuery>/text()
    (:let $_ := xdmp:log($sparqlQuery):)
  
  let $triples as item()* := sem:sparql($sparqlQuery)
  let $triples-xml as element(sparql-results:sparql) := sem:query-results-serialize($triples, "xml")
  let $docUris as xs:string* := $triples-xml//sparql-results:binding[@name='docUri']/string(.)
  
  return
    (: add qtextconst attribute so that search:unparse will work - required for some search library functions :)
    (:see http://blog.davidcassel.net/2011/08/unparsing-a-custom-facet for more explanations:)
    <cts:document-query qtextconst="{concat($constraint-qtext, string($right//cts:text))}">
      {
        for $docUri in $docUris return 
          <cts:uri>{$docUri}</cts:uri>
      }
    </cts:document-query>
    
};

(:Documentation: 
  xf:start-facet-numeroOrdre generate the values of the facet, its completed by xf:finish-facet-numeroOrdre 
  which generate the good format. It's usefull to have these 2 fonction for optimisation reasons.
  How it works? 
  1) Get all document URIs of the result of the current search
  2) Query the RDF Graph to get all numeroOrde of parentEE which have childEE attached
  3) Filter the graph result on URIs found at step 1
  4) Get all distinct values of the resulted numeroOrde
:)
declare function start-facet-graph(
  $constraint as element(search:constraint),
  $query as cts:query?,
  $facet-options as xs:string*,
  $quality-weight as xs:double?,
  $forests as xs:unsignedLong*)
as item()*
{

let $currentSearchUris as xs:string* := 
  for $uri in cts:uris((), ($facet-options, "concurrent"), $query, $quality-weight, $forests)
  return string($uri)

let $currentSearchUrisEscaped as xs:string* :=
  for $uri in $currentSearchUris
  (:return replace($uri, '(\\|\.|\*|\+|\?|\{|\}|\(|\)|\[|\]|\^|\$)', '\\$1')
  after testing : no need to escape other chars than "\":)
  return replace($uri, '(\\)', '\\$1')
  
let $currentSearchUrisRegex as xs:string := concat('^(', string-join($currentSearchUrisEscaped, ' | '),'$)')

let $rdf-request-property as xs:string := 
  $constraint/search:custom/search:annotation/search:sparql/search:rdf-request-property

let $rdf-uri-property as xs:string := 
  $constraint/search:custom/search:annotation/search:sparql/search:rdf-uri-property

let $sparql-prefixes as element()* := 
  $constraint/search:custom/search:annotation/search:sparql/search:prefix
  
let $sparqlQuery as xs:string := 
  <myQuery>
    #start-facet-graph query
    PREFIX rdf: &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: &lt;http://www.w3.org/2000/01/rdf-schema#>
    
    {
      for $prefix in $sparql-prefixes 
      return (('PREFIX '|| $prefix/@name || ': ' || $prefix/text() || '&#10;'))
    }
    
    SELECT ?propertyValue
    WHERE {{
      # ?doc rdf:type xf:EditorialEntity .
      ?doc {$rdf-request-property} ?propertyValue .
      ?doc {$rdf-uri-property} ?docUri .
      # Contextualize the query to the current search
      FILTER (regex (?docUri, "{$currentSearchUrisRegex}", "x"))
    }}
  </myQuery>/text()
  (:let $_ := xdmp:log($sparqlQuery):)
  
  let $triples as item()* := sem:sparql($sparqlQuery)
  let $triples-xml as element(sparql-results:sparql) := sem:query-results-serialize($triples, "xml")
  (:let $_ := xdmp:log($triples-xml):)
  for $val in distinct-values($triples-xml//sparql-results:binding[@name='propertyValue']/string(.)) 
  return 
    <value name="{$val}" 
           count="{count($triples-xml//sparql-results:binding[@name='propertyValue'][. = $val])}"/>
};

(:Documentation:
  xf:finish-facet-numeroOrdre get the result of start-facet-numeroOrdre in $start argument
  It only format it as expected by the API:)
declare function finish-facet-graph(
  $start as item()*,
  $constraint as element(search:constraint), 
  $query as cts:query?,
  $facet-options as xs:string*,
  $quality-weight as xs:double?, 
  $forests as xs:unsignedLong*)
as element(search:facet)
{

  <search:facet name="{$constraint/@name}">
  {
    for $val in $start
    return
      <search:facet-value name="{$val/@name}" count="{$val/@count}">
        {string($val/@name)}
      </search:facet-value>
  }
  </search:facet>
};