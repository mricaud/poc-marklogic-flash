xquery version "1.0-ml";

module namespace xf = "http://www.lefebvre-sarrut.eu/ns/xmlfirst";

declare namespace search = "http://marklogic.com/appservices/search";
declare namespace cts = "http://marklogic.com/cts";

(:======================================================:)
(:CUSTOM SNIPPET:)
(:======================================================:)

declare function xf:my-snippet(
  $result as node(),
  $ctsquery as schema-element(cts:query),
  $options as element(search:transform-results)?)
as element(search:snippet)
{
  if (not($result/xf:editorialEntity)) then 
  (
    let $output := 
      <search:snippet>
        {$result}
      </search:snippet>
    return $output
  )
  else (
  let $ee as element() := $result/xf:editorialEntity
  let $ee-short := 
    <editorialEntity xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst">
      {(
        $ee/@*,
        $ee/xf:metadata
      )}
    </editorialEntity>
  let $qText := $ctsquery/cts:text/text()
  let $output := 
    <search:snippet>
      {if ($qText) 
      then (cts:highlight($ee-short, $qText, <span class="highlight">{$cts:text}</span>))
      else ($ee-short)}
    </search:snippet>
    
  (:let $output-debug := <search:snippet>{$ee-short}</search:snippet>:)
  return $output
  )
};