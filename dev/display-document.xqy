xquery version "1.0-ml";

declare namespace xf = "http://www.lefebvre-sarrut.eu/ns/xmlfirst";

declare variable $uri := xdmp:get-request-field("uri", "");

xdmp:set-response-content-type('text/xml'),
doc($uri)